package br.ufsc.lapesd.qsq.detail;

import br.ufsc.lapesd.qsq.relations.impl.NaiveUniqueSequencedRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import br.ufsc.lapesd.qsq.rules.adornment.Adornment;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

import static br.ufsc.lapesd.qsq.detail.QSQRGlobals.Type.ANSWER;
import static br.ufsc.lapesd.qsq.detail.QSQRGlobals.Type.INPUT;
import static br.ufsc.lapesd.qsq.rules.Term.createConstant;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.testng.Assert.*;

public class QSQRGlobalsTest {

    private QSQRGlobals globals;

    @BeforeMethod
    public void setUp() {
        globals = new QSQRGlobals(NaiveUniqueSequencedRelation::new);
    }

    private static AdornedAtom createAtom(@Nonnull String predicate, @Nonnull String adornment,
                                          @Nonnull String... termStrings) {
        List<Term> terms = new ArrayList<>();
        for (String string : termStrings) {
            if (string.startsWith("?"))
                terms.add(Term.createVariable(string.substring(1)));
            else
                terms.add(Term.createConstant(string));
        }
        PlainAtom plainAtom = new PlainAtom(predicate, terms);
        return AdornedAtom.adorn(plainAtom, adornment);
    }

    @Test
    public void testInputRelation() {
        QSQRGlobals.Global global = globals.get(INPUT, createAtom("R",
                "fb", "?x", "a"));
        // input relations only have columns for bounded terms
        assertEquals(global.getRelation().getColumns(), singletonList("2"));

        assertEquals(global.getPredicate(), "R");
        assertEquals(global.getAdornment(), new Adornment("fb"));

        global.addPositional(singletonList(createConstant("b")));

        QSQRGlobals.Global global2 = globals.get(INPUT, createAtom("R",
                "fb", "?x", "c"));
        assertEquals(global2.getPredicate(), "R");
        assertEquals(global2.getAdornment(), new Adornment("fb"));

        assertEquals(global2.getRelation().getColumns(), singletonList("2"));

        List<Row> expected = singletonList(
                new IndexedListRow(singletonList("2"), singletonList("b")));
        assertTrue(RowOps.equals(global2.getRelation(), expected));
    }

    @Test
    public void testGetAllFree() {
        QSQRGlobals.Global global = globals.get(ANSWER,
                createAtom("R", "ff", "?x", "?y"));
        assertEquals(global.getRelation().getColumns(), asList("1", "2"));

        assertEquals(global.getPredicate(), "R");
        assertEquals(global.getAdornment(), new Adornment("ff"));
    }

    @Test
    public void testGetOneBound() {
        QSQRGlobals.Global global = globals.get(ANSWER,
                createAtom("R", "fbf", "?x", "?y", "?z"));
        assertEquals(global.getRelation().getColumns(), asList("1", "2", "3"));

        assertEquals(global.getPredicate(), "R");
        assertEquals(global.getAdornment(), new Adornment("fbf"));
    }

    @Test
    public void testGetSame() {
        QSQRGlobals.Global global = globals.get(ANSWER,
                createAtom("R", "ff", "?x", "?y"));
        assertEquals(global.getRelation().getColumns(), asList("1", "2"));

        global.addPositional(asList(createConstant("a"), createConstant("b")));

        global = globals.get(ANSWER, createAtom("R", "ff",
                "?x", "?y"));
        assertEquals(global.getRelation().getColumns(), asList("1", "2"));
        assertFalse(global.getRelation().isEmpty());

        List<Row> expected = singletonList(new IndexedListRow(asList("1", "2"), asList("a", "b")));
        assertTrue(RowOps.equals(global.getRelation(), expected));
    }
}