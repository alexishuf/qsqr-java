package br.ufsc.lapesd.qsq.detail;

import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedRule;
import br.ufsc.lapesd.qsq.rules.adornment.Adornment;
import br.ufsc.lapesd.qsq.rules.parser.DatalogParser;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom.adorn;
import static java.util.Arrays.stream;
import static org.testng.Assert.assertEquals;

public class SupplementaryRelationsTest {

    private static class Data {
        private final @Nonnull String ruleString;
        private final @Nonnull String headAdornmentString;
        private final @Nonnull String expectedReordering;
        private final @Nonnull String expectedAdornments;
        private final @Nonnull String expectedSuppColsString;

        private final @Nonnull Rule rule;
        private final @Nonnull Adornment headAdornment;
        private final @Nonnull AdornedRule expectedAdornedRule;
        private final @Nonnull List<List<String>> expectedSuppCols;

        /**
         *
         * @param ruleString string representation of the plain rule
         * @param headAdornmentString head adornment as a string of b's and f's
         * @param expectedReordering expected reordering of body atom indices as a ,-separated list
         * @param expectedAdornments expected adornments of the reordered body. Must be a
         *                           ,-separated list of adornment strings (strings of b's and f's).
         * @param expectedSuppColsString column names of each supplementary relation. Must be a
         *                               ;-separated list of ,-separed lists of variable names
         *                               (without the preceding '?')
         * @throws DatalogParser.SyntaxException If the rule has a syntax error
         */
        public Data(@Nonnull String ruleString, @Nonnull String headAdornmentString,
                    @Nonnull String expectedReordering,
                    @Nonnull String expectedAdornments,
                    @Nonnull String expectedSuppColsString) throws DatalogParser.SyntaxException {
            this.ruleString = ruleString;
            this.headAdornmentString = headAdornmentString;
            this.expectedReordering = expectedReordering;
            this.expectedAdornments = expectedAdornments;
            this.expectedSuppColsString = expectedSuppColsString;
            expectedSuppCols = stream(expectedSuppColsString.split("\\s*;\\s*"))
                    .map(s -> stream(s.split("\\s*,\\s*"))
                            .filter(col -> !col.isEmpty())
                            .collect(Collectors.toList()))
                    .collect(Collectors.toList());

            rule = Objects.requireNonNull(new DatalogParser().parseRule(ruleString));
            headAdornment = new Adornment(headAdornmentString);

            String[] bodyAdornments = expectedAdornments.split("\\s*,\\s*");
            String[] reorderingStrings = expectedReordering.split("\\s*,\\s*");
            assert bodyAdornments.length == reorderingStrings.length;
            List<AdornedAtom> adornedBody = new ArrayList<>();
            for (String iStr : reorderingStrings) {
                int i = Integer.parseInt(iStr);
                AdornedAtom atom = adorn(rule.getBody().get(i), bodyAdornments[adornedBody.size()]);
                adornedBody.add(atom);
            }

            expectedAdornedRule = new AdornedRule(rule,
                    new AdornedAtom(rule.getHead(), headAdornment), adornedBody);
        }

        public @Nonnull Rule getRule() {
            return rule;
        }
        public @Nonnull Adornment getHeadAdornment() {
            return headAdornment;
        }
        public @Nonnull List<List<String>> getExpectedSuppCols() {
            return expectedSuppCols;
        }
        public @Nonnull AdornedRule getExpectedAdornedRule() {
            return expectedAdornedRule;
        }

        @Nonnull
        private SupplementaryRelations createRelations() {
            return new SupplementaryRelations(getRule(), getHeadAdornment());
        }

        @Override
        public String toString() {
            return "Data{" +
                    "rule='" + ruleString + '\'' +
                    ", adornment='" + headAdornmentString + '\'' +
                    ", reordering='" + expectedReordering + '\'' +
                    ", expectedAdornments='" + expectedAdornments + '\'' +
                    ", expectedSuppCols='" + expectedSuppColsString + '\'' +
                    '}';
        }
    }

    @DataProvider
    public Object[][] data() throws DatalogParser.SyntaxException {
        return new Object[][] {
                {new Data("R(?x, a) <- R1(?x, b).",
                        "bb",
                        "0",
                        "bb",
                        "x;x") },
                {new Data("R(?x, a) <- R1(?x, ?y), R2(?y, b).",
                        "bb",
                        "0,1",
                        "bf,bb",
                        "x; x,y; x") },
                // reverses the atom order in the body
                {new Data("R(?x, a) <- R2(?y, b), R1(?x, ?y).",
                        "bb",
                        "1,0",
                        "bf,bb",
                        "x; x,y; x") },
                // RSG example from Abiteboul's book
                {new Data("R(?x, ?y) <- D(?y1, ?y), R(?y1, ?x1), U(?x, ?x1).",
                        "fb",
                        "0,1,2",
                        "fb,bf,fb",
                        "y; y,y1; y,x1; x,y") },
                // reverses body atoms
                {new Data("R(?x, ?y) <- U(?x, ?x1), R(?y1, ?x1), D(?y1, ?y).",
                        "fb",
                        "2,1,0",
                        "fb,bf,fb",
                        "y; y,y1; y,x1; x,y") },
                // yet another body re-ordering
                {new Data("R(?x, ?y) <- R(?y1, ?x1), U(?x, ?x1), D(?y1, ?y).",
                        "fb",
                        "2,0,1",
                        "fb,bf,fb",
                        "y; y,y1; y,x1; x,y") },
                // more complicated example from Abiteboul (p. 319)
                {new Data("R(?x, ?y, ?z) <- R1(?x, ?u, ?v), R2(?u, ?w, ?w, ?z), " +
                                                     "R3(?v, ?w, ?y, a).",
                        "bfb",
                        "0,1,2",
                        "bff, bffb, bbfb",
                        "x,z; x,z,u,v; x,z,v,w; x,y,z") },
                // above with reverse body
                {new Data("R(?x, ?y, ?z) <- R3(?v, ?w, ?y, a), R2(?u, ?w, ?w, ?z), " +
                                                     "R1(?x, ?u, ?v).",
                        "bfb",
                        "1,2,0", //R2 has the same cost as R1
                        "fffb, bbf, bbfb",
                        "x,z; x,z,w,u; x,z,v,w; x,y,z") },
                // empty supp_0
                {new Data("R(?x, a, b) <- R(?x, ?y, ?z)",
                          "fbb",
                        "0",
                        "fff",
                        ";x")},
                // rdfs7 -- keep column order on last relation
                {new Data("T(?x, ?b, ?y) <- T(?a, subPropOf, ?b), T(?x, ?a, ?y)",
                        "bbf",
                        "0,1",
                        "fbb, bbf",
                        "x,b; x,b,a; x,b,y")}
        };
    }

    @Test(dataProvider = "data")
    public void testGetSupplementaryCount(@Nonnull Data data) {
        SupplementaryRelations relations = data.createRelations();
        assertEquals(relations.getSupplementaryCount(), data.getRule().getBody().size()+1);
    }

    @Test(dataProvider = "data")
    public void testCreateAdornedRule(@Nonnull Data data) {
        SupplementaryRelations relations = data.createRelations();
        assertEquals(relations.createAdornedRule(), data.getExpectedAdornedRule());
    }

    @Test(dataProvider = "data")
    public void testGetColumnsOfSupplementaryRelations(@Nonnull Data data) {
        SupplementaryRelations relations = data.createRelations();
        for (int i = 0; i < relations.getSupplementaryCount(); i++) {
            assertEquals(relations.getColumnsOfSupplementary(i),
                         data.getExpectedSuppCols().get(i),
                    "columns mismatch at supp_"+i);
        }
    }
}