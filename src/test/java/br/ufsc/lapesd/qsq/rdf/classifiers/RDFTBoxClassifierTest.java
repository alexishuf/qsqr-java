package br.ufsc.lapesd.qsq.rdf.classifiers;

import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.Rule;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.qsq.rdf.RDFTermUtils.toResource;
import static org.testng.Assert.*;

public class RDFTBoxClassifierTest {
    @Test
    public void testRDFS() {
        RDFTBoxClassifier classifiers = new RDFTBoxClassifier();
        Program program = RDFPrograms.RDFS_FULL;
        List<Rule> rules = classifiers.getTBoxRules(program);
        assertNotNull(rules);
        List<String> missing = Stream.of(8, 5, 6, 10, 11, 12, 13).map(i -> "rdfs" + i)
                .filter(name -> !rules.contains(program.getRule(name)))
                .collect(Collectors.toList());
        assertEquals(missing, Collections.emptyList());
    }

    @Test
    public void testOWL2RL() {
        RDFTBoxClassifier classifier = new RDFTBoxClassifier();
        Program program = RDFPrograms.getOwl();
        List<Rule> rules = classifier.getTBoxRules(program);
        assertNotNull(rules);
        assertFalse(rules.isEmpty());
        assertTrue(rules.stream().noneMatch(r -> r.getHead().getTerms().size() != 3));
        assertTrue(rules.stream()
                .noneMatch(r -> toResource(r.getHead().getTerms().get(1)).equals(RDF.type)
                        && r.getHead().getTerms().get(2).isVariable()
                        && r.getHead().getTerms().get(0).isVariable()));
        assertTrue(rules.stream()
                .noneMatch(r -> toResource(r.getHead().getTerms().get(1)).equals(OWL2.sameAs)));
        assertTrue(rules.stream()
                .noneMatch(r -> toResource(r.getHead().getTerms().get(1)).equals(OWL2.differentFrom)));
//        program.streamRules().filter(r -> !rules.contains(r)).forEach(System.out::println);
    }
}