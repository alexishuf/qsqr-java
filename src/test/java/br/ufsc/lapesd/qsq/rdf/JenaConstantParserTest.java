package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.rules.parser.ConstantParser;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.apache.jena.rdf.model.ResourceFactory.*;
import static org.testng.Assert.*;

public class JenaConstantParserTest {
    @DataProvider
    public static Object[][] data() {
        return new Object[][] {
                {"<http://example.org/ns>", createResource("http://example.org/ns"), 23},
                {"<http://example.org/ns> asd", createResource("http://example.org/ns"), 23},
                //012345678901234567890123 -- 23
                {"a", RDF.type, 1},
                {"a, b", RDF.type, 1},
                //0123456789 PrefixMap tests
                {"rdf:type", RDF.type, 8},
                {"rdf:type, a", RDF.type, 8},
                {"rdf:, a", createResource(RDF.getURI()), 4},
                // 01 23456789012
                {"\"1\"^^xsd:int, a", createTypedLiteral("1", XSDDatatype.XSDint), 12},
                //0123 -- 1
                {"bobagem", null, 0},
                {"bobagem)", null, 0},
                {"bobagem,", null, 0},
                {"", null, 0},
                {"?x", null, 0},
                {"?x,", null, 0},
                {"?x ", null, 0},
                {"\"asd\"", createPlainLiteral("asd"), 5},
                // 0123 45
                // 012 345678901234567890123456789012345678901234
                {"\"23\"^^<http://www.w3.org/2001/XMLSchema#int>",
                        createTypedLiteral("23", XSDDatatype.XSDint), 44},
                {"\"brasil\"@pt_BR", createLangLiteral("brasil", "pt_BR"), 14}
                // 0123456 78901234
        };
    }

    @Test(dataProvider = "data")
    public void test(@Nonnull String text, @Nullable RDFNode expected, int endPos) {
        JenaConstantParser parser = new JenaConstantParser();
        assertTrue(parser.tryPrefix("@prefix rdf: <" + RDF.getURI() + ">."));
        assertTrue(parser.tryPrefix("@prefix owl: <" + OWL2.getURI() + ">."));
        assertTrue(parser.tryPrefix("@prefix xsd: <" + XSD.getURI() + ">."));
        assertFalse(parser.tryPrefix(text));
        ConstantParser.Parse parse = parser.parse(text);
        if (expected == null) {
            assertNull(parse);
        } else {
            assertNotNull(parse);
            assertEquals(parse.getValue(), expected);
            assertEquals(parse.getEndPos(), endPos);
        }
    }
}