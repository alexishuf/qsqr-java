package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.relations.MutableRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.impl.AbstractAddRowTest;
import br.ufsc.lapesd.qsq.relations.impl.AbstractProjectionTest;
import br.ufsc.lapesd.qsq.relations.impl.AbstractSelectionTest;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.testng.SkipException;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static br.ufsc.lapesd.qsq.relations.rows.RowOps.toValuesList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.jena.rdf.model.ResourceFactory.createProperty;
import static org.apache.jena.rdf.model.ResourceFactory.createResource;
import static org.testng.Assert.assertTrue;

public class ModelRelationTest {

    private static void addTriple(@Nonnull Model model, @Nonnull List<?> values) {
        if (values.size() > 3)
            throw new SkipException("Cannot add rows with more than 3 columns");
        List<Object> filled = new ArrayList<>(values);
        for (int i = filled.size(); i < 3; i++)
            filled.add("http://example.org/");
        model.add(createResource(filled.get(0).toString()),
                  createProperty(filled.get(1).toString()),
                  createResource(filled.get(2).toString()));
    }

    private static @Nonnull Row convertToString(@Nonnull Row row) {
        List<String> values = toValuesList(row).stream().map(Object::toString).collect(toList());
        return new IndexedListRow(row.getColumns(), values);
    }

    @Test
    public static class SelectionTest extends AbstractSelectionTest {
        @Override
        protected @Nonnull Relation createRelation(@Nonnull List<List<String>> dataRows) {
            Model model = ModelFactory.createDefaultModel();
            for (List<String> row : dataRows) addTriple(model, row);
            return new ModelRelation("T", model, columns);
        }

        @Override
        protected void verifySelection(@Nonnull List<IndexedListRow> expectedList,
                                       @Nonnull Relation selection) {
            Set<Row> expected = expectedList.stream().map(ModelRelationTest::convertToString)
                    .collect(toSet());
            Set<Row> actual = selection.stream().map(ModelRelationTest::convertToString)
                    .collect(toSet());
            assertTrue(RowOps.equals(actual, expected));
        }
    }

    @Test
    public static class ProjectionTest extends AbstractProjectionTest {
        @Override
        protected @Nonnull Relation createRelation(@Nonnull List<String> columns) {
            if (columns.size() != 3)
                throw new SkipException("Implementation does not support more than 3 columns");
            Model model = ModelFactory.createDefaultModel();
            addTriple(model, columns);
            return new ModelRelation("T", model, columns);
        }
    }

    @Test
    public static class AddRowTest extends AbstractAddRowTest {
        private @Nonnull Row convertRow(@Nonnull Row row) {
            List<RDFNode> values = new ArrayList<>(row.getColumns().size());
            for (String column : row.getColumns()) {
                String str = Objects.requireNonNull(row.get(column)).toString();
                values.add(ResourceFactory.createResource(str));
            }
            return new IndexedListRow(row.getColumns(), values);
        }

        @Override
        protected @Nonnull MutableRelation createRelation(@Nonnull List<String> columns,
                                                          @Nonnull List<List<Integer>> data) {
            if (columns.size() != 3)
                throw new SkipException("Cannot add rows when projecting");
            Model model = ModelFactory.createDefaultModel();
            for (List<Integer> row : data) addTriple(model, row);
            List<String> filled = new ArrayList<>(columns);
            for (int i = 0; i < 3; i++) filled.add(null);
            return new ModelRelation("T", model, filled) {
                @Override
                public boolean addRow(@Nonnull Row row) {
                    return super.addRow(convertRow(row));
                }
            };
        }

        @Override
        protected boolean findRow(@Nonnull Relation relation, @Nonnull Row expected) {
            Row converted = convertRow(expected);
            for (Row row : relation) {
                if (RowOps.equals(row, converted)) return true;
            }
            return false;
        }
    }
}