package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.qsq.rules.Term.createConstant;
import static br.ufsc.lapesd.qsq.rules.Term.createVariable;
import static org.testng.Assert.*;

public class JenaProgramsTest {
    @DataProvider
    public static Object[][] filenamesData() {
        return new Object[][] {
                {"rdfs.rules"},
                {"rdfs-fb.rules"},
                {"rdfs-b.rules"},
                {"owl.rules"},
                {"owl-fb-mini.rules"},
                {"owl-fb-micro.rules"}
        };
    }

    @DataProvider
    public static Object[][] rdfsFiles() {
        return new Object[][] {{"rdfs.rules"}, {"rdfs-b.rules"}};
    }

    @Test(dataProvider = "filenamesData")
    public void testLoadNonEmpty(@Nonnull String filename) throws JenaPrograms.UnsupportedFeature {
        Program program = JenaPrograms.getJenaRules("T", true, filename);
        assertNotNull(program);
        assertNotEquals(program.getRuleCount(), 0);
    }

    @Test
    public void testFailUnsupported() {
        String filename = "rdfs-b.rules";
        assertThrows(JenaPrograms.UnsupportedFeature.class,
                () -> JenaPrograms.loadJenaRules("T", false, filename));
    }

    @Test(dataProvider = "rdfsFiles")
    public void testReadRdfs2(@Nonnull String filename) throws JenaPrograms.UnsupportedFeature {
        Program program = JenaPrograms.getJenaRules("T", true, filename);
        List<Rule> rdfs2List = program.streamRules()
                .filter(r -> Objects.equals(r.getName(), "rdfs2")).collect(Collectors.toList());
        assertEquals(rdfs2List.size(), 1);
        Rule rule = rdfs2List.get(0);
        Term x = createVariable("x"), y = createVariable("y");
        Term c = createVariable("c"), p = createVariable("p");
        Term type = createConstant(RDF.type), domain = createConstant(RDFS.domain);

        assertEquals(rule.getHead(), new PlainAtom("T", x, type, c));
        assertEquals(rule.getBody().size(), 2);

        Set<Atom> actual = new HashSet<>(rule.getBody()), expected = new HashSet<>();
        expected.add(new PlainAtom("T", p, domain, c));
        expected.add(new PlainAtom("T", x, p, y));
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "filenamesData")
    public void testNoFunctors(@Nonnull String filename) throws JenaPrograms.UnsupportedFeature {
        Program program = JenaPrograms.getJenaRules("T", true, filename);
        Pattern pattern = Pattern.compile("\\^\\^urn:x-hp-jena:Functor");
        List<String> offending = program.streamRules().map(Rule::toString)
                .filter(r -> pattern.matcher(r).find()).collect(Collectors.toList());
        assertEquals(offending, Collections.emptyList());
    }

    @Test(dataProvider = "filenamesData")
    public void testNoNesting(@Nonnull String filename) throws JenaPrograms.UnsupportedFeature {
        Program program = JenaPrograms.getJenaRules("T", true, filename);
        Pattern pattern = Pattern.compile("\\([^()]*\\(.*\\)[^()]*\\)");
        List<String> offending = program.streamRules().map(Rule::toString)
                .filter(r -> pattern.matcher(r).find()).collect(Collectors.toList());
        assertEquals(offending, Collections.emptyList());
    }
}