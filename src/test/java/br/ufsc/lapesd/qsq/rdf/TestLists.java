package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.rdf.program.RDFProgram;
import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import br.ufsc.lapesd.qsq.rdf.tbox.RDFTBoxReasoner;
import br.ufsc.lapesd.qsq.rdf.tbox.RobustVLogRDFTBoxReasoner;
import br.ufsc.lapesd.qsq.rdf.tbox.VLogRDFTBoxReasoner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestLists {
    public static Map<String, RDFTBoxReasoner.Factory> reasoners;
    public static Map<String, RDFTBoxReasoner.Factory> reliableReasoners;
    public static List<RDFProgram> rdfPrograms;

    static {
        reliableReasoners = new HashMap<>();
        reliableReasoners.put("RobustVLogRDFTBoxReasoner", RobustVLogRDFTBoxReasoner.FACTORY);

        reasoners = new HashMap<>();
        reasoners.putAll(reliableReasoners);
        reasoners.put("VLogRDFTBoxReasoner", VLogRDFTBoxReasoner.FACTORY);

        rdfPrograms = new ArrayList<>();
        rdfPrograms.add(RDFPrograms.EMPTY);
        rdfPrograms.add(RDFPrograms.RDFS_T);
        rdfPrograms.add(RDFPrograms.RDFS_FULL);
        rdfPrograms.add(RDFPrograms.getOwlMicro());
        rdfPrograms.add(RDFPrograms.getOwlMini());
        rdfPrograms.add(RDFPrograms.getOwl());
    }
}
