package br.ufsc.lapesd.qsq.rdf.tbox;

import br.ufsc.lapesd.qsq.rdf.ModelUtils;
import br.ufsc.lapesd.qsq.rdf.TestLists;
import br.ufsc.lapesd.qsq.rdf.program.RDFProgram;
import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import br.ufsc.lapesd.qsq.rules.Program;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotSame;

@Test
public class RDFTBoxReasonerTest {
    public static String RDFS_INFERENCE_TTL = "br/ufsc/lapesd/qsq/rdfs-inference.ttl";
    public static @Nonnull Model loadResourceAsModel(String absolutePath) throws IOException {
        ClassLoader loader = ClassLoader.getSystemClassLoader();
        Model model = ModelFactory.createDefaultModel();
        try (InputStream in = loader.getResourceAsStream(absolutePath)) {
            if (in == null)
                throw new FileNotFoundException(absolutePath);
            RDFDataMgr.read(model, in, RDFLanguages.filenameToLang(absolutePath));
        }
        return model;
    }

    @DataProvider
    public static Object[][] monotonicData() throws IOException {
        List<Object[]> list = new ArrayList<>();
        Model emptyModel = ModelFactory.createDefaultModel();
        Model rdfsModel = loadResourceAsModel(RDFS_INFERENCE_TTL);

        for (RDFTBoxReasoner.Factory factory : TestLists.reliableReasoners.values()) {
            for (RDFProgram program : TestLists.rdfPrograms) {
                list.add(new Object[]{factory.create("T"), program, emptyModel});
                list.add(new Object[]{factory.create("T"), program, rdfsModel});
            }
        }
        return list.toArray(new Object[0][]);
    }

    @Test(dataProvider = "monotonicData")
    public void testMonotonic(@Nonnull RDFTBoxReasoner reasoner,
                              @Nonnull Program program, @Nonnull Model model) {
        reasoner.setProgram(program);
        Model materialized = reasoner.materialize(model);
        assertNotSame(materialized, model);

        Model missing = ModelFactory.createDefaultModel();
        for (StmtIterator it = model.listStatements(); it.hasNext(); ) {
            Statement stmt = it.next();
            if (!materialized.contains(stmt))
                missing.add(stmt);
        }
        StringWriter writer = new StringWriter();
        RDFDataMgr.write(writer, missing, RDFFormat.TURTLE_PRETTY);
        assertEquals(writer.toString(), "");
    }

    @DataProvider
    public static Object[][] subclassData() throws IOException {
        Model input = loadResourceAsModel(RDFS_INFERENCE_TTL);
        List<Object[]> list = new ArrayList<>();
        for (RDFTBoxReasoner.Factory factory : TestLists.reliableReasoners.values()) {
            list.add(new Object[]{factory.create("T"), RDFPrograms.RDFS_T, input});
            list.add(new Object[]{factory.create("T"), RDFPrograms.RDFS_FULL, input});
            list.add(new Object[]{factory.create("T"), RDFPrograms.getOwl(), input});
        }
        return list.toArray(new Object[0][]);
    }

    @Test(dataProvider = "subclassData")
    public void testSubclassTransitivity(@Nonnull RDFTBoxReasoner reasoner,
                                         @Nonnull Program program, @Nonnull Model model) {
        Model result = reasoner.setProgram(program).materialize(model);

        Set<Resource> classes = new HashSet<>();
        Stream.concat(
                ModelUtils.list(model, null, RDF.type, RDFS.Class, Statement::getSubject),
                ModelUtils.list(model, null, RDF.type, OWL2.Class, Statement::getSubject)
        ).forEach(classes::add);
        for (Resource top : classes) {
            Stack<Resource> stack = new Stack<>();
            stack.push(top);
            Set<Resource> visited = new HashSet<>();
            while (!stack.empty()) {
                Resource cls = stack.pop();
                if (!visited.add(cls)) continue;
                ModelUtils.list(model, null, RDFS.subClassOf, cls, Statement::getSubject)
                        .forEach(stack::push);
            }

            String missing = visited.stream()
                    .filter(sub -> !result.contains(sub, RDFS.subClassOf, top))
                    .map(Objects::toString)
                    .collect(Collectors.joining(", "));
            assertEquals(missing, "", "top class: " + top);
        }
    }
}