package br.ufsc.lapesd.qsq.rdf;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.system.PrefixMap;
import org.apache.jena.riot.system.PrefixMapStd;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.OWL2;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.apache.jena.rdf.model.ResourceFactory.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

public class RDFTermUtilsTest {
    @DataProvider
    public static Object[][] ttlData() {
        PrefixMapStd empty = new PrefixMapStd();
        PrefixMapStd std = new PrefixMapStd(RDFTermUtils.defaultPrefixMap);
        return new Object[][] {
                {"<https://example.org>", empty, createResource("https://example.org")},
                {"<https://example.org>", std, createResource("https://example.org")},
                {"<"+OWL2.Class.getURI()+">", empty, OWL2.Class},
                {"\"23\"^^xsd:int", std, createTypedLiteral("23", XSDDatatype.XSDint)},
                {"\"23\"^^xsd:int", empty, null},
                {"\"23\"^^<"+XSDDatatype.XSDint.getURI()+">", empty, createTypedLiteral("23", XSDDatatype.XSDint)},
                {"\"23\"", std, createPlainLiteral("23")},
                {"\"23\"", empty, createPlainLiteral("23")},
                {"\""+OWL.Class+"\"^^xsd:anyURI", std, createTypedLiteral(OWL2.Class.getURI(), XSDDatatype.XSDanyURI)},
                {"\""+OWL.Class+"\"", empty, createPlainLiteral(OWL2.Class.getURI())},
                {"\"Rome\"@en", empty, createLangLiteral("Rome", "en")},
                {"\"Roma\"@pt_BR", empty, createLangLiteral("Roma", "pt_BR")},
                {OWL2.Class.getURI(), empty, OWL2.Class},
                {"", empty, null},
                {"23", empty, null},
                {"?x", empty, null},
        };
    }

    @Test(dataProvider = "ttlData")
    public void testFromTTL(@Nonnull String fragment, @Nonnull PrefixMap prefixMap,
                            @Nullable RDFNode expected) {
        if (expected == null) {
            assertThrows(() -> RDFTermUtils.fromTTL(fragment, prefixMap));
        } else {
            assertEquals(RDFTermUtils.fromTTL(fragment, prefixMap), expected);
        }
    }

    @Test(dataProvider = "ttlData")
    public void testToTTL(@Nonnull String expected, @Nonnull PrefixMap prefixMap,
                          @Nullable RDFNode node) {
        if (node != null) {
            if (node.isResource() && !expected.startsWith("<") && prefixMap.isEmpty())
                expected = "<" + expected + ">";
            String fragment = RDFTermUtils.toTTL(node, prefixMap);
            assertEquals(fragment, expected);
        }
    }
}