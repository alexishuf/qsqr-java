package br.ufsc.lapesd.qsq.rdf.tbox;

import br.ufsc.lapesd.qsq.rdf.TestLists;
import br.ufsc.lapesd.qsq.rdf.program.RDFProgram;
import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.StringWriter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.qsq.rdf.ModelUtils.list;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class RDFTBoxLoaderTest {
    @Test
    public void testLoadLanguage() {
        Model model = new RDFTBoxLoader().addOWL().addRDF().getModel();

        Set<Resource> ontologies = list(model, null, RDF.type, OWL2.Ontology,
                Statement::getSubject).collect(Collectors.toSet());
        Set<Resource> expectedOntologies = new HashSet<>(Arrays.asList(
                ResourceFactory.createResource(RDF.getURI()),
                ResourceFactory.createResource(RDFS.getURI()),
                ResourceFactory.createResource(OWL2.getURI().replaceAll("#$", ""))));
        assertEquals(ontologies, expectedOntologies);
    }

    @Test
    public void testLoadSKOS() {
        String uri = "http://www.w3.org/2004/02/skos/core#";
        Model model = new RDFTBoxLoader().addRDF().fetchOntology(uri).getModel();
        Set<Resource> ontologies = list(model, null, RDF.type, OWL2.Ontology,
                Statement::getSubject).collect(Collectors.toSet());
        // SKOS makes no imports
        Set<Resource> expectedOntologies = new HashSet<>(Arrays.asList(
                ResourceFactory.createResource(RDF.getURI()),
                ResourceFactory.createResource(uri.replaceAll("#$", ""))));
        assertEquals(ontologies, expectedOntologies);
    }

    @DataProvider
    public static Object[][] reasonersData() {
        List<Object[]> list = new ArrayList<>();
        List<RDFProgram> programs = Arrays.asList(RDFPrograms.RDFS_T,
                RDFPrograms.RDFS_FULL, RDFPrograms.getOwl());
        for (RDFTBoxReasoner.Factory factory : TestLists.reasoners.values())
            list.add(new Object[]{factory, RDFPrograms.EMPTY});
        for (RDFTBoxReasoner.Factory factory : TestLists.reliableReasoners.values()) {
            for (RDFProgram program : programs)
                list.add(new Object[]{factory, program});
        }

        return list.toArray(new Object[0][]);
    }

    @Test(dataProvider = "reasonersData")
    public void testReason(@Nonnull RDFTBoxReasoner.Factory factory,
                           @Nonnull RDFProgram program) {
        RDFTBoxLoader loader = new RDFTBoxLoader();
        String resourcePath = "/br/ufsc/lapesd/qsq/rdfs-inference.ttl";
        loader.withReasonerFactory(factory).withProgram(program).addFromResource(resourcePath);

        Model initial = ModelFactory.createDefaultModel();
        initial.add(loader.getModel());
        loader.reason();

        Model materialization = loader.getModel();
        Model missing = ModelFactory.createDefaultModel();
        for (StmtIterator it = initial.listStatements(); it.hasNext(); ) {
            Statement stmt = it.next();
            if (!materialization.contains(stmt))
                missing.add(stmt);
        }
        StringWriter writer = new StringWriter();
        RDFDataMgr.write(writer, missing, RDFFormat.TURTLE_PRETTY);
        assertTrue(missing.isEmpty(), writer.toString());

        Function<String, Resource> r = local -> materialization
                .createResource("http://example.org/ns#" + local);
        materialization.contains(r.apply("D3"), RDFS.subClassOf, r.apply("C1"));
        materialization.contains(r.apply("i5"), RDF.type, r.apply("C1"));
        materialization.contains(r.apply("j1"), RDF.type, r.apply("E2"));
        materialization.contains(r.apply("k2"), RDF.type, r.apply("E3"));
        materialization.contains(r.apply("k1"), RDF.type, r.apply("E2"));
        materialization.contains(r.apply("k1"), RDF.type, r.apply("E1"));
    }

}