package br.ufsc.lapesd.qsq.joins;

import br.ufsc.lapesd.qsq.relations.ProjectionSpec;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.impl.MutableArrayListRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import br.ufsc.lapesd.qsq.stats.QueryEntry;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BindJoinExecutorTest {
    private BindJoinExecutor executor;

    @BeforeMethod
    public void setUp() {
        executor = new BindJoinExecutor();
    }

    private @Nonnull Row createRow(@Nonnull String columns, @Nonnull String values) {
        List<String> columnsList = asList(columns.split("\\s*,\\s*"));
        List<String> valuesList = asList(values.split("\\s*,\\s*"));
        return new IndexedListRow(columnsList, valuesList);
    }

    private @Nonnull List<Row> createRowList(@Nonnull String columns, @Nonnull String rows) {
        List<String> columnsList = asList(columns.split("\\s*,\\s*"));
        return Arrays.stream(rows.split("\\s*;\\s*"))
                .map(str -> new IndexedListRow(columnsList, asList(str.split("\\s*,\\s*"))))
                .collect(Collectors.toList());
    }

    private @Nonnull Set<Row> createRowSet(@Nonnull String columns, @Nonnull String rows) {
        return new HashSet<>(createRowList(columns, rows));
    }

    private @Nonnull Relation createRelation(@Nonnull String name, @Nonnull String columns,
                                             @Nonnull String rows) {
        List<String> columnsList = asList(columns.split("\\s*,\\s*"));
        MutableArrayListRelation relation = new MutableArrayListRelation(name, columnsList);
        createRowList(columns, rows).forEach(relation::addRow);
        return relation;
    }

    @DataProvider
    public Object[][] data() {
        return new Object[][] {
                {
                        LeftDeepJoin.builder().build(createRow("x", "a")),
                        createRowSet("x", "a"),
                        0
                },
                { // simple binary join
                        LeftDeepJoin.builder()
                                .add(createRowList("x,y", "a,k;b,l;a,m"),
                                        ProjectionSpec.nop(singletonList("x")),
                                        ProjectionSpec.nop(asList("x", "y")))
                                .build(createRow("x", "a")),
                        createRowSet("x,y", "a,k;a,m"),
                        0
                },
                { // same as above, but as an actual Relation
                        LeftDeepJoin.builder()
                                .add(createRelation("R", "x,y", "a,k;b,l;a,m"),
                                        ProjectionSpec.nop(singletonList("x")),
                                        ProjectionSpec.nop(asList("x", "y")))
                                .build(createRow("x", "a")),
                        createRowSet("x,y", "a,k;a,m"),
                        1
                },
                { // now project x out of the result
                        LeftDeepJoin.builder()
                                .add(createRelation("R", "x, y", "a,k;b,l;a,m"),
                                        ProjectionSpec.nop(singletonList("x")),
                                        ProjectionSpec.nop(singletonList("y")))
                                .build(createRow("x", "a")),
                        createRowSet("y", "k;m"),
                        1
                },
                { // do a query projection renaming x1 -> x
                        LeftDeepJoin.builder()
                                .add(createRelation("R", "x, y", "a,k;b,l;a,m"),
                                        new ProjectionSpec(singletonList("x1"), singletonList("x")),
                                        ProjectionSpec.nop(asList("x", "y")))
                                .build(createRow("x1", "a")),
                        createRowSet("x,y", "a,k;a,m"),
                        1
                },
                { // three-way join
                        LeftDeepJoin.builder()
                                .add(createRowList("x,y", "a,k;b,l;a,m"),
                                        ProjectionSpec.nop(singletonList("x")),
                                        ProjectionSpec.nop(asList("x", "y")))
                                .add(createRowList("y,z", "n,a;m,b"),
                                        ProjectionSpec.nop(singletonList("y")),
                                        ProjectionSpec.nop(asList("y", "z")))
                                .build(createRow("x", "a")),
                        createRowSet("y,z", "m,b"),
                        0
                },
                { // three-way join, with actual Relation midway
                        LeftDeepJoin.builder()
                                .add(createRowList("x,y", "a,k;b,l;a,m"),
                                        ProjectionSpec.nop(singletonList("x")),
                                        ProjectionSpec.nop(asList("x", "y")))
                                .add(createRelation("R", "y,z", "n,a;m,b"),
                                        ProjectionSpec.nop(singletonList("y")),
                                        ProjectionSpec.nop(asList("y", "z")))
                                .build(createRow("x", "a")),
                        createRowSet("y,z", "m,b"),
                        1
                },
                { // three-way join from initial list
                        LeftDeepJoin.builder()
                                .add(createRowList("x,y", "a,k;b,l;a,m"),
                                        ProjectionSpec.nop(singletonList("x")),
                                        ProjectionSpec.nop(asList("x", "y")))
                                .add(createRowList("y,z", "l,a;m,b;n,o"),
                                        ProjectionSpec.nop(singletonList("y")),
                                        ProjectionSpec.nop(asList("y", "z")))
                                .build(createRowList("x", "a;b")),
                        createRowSet("y,z", "l,a;m,b"),
                        0
                },
                { // regression test -- rename and reorder at resultsProjection
                        LeftDeepJoin.builder()
                                .add(createRelation("R", "x,y", "a,b;a,c;b,c"),
                                        ProjectionSpec.nop(singletonList("y")),
                                        new ProjectionSpec(asList("y", "x"), asList("y", "y1")))
                                .build(createRowList("y", "c")),
                        createRowSet("y,y1", "c,a;c,b"),
                        1
                },
                { // regression test -- fetch from left and from right to build result
                        LeftDeepJoin.builder()
                                .add(createRelation("R", "1,2", "c,b;d,d;e,b"),
                                        new ProjectionSpec(singletonList("x1"), singletonList("2")),
                                        new ProjectionSpec(asList("x", "1"), asList("x", "y1")))
                                .build(createRowList("x,x1", "a,b")),
                        createRowSet("x,y1", "a,c;a,e"),
                        1
                },
                {
                        LeftDeepJoin.builder()
                                .add(createRelation("R", "1,2,3",
                                        "i,a,j;k,a,l;g,b,j"),
                                    new ProjectionSpec(singletonList("x"), singletonList("3")),
                                    ProjectionSpec.nop(asList("1", "3")))
                                .addQueryConstants(singletonList("2"), singletonList("a"))
                                .build(createRowList("x,y", "j,i")),
                        createRowSet("1,3", "i,j"),
                        1
                }
        };
    }

    @Test(dataProvider = "data")
    public void testJoin(@Nonnull LeftDeepJoin operation, @Nonnull Set<Row> expected,
                         int queryEntries) {
        BindJoinExecutor executor = new BindJoinExecutor();
        List<QueryEntry> log = new ArrayList<>();
        executor.setLogConsumer(log::add);
        Iterable<Row> result = executor.execute(operation);
        Set<Row> actual = new HashSet<>();
        result.forEach(actual::add);
        assertTrue(RowOps.equals(actual, expected));
        assertEquals(log.size(), queryEntries);
    }
}