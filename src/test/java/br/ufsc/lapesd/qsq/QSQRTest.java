package br.ufsc.lapesd.qsq;

import br.ufsc.lapesd.qsq.rdf.ModelInstanceData;
import br.ufsc.lapesd.qsq.rdf.ModelRelation;
import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.impl.MapInstanceData;
import br.ufsc.lapesd.qsq.relations.impl.MutableArrayListRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.ProgramBuilder;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Pattern;

import static br.ufsc.lapesd.qsq.rules.Term.createConstant;
import static br.ufsc.lapesd.qsq.rules.Term.createVariable;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.concat;
import static org.apache.jena.rdf.model.ResourceFactory.createResource;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class QSQRTest {

    private @Nonnull
    MapInstanceData getAbiteboulData() {
        MutableArrayListRelation up = new MutableArrayListRelation(asList("x", "y"));
        up.addRow(asList("a", "e"));
        up.addRow(asList("a", "f"));
        up.addRow(asList("f", "m"));
        up.addRow(asList("g", "n"));
        up.addRow(asList("h", "n"));
        up.addRow(asList("i", "o"));
        up.addRow(asList("j", "o"));

        MutableArrayListRelation flat = new MutableArrayListRelation(asList("x", "y"));
        flat.addRow(asList("g", "f"));
        flat.addRow(asList("m", "n"));
        flat.addRow(asList("m", "o"));
        flat.addRow(asList("p", "m"));

        MutableArrayListRelation down = new MutableArrayListRelation(asList("x", "y"));
        down.addRow(asList("l", "f"));
        down.addRow(asList("m", "f"));
        down.addRow(asList("g", "b"));
        down.addRow(asList("h", "c"));
        down.addRow(asList("i", "d"));
        down.addRow(asList("p", "k"));

        MapInstanceData data = new MapInstanceData();
        data.set("up", up);
        data.set("flat", flat);
        data.set("down", down);
        return data;
    }

    @Nonnull
    private Program getAbiteboulProgram() {
        return ProgramBuilder.start()
                .beginRule().head("rsg(?x, ?y)").add("flat(?x, ?y)").endRule()
                .beginRule().head("rsg(?x, ?y)")
                .add("up(?x, ?x1)").add("rsg(?y1, ?x1)").add("down(?y1, ?y)").endRule()
                .build();
    }

    @Test
    public void testAbiteboulExtensional() {
        QSQR qsq = new QSQR(getAbiteboulProgram(), getAbiteboulData(),
                new PlainAtom("up", createConstant("a"), createVariable("y")));
        Relation results = qsq.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        List<String> columns = singletonList("y");
        Set<Row> expected = new HashSet<>(asList(new IndexedListRow(columns, singletonList("e")),
                                                 new IndexedListRow(columns, singletonList("f"))));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void testAbiteboulExample() {
        QSQR qsq = new QSQR(getAbiteboulProgram(), getAbiteboulData(),
                new PlainAtom("rsg", createConstant("a"), createVariable("y")));
//        qsq.setLogEnabled(true);
//        qsq.setInteractionCtl(new LanternaDisplay());
        Relation results = qsq.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = new HashSet<>(asList(
                new IndexedListRow(singletonList("y"), singletonList("b")),
                new IndexedListRow(singletonList("y"), singletonList("c")),
                new IndexedListRow(singletonList("y"), singletonList("d"))
        ));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void testAbiteboulExample2() {
        QSQR qsq = new QSQR(getAbiteboulProgram(), getAbiteboulData(),
                new PlainAtom("rsg", createVariable("x"), createConstant("f")));
//        qsq.setInteractionCtl(new LanternaDisplay());
        Relation results = qsq.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = new HashSet<>(asList(
                new IndexedListRow(singletonList("x"), singletonList("g")),
                new IndexedListRow(singletonList("x"), singletonList("h")),
                new IndexedListRow(singletonList("x"), singletonList("i")),
                new IndexedListRow(singletonList("x"), singletonList("j"))
        ));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void testGetFreeFromBoundRule() {
        Program program = ProgramBuilder.start()
                .beginRule().head("rsg(?x, ?y)").add("flat(?x, ?y)").endRule()
                .beginRule().head("rsg(?x,  2)").add("flat(?x, n)").endRule()
                .build();
        QSQR qsq = new QSQR(program, getAbiteboulData(),
                new PlainAtom("rsg", createConstant("m"), createVariable("y")));
//        qsq.setInteractionCtl(new LanternaDisplay());
        Relation results = qsq.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = new HashSet<>(asList(
                new IndexedListRow(singletonList("y"), singletonList("n")),
                new IndexedListRow(singletonList("y"), singletonList("o")),
                new IndexedListRow(singletonList("y"), singletonList("2"))
        ));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    private ModelInstanceData parseTurtle(@Nonnull String filename) throws IOException {
        try (InputStream input = getClass().getResourceAsStream(filename)) {
            Model model = ModelFactory.createDefaultModel();
            RDFDataMgr.read(model, input, Lang.TTL);
            return new ModelInstanceData("T", model);
        }
    }

    public static final String EX = "http://example.org/ns#";

    protected @Nonnull
    Resource exResource(@Nonnull String localName) {
        final Pattern pattern = Pattern.compile("^https?://.*");
        if (pattern.matcher(localName).matches())
            return createResource(localName);
        else
            return createResource(EX+localName);
    }

    protected @Nonnull List<Resource> exResources(@Nonnull String... localNames) {
        return Arrays.stream(localNames).map(this::exResource).collect(toList());
    }

    @Test
    public void  testGetAllExplicit() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        /* Variable names in the query match the defaults for ModelRelation */
        PlainAtom query = new PlainAtom("T",
                createVariable("s"), createVariable("p"), createVariable("o"));
        Program program = ProgramBuilder.start().build();
        QSQR qsqr = new QSQR(program, data, query);
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = requireNonNull(data.get("T")).stream().collect(toSet());
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void testRDFS10() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        PlainAtom query = new PlainAtom("T", createVariable("x"),
                createConstant(RDFS.subClassOf), createConstant(exResource("C3")));
        Program program = ProgramBuilder.start().beginRule("rdfs10")
                .head("T", createVariable("x"), RDFS.subClassOf, createVariable("x"))
                .add("T", createVariable("x"), RDF.type, RDFS.Class).endRule().build();
        QSQR qsqr = new QSQR(program, data, query);
//        qsqr.setInteractionCtl(new LanternaDisplay());
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = Collections.singleton(
                new IndexedListRow(singletonList("x"), singletonList(exResource("C3"))));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void  testGetAllRDFS10() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        /* Variable names in the query match the defaults for ModelRelation */
        PlainAtom query = new PlainAtom("T",
                createVariable("s"), createVariable("p"), createVariable("o"));
        /* T(?x, rdfs:subClassOf, ?x) <- T(?x, a, rdfs:Class) */
        Program program = ProgramBuilder.start().beginRule("rdfs10")
                .head("T", createVariable("x"), RDFS.subClassOf, createVariable("x"))
                .add("T", createVariable("x"), RDF.type, RDFS.Class).endRule().build();
        QSQR qsqr = new QSQR(program, data, query);
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());

        Resource c1 = exResource("C1");
        Resource c2 = exResource("C2");
        Resource c3 = exResource("C3");
        Resource d2 = exResource("D2");
        Resource d3 = exResource("D3");
        Resource e1 = exResource("E1");
        Resource e2 = exResource("E2");
        Resource e3 = exResource("E3");
        List<IndexedListRow> derived = asList(
                new IndexedListRow(ModelRelation.SPO, asList(c1, RDFS.subClassOf, c1)),
                new IndexedListRow(ModelRelation.SPO, asList(c2, RDFS.subClassOf, c2)),
                new IndexedListRow(ModelRelation.SPO, asList(c3, RDFS.subClassOf, c3)),
                new IndexedListRow(ModelRelation.SPO, asList(d2, RDFS.subClassOf, d2)),
                new IndexedListRow(ModelRelation.SPO, asList(d3, RDFS.subClassOf, d3)),
                new IndexedListRow(ModelRelation.SPO, asList(e1, RDFS.subClassOf, e1)),
                new IndexedListRow(ModelRelation.SPO, asList(e2, RDFS.subClassOf, e2)),
                new IndexedListRow(ModelRelation.SPO, asList(e3, RDFS.subClassOf, e3))
        );
        Set<Row> expected = concat(requireNonNull(data.get("T")).stream(), derived.stream())
                .collect(toSet());
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void testRDFSExplicitType() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        PlainAtom query = new PlainAtom("T", createVariable("x"),
                createConstant(RDF.type), createConstant(exResource("C3")));
        Program program = ProgramBuilder.start().beginRule("rdfs9")
                .head("T", createVariable("z"), RDF.type, createVariable("y"))
                .add("T", createVariable("x"), RDFS.subClassOf, createVariable("y"))
                .add("T", createVariable("z"), RDF.type, createVariable("x"))
                .endRule().build();
        QSQR qsqr = new QSQR(program, data, query);
//        qsqr.setInteractionCtl(new LanternaDisplay());
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = Collections.singleton(
                new IndexedListRow(singletonList("x"), singletonList(exResource("i3"))));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void testAllTypes() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        PlainAtom query = new PlainAtom("T", createVariable("x"),
                createConstant(RDF.type), createVariable("y"));
        Program program = ProgramBuilder.start().beginRule("rdfs9")
                .head("T", createVariable("z"), RDF.type, createVariable("y"))
                .add("T", createVariable("x"), RDFS.subClassOf, createVariable("y"))
                .add("T", createVariable("z"), RDF.type, createVariable("x"))
                .endRule().build();
        QSQR qsqr = new QSQR(program, data, query);
//        qsqr.setInteractionCtl(new LanternaDisplay());
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        IndexedListRow.Factory fac = new IndexedListRow.Factory(asList("x", "y"));
        Set<Row> expected = new HashSet<>(asList(
                fac.create(asList(exResource("C1"), RDFS.Class)),
                fac.create(asList(exResource("C2"), RDFS.Class)),
                fac.create(asList(exResource("C3"), RDFS.Class)),
                fac.create(asList(exResource("D2"), RDFS.Class)),
                fac.create(asList(exResource("D3"), RDFS.Class)),
                fac.create(asList(exResource("E1"), RDFS.Class)),
                fac.create(asList(exResource("E2"), RDFS.Class)),
                fac.create(asList(exResource("E3"), RDFS.Class)),
                fac.create(asList(exResource("p1"), RDF.Property)),
                fac.create(asList(exResource("p2"), RDF.Property)),

                fac.create(asList(exResource("i1"), exResource("C1"))),
                fac.create(asList(exResource("i2"), exResource("C2"))),
                fac.create(asList(exResource("i2"), exResource("C1"))),
                fac.create(asList(exResource("i3"), exResource("C3"))),
                fac.create(asList(exResource("i3"), exResource("C2"))),
                fac.create(asList(exResource("i4"), exResource("D2"))),
                fac.create(asList(exResource("i4"), exResource("C1"))),
                fac.create(asList(exResource("i5"), exResource("D3"))),
                fac.create(asList(exResource("i5"), exResource("D2"))),

                //recursive applications of rdfs9:
                fac.create(asList(exResource("i3"), exResource("C1"))),
                fac.create(asList(exResource("i5"), exResource("C1")))

//                fac.create(asList(exResource("j1"), exResource("E2"))),
//                fac.create(asList(exResource("j1"), exResource("E1"))),
//                fac.create(asList(exResource("j2"), exResource("E3")))
        ));
        assertEquals(RowOps.missing(resultsSet, expected), emptySet(), "extra results");
        assertEquals(RowOps.missing(expected, resultsSet), emptySet(), "missing results");
    }

    private static Object[][] createProgramVariations(@Nonnull Object[][] data,
                                                      @Nonnull Program... programs) {

        Object[][] expanded = new Object[data.length*2][];
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < programs.length; j++) {
                Object[] row = new Object[data[i].length+1];
                row[0] = programs[j];
                System.arraycopy(data[i], 0, row, 1, data[i].length);
                expanded[i*programs.length + j] = row;
            }
        }
        return expanded;
    }

    @DataProvider
    public Object[][] rdfsSubjects() {
        Object[][] base = {
                {RDFS.subClassOf, exResource("C3"), exResources("C3")},
                {RDFS.subClassOf, exResource("C2"), exResources("C3", "C2")},
                {RDFS.subClassOf, exResource("C1"),
                        exResources("C1", "C3", "C2", "D2", "D3")},
                {RDFS.subClassOf, exResource("i3"), exResources()},

                {RDF.type, exResource("C3"), exResources("i3")},
                {RDF.type, exResource("C1"),
                        exResources("i1", "i2", "i3", "i4", "i5")},
                {RDF.type, exResource("C2"), exResources("i2", "i3")},
                {RDF.type, exResource("D2"), exResources("i4", "i5")},
                {RDF.type, exResource("D3"), exResources("i5")},

                {RDF.type, exResource("E2"), exResources("j1", "k1")},
                {RDF.type, exResource("E1"), exResources("j1", "k1")},
                {RDF.type, exResource("E3"), exResources("j2", "k2")},
        };
        return createProgramVariations(base, RDFPrograms.RDFS_T,
                RDFPrograms.RDFS_FULL);
    }

    @Test
    public void testRdfs2() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        Term x = createVariable("x"), y = createVariable("y"), z = createVariable("z");
        Term a = createVariable("a");
        PlainAtom query = new PlainAtom("T", x, createConstant(RDF.type),
                createConstant(exResource("E2")));
        Program program = ProgramBuilder.start().beginRule("rdfs2")
                .head("T", y, RDF.type, x)
                .add("T", a, RDFS.domain, x)
                .add("T", y, a, z).endRule().build();
        QSQR qsqr = new QSQR(program, data, query);
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = Collections.singleton(
                new IndexedListRow(singletonList("x"), singletonList(exResource("j1"))));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test
    public void testRdfs2_7_9() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        Term x = createVariable("x"), y = createVariable("y"), z = createVariable("z");
        Term a = createVariable("a"), b = createVariable("b");
        PlainAtom query = new PlainAtom("T", x, createConstant(RDF.type),
                createConstant(exResource("E1")));
        Program program = ProgramBuilder.start()
                .beginRule("rdfs2")
                    .head("T", y, RDF.type, x)
                    .add("T", a, RDFS.domain, x)
                    .add("T", y, a, z).endRule()
                .beginRule("rdfs7")
                    .head("T", x, b, y)
                    .add("T", a, RDFS.subPropertyOf, b)
                    .add("T", x, a, y).endRule()
                .beginRule("rdfs9")
                    .head("T", z, RDF.type, y)
                    .add("T", x, RDFS.subClassOf, y)
                    .add("T", z, RDF.type, x).endRule()
                .build();
        QSQR qsqr = new QSQR(program, data, query);
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = new HashSet<>(asList(
                new IndexedListRow(singletonList("x"), singletonList(exResource("j1"))),
                new IndexedListRow(singletonList("x"), singletonList(exResource("k1")))));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @Test(dataProvider = "rdfsSubjects")
    public void testSubjectsRDFS(@Nonnull Program program, @Nonnull Property p, @Nonnull RDFNode o,
                                 @Nonnull List<Resource> subjects) throws IOException {
        PlainAtom query = new PlainAtom("T", createVariable("x"),
                createConstant(p), createConstant(o));
        QSQR qsqr = new QSQR(program, parseTurtle("rdfs-inference.ttl"), query);
//        qsqr.setInteractionCtl(new LanternaDisplay());
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = subjects.stream()
                .map(s -> new IndexedListRow(singletonList("x"), singletonList(s)))
                .collect(toSet());
        assertEquals(RowOps.missing(resultsSet, expected), emptySet(), "extra results");
        assertEquals(RowOps.missing(expected, resultsSet), emptySet(), "missing results");
    }

    @Test
    public void testRdfs7() throws IOException {
        ModelInstanceData data = parseTurtle("rdfs-inference.ttl");
        Term x = createVariable("x"), y = createVariable("y");
        Term a = createVariable("a"), b = createVariable("b");
        PlainAtom query = new PlainAtom("T", createConstant(exResource("k1")),
                createConstant(exResource("p1")), x);
        Program program = ProgramBuilder.start().beginRule("rdfs7")
                .head("T", x, b, y)
                .add("T", a, RDFS.subPropertyOf, b)
                .add("T", x, a, y).endRule().build();
        QSQR qsqr = new QSQR(program, data, query);
//        qsqr.setInteractionCtl(new LanternaDisplay());
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = Collections.singleton(
                new IndexedListRow(singletonList("x"), singletonList(exResource("k2"))));
        assertTrue(RowOps.equals(resultsSet, expected));
    }

    @DataProvider
    public Object[][] rdfsObjects() {
        Program complete = RDFPrograms.RDFS_FULL;
        Program simple = RDFPrograms.RDFS_T;
        String resource = RDFS.Resource.getURI();
        Property p1 = ResourceFactory.createProperty(exResource("p1").getURI());
        return new Object[][] {
                {simple, RDF.type, exResource("i1"), exResources("C1")},
                {simple, RDF.type, exResource("j1"), exResources("E2", "E1")},
                {simple, RDF.type, exResource("k1"), exResources("E2", "E1")},
                {simple, RDF.type, exResource("k2"), exResources("E3")},

                // derivations from subClassOf axioms
                {complete, RDF.type, exResource("i1"), exResources("C1", resource)},
                {complete, RDF.type, exResource("i2"), exResources("C1", "C2", resource)},
                {complete, RDF.type, exResource("i5"), exResources("D3", "D2", "C1", resource)},

                // derivations from domain/range axioms
                {complete, RDF.type, exResource("j1"), exResources("E2", "E1", resource)},
                {complete, RDF.type, exResource("j2"), exResources("E3", resource)},

                // derivations from subPropertyOf axioms
                {complete, p1, exResource("k1"), exResources("k2")},
                {complete, RDF.type, exResource("k1"), exResources("E2", "E1", resource)},
                {complete, RDF.type, exResource("k2"), exResources("E3", resource)},
        };
    }

    @Test(dataProvider = "rdfsObjects")
    public void testObjectsRDFS(@Nonnull Program program, @Nonnull Property p, @Nonnull Resource s,
                                 @Nonnull List<Resource> objects) throws IOException {
        PlainAtom query = new PlainAtom("T",
                createConstant(s), createConstant(p), createVariable("x"));
        QSQR qsqr = new QSQR(program, parseTurtle("rdfs-inference.ttl"), query);
//        qsqr.setInteractionCtl(new LanternaDisplay());
        Relation results = qsqr.run();
        Set<Row> resultsSet = results.stream().collect(toSet());
        Set<Row> expected = objects.stream()
                .map(o -> new IndexedListRow(singletonList("x"), singletonList(o)))
                .collect(toSet());
        assertTrue(RowOps.equals(resultsSet, expected));
    }
}