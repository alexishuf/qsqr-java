package br.ufsc.lapesd.qsq.rules;

import br.ufsc.lapesd.qsq.rules.parser.DatalogParser;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

public class CompositeProgramTest {
    private Program p1, p2, pQuery;
    private Rule r1, r2, r3, query;

    @BeforeMethod
    public void setUp() throws DatalogParser.SyntaxException {
        DatalogParser parser = new DatalogParser();
        p1 = parser.parseProgram("R1(?x, a) <- R1(?x, b)");
        r1 = p1.getRule(0);
        p2 = parser.parseProgram("R2(?x, a) <- R2(?x, b)\n" +
                                 "R3(?x, a) <- R3(?x, b)\n");
        r2 = p2.getRule(0);
        r3 = p2.getRule(1);
        pQuery = parser.parseProgram("Q(?x, a) <- R1(?x, b), R2(?x, b)");
        query = pQuery.getRule(0);
    }

    @Test
    public void testSize() {
        assertEquals(new CompositeProgram(Collections.singletonList(p2)).getRuleCount(), 2);
        assertEquals(new CompositeProgram(Arrays.asList(p1, pQuery)).getRuleCount(), 2);
        assertEquals(new CompositeProgram(Arrays.asList(p1, p2)).getRuleCount(), 3);
    }

    @Test
    public void testGetRule() {
        CompositeProgram c1 = new CompositeProgram(Collections.singletonList(p2));
        assertThrows(IndexOutOfBoundsException.class, () -> c1.getRule(-1));
        assertEquals(c1.getRule(0), r2);
        assertEquals(c1.getRule(1), r3);
        assertThrows(IndexOutOfBoundsException.class, () -> c1.getRule(2));

        CompositeProgram c2 = new CompositeProgram(Arrays.asList(p1, p2, pQuery));
        assertEquals(c2.getRule(0), r1);
        assertEquals(c2.getRule(1), r2);
        assertEquals(c2.getRule(2), r3);
        assertEquals(c2.getRule(3), query);
        assertThrows(IndexOutOfBoundsException.class, () -> c2.getRule(4));

        CompositeProgram c3 = new CompositeProgram(Arrays.asList(p2, p1, pQuery));
        assertEquals(c3.getRule(0), r2);
        assertEquals(c3.getRule(1), r3);
        assertEquals(c3.getRule(2), r1);
        assertEquals(c3.getRule(3), query);
        assertThrows(IndexOutOfBoundsException.class, () -> c3.getRule(4));
    }

    @Test
    public void testStreamRules() {
        CompositeProgram c = new CompositeProgram(Arrays.asList(p2, p1, pQuery));
        List<Rule> actual = c.streamRules().collect(Collectors.toList());
        List<Rule> expected = Stream.of(r2, r3, r1, query).collect(Collectors.toList());
        assertEquals(actual, expected);
    }

    @Test
    public void testIterateRules() {
        CompositeProgram c = new CompositeProgram(Arrays.asList(p2, p1, pQuery));
        List<Rule> actual = new ArrayList<>();
        for (Rule rule : c.getRules()) actual.add(rule);
        List<Rule> expected = Stream.of(r2, r3, r1, query).collect(Collectors.toList());
        assertEquals(actual, expected);
    }

}