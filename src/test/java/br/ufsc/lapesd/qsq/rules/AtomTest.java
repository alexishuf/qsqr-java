package br.ufsc.lapesd.qsq.rules;

import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.testng.Assert.*;

public class AtomTest {
    @Test
    public void testConstructor() {
        assertThrows(IllegalArgumentException.class,
                () -> new PlainAtom("pred", new ArrayList<>()));
        assertThrows(IllegalArgumentException.class,
                () -> new PlainAtom("", singletonList(Term.createVariable("x"))));
    }

    @Test
    public void testGetters() {
        Atom atom = new PlainAtom("predicate",
                Term.createVariable("x"), Term.createConstant("a"));
        assertEquals(atom.getPredicate(), "predicate");

        Collection<Term> expected = asList(Term.createVariable("x"), Term.createConstant("a"));
        assertEquals(atom.getTerms(), expected);
    }

    @Test
    public void testEquals() {
        PlainAtom a0 = new PlainAtom("pred", Term.createVariable("x"));
        PlainAtom a1 = new PlainAtom("pred", Term.createVariable("x"));
        Atom a2 = new PlainAtom("pred", Term.createVariable("y"));
        Atom a3 = new PlainAtom("pred2", Term.createVariable("y"));

        assertEquals(a0, a1);
        assertEquals(a1, a0);
        assertEquals(a0.hashCode(), a1.hashCode());
        assertEquals(a1.hashCode(), a0.hashCode());

        assertNotEquals(a0, a2);
        assertNotEquals(a0, a3);
        assertNotEquals(a2, a0);
        assertNotEquals(a3, a0);
    }
}