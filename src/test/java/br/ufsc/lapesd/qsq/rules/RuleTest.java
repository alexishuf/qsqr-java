package br.ufsc.lapesd.qsq.rules;

import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import br.ufsc.lapesd.qsq.rules.plain.PlainRule;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.testng.Assert.*;

public class RuleTest {
    private Term x, y, a, b;

    @BeforeMethod
    public void setUp() {
        x = Term.createVariable("x");
        y = Term.createVariable("y");
        a = Term.createVariable("a");
        b = Term.createVariable("b");
    }

    @Test
    public void testGetters() {
        Rule rule = new PlainRule("name", new PlainAtom("R0", x, y),
                new PlainAtom("R1", x, a), new PlainAtom("R2", y, b));
        assertEquals(rule.getName(), "name");
        assertEquals(rule.getHead().getPredicate(), "R0");
        assertEquals(rule.getPredicate(), "R0");

        List<Atom> body = Arrays.asList(new PlainAtom("R1", x, a),
                new PlainAtom("R2", y, b));
        assertEquals(rule.getBody(), body);
    }

    @Test
    public void testAllowEmptyBody() {
        Rule rule1 = new PlainRule(new PlainAtom("R0", x, y));
        Rule rule2 = new PlainRule(new PlainAtom("R0", x, y), emptyList());
        assertTrue(rule1.getBody().isEmpty());
        assertTrue(rule2.getBody().isEmpty());
    }

    @Test
    public void testEquals() {
        PlainRule rule0 = new PlainRule(new PlainAtom("R0", x, y));
        PlainRule rule1 = new PlainRule(new PlainAtom("R0", x, y), emptyList());
        Rule rule2 = new PlainRule(new PlainAtom("R1", x, y), emptyList());
        Rule rule3 = new PlainRule(new PlainAtom("R0", x, a), emptyList());
        PlainRule rule4 = new PlainRule("name", new PlainAtom("R0", x, y));
        PlainRule rule5 = new PlainRule("name", new PlainAtom("R0", x, y), emptyList());
        Rule rule6 = new PlainRule("name1", new PlainAtom("R0", x, y), emptyList());
        PlainRule rule7 = new PlainRule(new PlainAtom("R0", x, y), new PlainAtom("R1", x, a));
        PlainRule rule8 = new PlainRule(new PlainAtom("R0", x, y), new PlainAtom("R1", x, a));
        Rule rule9 = new PlainRule(new PlainAtom("R0", x, y), new PlainAtom("R2", x, a));

        assertEquals(rule0, rule1);
        assertEquals(rule1, rule0);
        assertNotEquals(rule0, rule2);
        assertNotEquals(rule0, rule3);
        assertNotEquals(rule1, rule2);
        assertNotEquals(rule1, rule3);

        assertNotEquals(rule0, rule4);
        assertEquals(rule4, rule5);
        assertNotEquals(rule4, rule6);
        assertNotEquals(rule5, rule6);
        assertNotEquals(rule6, rule4);
        assertNotEquals(rule6, rule5);

        assertNotEquals(rule0, rule7);
        assertEquals(rule7, rule8);
        assertNotEquals(rule7, rule9);


        assertEquals(rule0.hashCode(), rule1.hashCode());
        assertEquals(rule1.hashCode(), rule0.hashCode());
        assertEquals(rule4.hashCode(), rule5.hashCode());
        assertEquals(rule7.hashCode(), rule8.hashCode());
    }
}