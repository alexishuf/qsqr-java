package br.ufsc.lapesd.qsq.rules;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;

import static org.testng.Assert.*;

public class TermTest {
    @DataProvider
    public Object[][] equalsData() {
        return new Object[][] {
                {Term.createVariable("x"), Term.createVariable("x"), true},
                {Term.createVariable("x"), Term.createVariable("y"), false},
                {Term.createConstant("x"), Term.createConstant("x"), true},
                {Term.createConstant("x"), Term.createConstant("y"), false},
                {Term.createVariable("x"), Term.createConstant("x"), false},
                {Term.createConstant("x"), Term.createVariable("x"), false},
        };
    }


    @Test(dataProvider = "equalsData")
    public void testEquals(@Nonnull Term l, @Nonnull Term r, boolean equals) {
        assertEquals(l.equals(r), equals);
        if (equals)
            assertEquals(l.hashCode(), r.hashCode());
    }

    @Test
    public void testGetName() {
        Term var = Term.createVariable("z");
        assertTrue(var.isVariable());
        assertEquals(var.getName(), "z");

        Term constant = Term.createConstant("k");
        assertFalse(constant.isVariable());
        assertThrows(IllegalArgumentException.class, constant::getName);
    }

    @Test
    public void testGetValue() {
        Term x = Term.createVariable("x");
        assertEquals(x.getValue(), "x");

        Term c = Term.createConstant("c");
        assertEquals(c.getValue(), "c");
    }
}