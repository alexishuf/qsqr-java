package br.ufsc.lapesd.qsq.rules.adornment;

import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.parser.DatalogParser;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import br.ufsc.lapesd.qsq.rules.plain.PlainRule;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.*;

public class AdornedRuleTest {
    private @Nonnull final Term x = Term.createVariable("x");
    private @Nonnull final Term y = Term.createVariable("y");

    @Test
    public void testAdornUnit() {
        PlainRule plain = new PlainRule("rule x", new PlainAtom("R", x, y));
        AdornedRule rule = new AdornedRule(plain, "fb");

        assertEquals(rule.getAdornment(), new Adornment("fb"));
        assertEquals(rule.getName(), "rule x");

        assertNotEquals(rule.getHead(), plain.getHead());
        assertEquals(((AdornedAtom)rule.getHead()).getAdornment(), new Adornment("fb"));

        assertTrue(rule.getBody().isEmpty());
    }

    @DataProvider
    public Object[][] adornedData() {
        return new Object[][] {
                {"R(?x, ?y, a) <- R(?x, ?y)", "bfb", "bf"},
                {"R(?x, ?y, a) <- R(?y, ?x)", "bfb", "fb"},
                {"R(?x, ?y, a) <- R(a, ?y, ?x)", "bfb", "bfb"},
                {"R(?x, ?y, a) <- R(b, ?y, ?x)", "bfb", "bfb"},
                {"R(?x, ?y, a) <- R1(?x, ?y), R2(?x, a, b), R3(?y, ?x)", "bfb", "bf;bbb;fb"},
        };
    }

    @Test(dataProvider = "adornedData")
    public void testBodyAdornments(@Nonnull String ruleString,
                                   @Nonnull String adornmentString,
                                   @Nonnull String bodyAdornmentList)
            throws DatalogParser.SyntaxException {
        Rule rule = new DatalogParser().parseRule(ruleString);
        assertNotNull(rule);
        Adornment adornment = new Adornment(adornmentString);
        List<Adornment> bodyAdornments = Arrays.stream(bodyAdornmentList.split(";"))
                .map(Adornment::new).collect(Collectors.toList());

        AdornedRule adornedRule = new AdornedRule(rule, adornment);
        assertEquals(((AdornedAtom)adornedRule.getHead()).getAdornment(),
                adornedRule.getAdornment());
        assertEquals(adornedRule.getAdornment(), adornment);

        assertEquals(adornedRule.getBody().size(), rule.getBody().size());

        for (int i = 0; i < bodyAdornments.size(); i++) {
            Adornment expected = bodyAdornments.get(i);
            Adornment actual = ((AdornedAtom) adornedRule.getBody().get(i)).getAdornment();
            assertEquals(actual, expected, "adornment mismatch at i=" + i);
        }
    }

}