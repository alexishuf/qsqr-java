package br.ufsc.lapesd.qsq.rules.adornment;

import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collections;

import static br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom.adorn;
import static org.testng.Assert.*;

public class AdornedAtomTest {

    private final @Nonnull Term x = Term.createVariable("x");
    private final @Nonnull Term y = Term.createVariable("y");
    private final @Nonnull Term a = Term.createConstant("a");
    private final @Nonnull Term b = Term.createConstant("b");

    @Test
    public void testGetPlain() {
        AdornedAtom atom = adorn(new PlainAtom("R", x, a), "fb");
        assertEquals(atom.getAtom(), new PlainAtom("R", x, a));
    }

    @Test
    public void testEquals() {
        AdornedAtom a1 = adorn(new PlainAtom("R", x, y), "fb");
        AdornedAtom a2 = adorn(new PlainAtom("R", x, y), "fb");
        AdornedAtom a3 = adorn(new PlainAtom("R", x, a), "fb");
        AdornedAtom a4 = adorn(new PlainAtom("R", x, a), "fb");
        AdornedAtom a5 = adorn(new PlainAtom("R", x, b), "fb");
        AdornedAtom a6 = adorn(new PlainAtom("Q", x, a), "fb");

        assertEquals(a1, a2);
        assertEquals(a3, a4);
        assertEquals(a1.hashCode(), a2.hashCode());

        assertNotEquals(a1, a5);
        assertNotEquals(a3, a5);
        assertNotEquals(a3, a6);

        assertNotEquals(a1, new PlainAtom("R", x, y));
        assertNotEquals(a3, new PlainAtom("R", x, a));
    }

    @Test
    public void testConstantsMustBeBound() {
        assertThrows(IllegalArgumentException.class, () ->
                adorn(new PlainAtom("R", x, a), "bf"));
        assertThrows(IllegalArgumentException.class, () ->
                adorn(new PlainAtom("R", x, a), "ff"));
    }

    @Test
    public void testGetFree() {
        AdornedAtom ax = adorn(new PlainAtom("R", x, y, a), "fbb");
        AdornedAtom ay = adorn(new PlainAtom("R", x, y, a), "bfb");
        AdornedAtom axy = adorn(new PlainAtom("R", x, y, a), "ffb");

        assertEquals(ax.getFree(), Collections.singletonList(x));
        assertEquals(ay.getFree(), Collections.singletonList(y));
        assertEquals(axy.getFree(), Arrays.asList(x, y));
    }

    @Test
    public void testGetBound() {
        AdornedAtom ax = adorn(new PlainAtom("R", x, y, a), "fbb");
        AdornedAtom ay = adorn(new PlainAtom("R", x, y, a), "bfb");
        AdornedAtom axy = adorn(new PlainAtom("R", x, y, a), "ffb");

        assertEquals(ax.getBound(), Arrays.asList(y, a));
        assertEquals(ay.getBound(), Arrays.asList(x, a));
        assertEquals(axy.getBound(), Collections.singletonList(a));
    }

}