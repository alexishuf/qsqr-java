package br.ufsc.lapesd.qsq.rules.adornment;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;

import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.BOUND;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.FREE;
import static org.testng.Assert.*;

public class AdornmentTest {
    @Test
    public void testInvalidStrings() {
        assertThrows(IllegalArgumentException.class, () -> new Adornment(""));
        assertThrows(IllegalArgumentException.class, () -> new Adornment("x"));
        assertThrows(IllegalArgumentException.class, () -> new Adornment("fbx"));
    }

    @Test
    public void testEquals() {
        assertEquals(new Adornment("fb"), new Adornment("fb"));
        assertEquals(new Adornment("fb"), new Adornment(Arrays.asList(FREE, BOUND)));
        assertEquals(new Adornment("fb"), new Adornment(FREE, BOUND));

        assertEquals(new Adornment("fb").hashCode(), new Adornment("fb").hashCode());
        assertEquals(new Adornment("fb").hashCode(),
                new Adornment(Arrays.asList(FREE, BOUND)).hashCode());
        assertEquals(new Adornment("fb").hashCode(), new Adornment(FREE, BOUND).hashCode());

        assertNotEquals(new Adornment("fb"), new Adornment("bf"));
        assertNotEquals(new Adornment("fb"), new Adornment("fbb"));
        assertNotEquals(new Adornment("fb"), new Adornment("f"));
    }

    @DataProvider
    public Object[][] boundData() {
        return new Object[][] {
                {"f", new int[]{}},
                {"fb", new int[]{1}},
                {"fbf", new int[]{1}},
                {"fbfbbf", new int[]{1,3, 4}},
        };
    }

    @Test(dataProvider = "boundData")
    public void testGetBound(@Nonnull String string, @Nonnull int[] bound) {
        Adornment adornment = new Adornment(string);
        assertEquals(adornment.getBound(), bound);
    }

    @Test(dataProvider = "boundData")
    public void testGetFree(@Nonnull String string, @Nonnull int[] free) {
        string = string.replaceAll("b", "x").replaceAll("f", "b").replaceAll("x", "f");
        Adornment adornment = new Adornment(string);
        assertEquals(adornment.getFree(), free);
    }
}