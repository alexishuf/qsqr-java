package br.ufsc.lapesd.qsq.rules;

import br.ufsc.lapesd.qsq.rules.parser.DatalogParser;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static br.ufsc.lapesd.qsq.rules.Term.createConstant;
import static br.ufsc.lapesd.qsq.rules.Term.createVariable;
import static org.testng.Assert.assertEquals;

public class ProgramBuilderTest {
    private DatalogParser parser;

    @BeforeMethod
    public void setUp() {
        parser = new DatalogParser();
    }

    @Test
    public void testCreateUnitRule() throws DatalogParser.SyntaxException {
        Program program = ProgramBuilder.start().beginRule()
                .head("R", "a", "b").endRule()
                .build();
        Program expected = parser.parseProgram("R(a, b) <- .");
        assertEquals(program, expected);
    }

    @Test
    public void testCreateUnitRuleWithVar() throws DatalogParser.SyntaxException {
        Program program = ProgramBuilder.start().beginRule()
                .head("R", "?x", "b").endRule()
                .build();
        Program expected = parser.parseProgram("R(?x, b) <- .");
        assertEquals(program, expected);
    }


    @Test
    public void testCreateUnitRuleWithExplicitTerms() throws DatalogParser.SyntaxException {
        Program program = ProgramBuilder.start().beginRule()
                .head("R", createVariable("x"), createConstant("a")).endRule()
                .build();
        Program expected = parser.parseProgram("R(?x, a) <- .");
        assertEquals(program, expected);
    }

    @Test
    public void testCreateSingleAtomBody() throws DatalogParser.SyntaxException {
        Program program = ProgramBuilder.start().beginRule()
                .head("R", "?x", "a")
                .add("R1", "?x", "b").endRule()
                .build();
        Program expected = parser.parseProgram("R(?x, a) <- R1(?x, b)");
        assertEquals(program, expected);
    }

    @Test
    public void testCreateTwoAtomsBody() throws DatalogParser.SyntaxException {
        Program program = ProgramBuilder.start().beginRule()
                .head("R", "?x", "a")
                .add("R1", "?x", "?y")
                .add("R2", "?y", "b").endRule()
                .build();
        Program expected = parser.parseProgram("R(?x, a) <- R1(?x, ?y), R2(?y, b)");
        assertEquals(program, expected);
    }

    @Test
    public void testCreateTwoRules() throws DatalogParser.SyntaxException {
        Program program = ProgramBuilder.start().beginRule()
                .head("R", "?x", "a")
                .add("R1", "?x", "?y")
                .add("R2", "?y", "b").endRule()
                .beginRule("rule 2").head("R3", "?x", "?y")
                .add("R4", "?y", "?x").endRule()
                .build();
        Program expected = parser.parseProgram("R(?x, a) <- R1(?x, ?y), R2(?y, b)\n" +
                "[rule 2] R3(?x, ?y) <- R4(?y, ?x)");
        assertEquals(program, expected);
    }

    @Test
    public void testCreateCompactSyntax() throws DatalogParser.SyntaxException {
        Program program = ProgramBuilder.start().beginRule()
                .head("R(?x, a)")
                .add("R1(b, ?x)").endRule()
                .build();
        Program expected = parser.parseProgram("R(?x, a) <- R1(b, ?x).");
        assertEquals(program, expected);
    }
}