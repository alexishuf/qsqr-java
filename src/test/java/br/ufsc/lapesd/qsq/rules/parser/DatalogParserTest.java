package br.ufsc.lapesd.qsq.rules.parser;

import br.ufsc.lapesd.qsq.rdf.JenaConstantParser;
import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import br.ufsc.lapesd.qsq.rules.plain.PlainRule;
import org.apache.jena.vocabulary.OWL2;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.qsq.rules.Term.createConstant;
import static br.ufsc.lapesd.qsq.rules.Term.createVariable;
import static org.testng.Assert.*;

public class DatalogParserTest {
    private DatalogParser parser;

    @BeforeMethod
    public void setUp() {
        parser = new DatalogParser();
    }

    @DataProvider
    public Object[][] ruleData() {
        Term x = createVariable("x"), y = createVariable("y"), z = createVariable("z");
        Term a = createConstant("a"), b = createConstant("b"), c = createConstant("c");
        PlainAtom Rx = new PlainAtom("R", x);
        PlainAtom Rxy = new PlainAtom("R1", x, y);
        PlainAtom Rxa = new PlainAtom("R2", x, a);
        PlainAtom Rby = new PlainAtom("R3", b, y);
        Atom Rxya = new PlainAtom("R4", x, y, a);

        return new Object[][] {
                {"", null},
                {"  ", null},
                {"  \n", null},
                {"  \r\n", null},
                {"R(?x) <- ", new PlainRule(Rx)},
                {"R(?x) <- .", new PlainRule(Rx)},
                {"R2(?x, a) <- .", new PlainRule(Rxa)},
                {"R3(b, ?y) <- ", new PlainRule(Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y)", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y).", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R4(?x, ?y, a), R2(?x, a), R3(b, ?y)", new PlainRule(Rxy, Rxya, Rxa, Rby)},

                {"[rule9] R1(?x, ?y) <- R2(?x, a), R3(b, ?y)", new PlainRule("rule9", Rxy, Rxa, Rby)},
                {"[rule 9] R1(?x, ?y) <- R2(?x, a), R3(b, ?y)", new PlainRule("rule 9", Rxy, Rxa, Rby)},
                {"[ rule 9] R1(?x, ?y) <- R2(?x, a), R3(b, ?y)", new PlainRule(" rule 9", Rxy, Rxa, Rby)},
                {"rule9: R1(?x, ?y) <- R2(?x, a), R3(b, ?y)", new PlainRule("rule9", Rxy, Rxa, Rby)},
                {" rule 9: R1(?x, ?y) <- R2(?x, a), R3(b, ?y)", new PlainRule("rule 9", Rxy, Rxa, Rby)},

                /* variations of endings and line breaks */
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y)\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y) \n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y)\r\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y) \r\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y).\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y). \n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y).\r\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y). \r\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y) .\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y) . \n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y) .\r\n", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x, ?y) <- R2(?x, a), R3(b, ?y) . \r\n", new PlainRule(Rxy, Rxa, Rby)},

                /* variants without any space */
                {"R(?x)<-", new PlainRule(Rx)},
                {"R(?x)<-.", new PlainRule(Rx)},
                {"R2(?x,a)<-.", new PlainRule(Rxa)},
                {"R3(b,?y)<-", new PlainRule(Rby)},
                {"R1(?x,?y)<-R2(?x,a),R3(b,?y)", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x,?y)<-R2(?x,a),R3(b,?y).", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x,?y)<-R4(?x,?y,a),R2(?x,a),R3(b,?y)", new PlainRule(Rxy, Rxya, Rxa, Rby)},
                {"[rule9] R1(?x,?y)<-R2(?x,a),R3(b,?y)", new PlainRule("rule9", Rxy, Rxa, Rby)},

                /* variants with doubled spaces*/
                {"R(?x)  <-  ", new PlainRule(Rx)},
                {"R(?x)  <-  .", new PlainRule(Rx)},
                {"R2(?x,  a)  <-  .",  new PlainRule(Rxa)},
                {"R3(b,  ?y)  <-  ",  new PlainRule(Rby)},
                {"R1(?x,  ?y)  <-  R2(?x,  a),  R3(b,  ?y)", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x,  ?y)  <-  R2(?x,  a),  R3(b,  ?y).", new PlainRule(Rxy, Rxa, Rby)},
                {"R1(?x,  ?y)  <-  R4(?x,  ?y,  a),  R2(?x,  a),  R3(b,  ?y)",
                        new PlainRule(Rxy, Rxya, Rxa, Rby)},
                {"[ rule  9 ]   R1(?x,  ?y)  <-  R2(?x,  a),  R3(b,  ?y)  .  ",
                        new PlainRule(" rule  9 ", Rxy, Rxa, Rby)},
        };
    }

    @Test(dataProvider = "ruleData")
    public void testParseRule(@Nonnull String string,
                              @Nonnull Rule expected) throws DatalogParser.SyntaxException {
        Rule actual = parser.parseRule(string);
        assertEquals(actual, expected);
    }

    @DataProvider
    public Object[][] badRuleData() {
        return new Object[][] {
                {"[name]"},
                {"name:"},
                {"a"},
                {"R()"},
                {"R(x, y)"},
                {"R(x, y, )"},
                {"a <- R(x)"},
                {"R() <- R(x)"},
                {"R(?, y) <- R(x)"},
                {"R(?x?, y) <- R(x)"},
                {"R(??x, y) <- R(x)"},
                {"R(?/x, y) <- R(x)"},
                {"R(? x, y) <- R(x)"},
                {"R(x, y), <- R(x)"},
                {"R(x, y), Q <- R(x)"},
                {"R(x, y), Q() <- R(x)"},
                {"R(x, y), Q(x) <- R(x)"},
                {"R(x, y) <- x"},
                {"R(x, y) <- R"},
                {"R(x, y) <- R[x, y]"},
                {"R(x, y) <- R(x, , y)"},
                {"R(x, y) <- R(,)"},
                {"R(x, y) <- R(x, y), "},
                {"R(x, y) <- R(x, y), Q()"},
                {"R(x, y) <- R(x, y), a"},
                {"R(x, y) <- R(x, y) Q(x, y)"},
                {"R(x, y) <- R(x, y), Q(x, y).."},
                {"R(x, y) <- R(x, y), # Q(x, y)"},
        };
    }

    @Test(dataProvider = "badRuleData")
    public void testParseBadRule(@Nonnull String string) {
        assertThrows(DatalogParser.SyntaxException.class, () -> parser.parseRule(string));
    }

    @Test
    public void testParsePrefixedProgram() throws DatalogParser.SyntaxException {
        DatalogParser parser = new DatalogParser();
        parser.setConstantParser(new JenaConstantParser());
        Program program = parser.parseProgram("@prefix owl: <" + OWL2.NS + ">\n" +
                "[eq-ref-1] T(?s, owl:sameAs, ?s) <- T(?s, ?p, ?o) .\n" +
                "[prp-fp]   T(?y1, owl:sameAs, ?y2) <- T(?p, rdf:type, owl:FunctionalProperty), T(?x, ?p, ?y1), T(?x, ?p, ?y2) .\n" +
                "[cls-maxc2]  T(?y1, owl:sameAs, ?y2) <- T(?x, owl:maxCardinality, \"1\"^^xsd:nonNegativeInteger), T(?x, owl:onProperty, ?p), T(?u, rdf:type, ?x), T(?u, ?p, ?y1), T(?u, ?p, ?y2) .");
        assertEquals(program.getRule(0).getHead().getTerms().get(1).getValue(), OWL2.sameAs);
    }

    @Test
    public void testParseOWLRL() throws IOException, DatalogParser.SyntaxException {
        DatalogParser parser = new DatalogParser();
        parser.setConstantParser(new JenaConstantParser());
        String path = "br/ufsc/lapesd/qsq/rdf/owl-rl.rules";
        try (InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(path)) {
            assertNotNull(in);
            Program program = parser.parseProgram(in);
            assertTrue(program.getRuleCount() > 0, "count="+program.getRuleCount());
            List<Rule> list = program.streamRules().filter(r -> !r.getPredicate().equals("T"))
                    .collect(Collectors.toList());
            assertEquals(list, Collections.emptyList());
        }
    }
}