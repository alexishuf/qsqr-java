package br.ufsc.lapesd.qsq.rules.parser;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.testng.Assert.*;

public class PlainConstantParserTest {
    @DataProvider
    public static Object[][] data() {
        return new Object[][] {
                {"?x", null, 0},
                {"?x,", null, 0},
                {"?x)", null, 0},
                {"?x ", null, 0},

                {"x", "x", 1},
                {"x,", "x", 1},
                {"x)", "x", 1},
                {"x ", "x", 1},

                {"x9", "x9", 2},
                {"x9,", "x9", 2},
                {"x9)", "x9", 2},
                {"x9 ", "x9", 2},

                {"'x'", "x", 3},
                {"'x',", "x", 3},
                {"'x')", "x", 3},

                {"\"x\"", "x", 3},
                {"\"x\",", "x", 3},
                {"\"x\")", "x", 3},

                {"'\"'", "\"", 3},
                {"\"'\"", "'", 3},
        };
    }

    @Test(dataProvider = "data")
    public void test(@Nonnull String text, @Nullable String expected, int endPos) {
        ConstantParser.Parse parse = new PlainConstantParser().parse(text);
        if (expected == null) {
            assertNull(parse);
        } else {
            assertNotNull(parse);
            assertEquals(parse.getValue(), expected);
            assertEquals(parse.getEndPos(), endPos);
        }
    }
}