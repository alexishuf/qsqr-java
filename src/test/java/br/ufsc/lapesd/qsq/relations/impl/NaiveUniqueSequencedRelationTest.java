package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.MutableRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.SequencedRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.List;

public class NaiveUniqueSequencedRelationTest {
    @Test
    public static class AddRowTest extends AbstractAddRowTest {
        @Override
        protected @Nonnull MutableRelation createRelation(@Nonnull List<String> columns,
                                                          @Nonnull List<List<Integer>> data) {
            NaiveUniqueSequencedRelation relation = new NaiveUniqueSequencedRelation(columns);
            data.forEach(row -> relation.addRow(new IndexedListRow(columns, row)));
            return relation;
        }
    }

    @Test
    public static class SelectionTest extends AbstractSelectionTest {
        @Override
        protected @Nonnull Relation createRelation(@Nonnull List<List<String>> dataRows) {
            NaiveUniqueSequencedRelation relation = new NaiveUniqueSequencedRelation(columns);
            dataRows.forEach(row -> relation.addRow(new IndexedListRow(columns, row)));
            return relation;
        }
    }

    @Test
    public static class ProjectionTest extends AbstractProjectionTest {
        @Override
        protected @Nonnull Relation createRelation(@Nonnull List<String> columns) {
            return new NaiveUniqueSequencedRelation(columns);
        }
    }

    @Test
    public static class MarkerTest extends AbstractMarkerTest {
        @Override
        protected @Nonnull SequencedRelation createRelation(@Nonnull List<String> columns) {
            return new NaiveUniqueSequencedRelation(columns);
        }
    }
}