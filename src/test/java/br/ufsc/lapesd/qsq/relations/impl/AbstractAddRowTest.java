package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.MaterializedRelation;
import br.ufsc.lapesd.qsq.relations.MutableRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.nCopies;
import static java.util.Collections.singletonList;
import static org.testng.Assert.*;

public abstract class AbstractAddRowTest {

    protected  abstract  @Nonnull MutableRelation createRelation(@Nonnull List<String> columns,
                                                                 @Nonnull List<List<Integer>> data);

    @DataProvider
    public Object[][] data() {
        return new Object[][] {
                {
                        asList("a", "b"),
                        singletonList(asList(1, 2))
                },
                {
                        singletonList("a"),
                        asList(singletonList(1), singletonList(2))
                },
                {
                        asList("a", "b", "c"),
                        Collections.emptyList()
                },
        };
    }

    protected boolean findRow(@Nonnull Relation relation, @Nonnull Row expected) {
        boolean match = false;
        for (Row row : relation)
            match |= RowOps.equals(row, expected);
        return match;
    }

    @Test(dataProvider = "data")
    public void testAddRow(List<String> columns, List<List<Integer>> data) {
        MutableRelation relation = createRelation(columns, data);
        Row expected = new IndexedListRow(columns, nCopies(columns.size(), 99));
        relation.addRow(new IndexedListRow(expected));

        if (relation.isMaterialized()) {
            assertEquals(((MaterializedRelation)relation).getSize(), data.size() + 1);
        }
        assertEquals(relation.getSizeEstimate(), data.size() + 1);
        assertTrue(findRow(relation, expected), "Failed to find the inserted row");
    }

    @Test(dataProvider = "data")
    public void testAddRowWithMissingLastColumn(List<String> columns, List<List<Integer>> data) {
        if (columns.size() == 1) return;
        MutableRelation relation = createRelation(columns, data);
        int nSize = columns.size() - 1;
        Row row = new IndexedListRow(columns.subList(0, nSize), nCopies(nSize, 99));
        assertThrows(IllegalArgumentException.class, () -> relation.addRow(row));
    }

    @Test(dataProvider = "data")
    public void testAddRowWithMissingFirstColumn(List<String> columns, List<List<Integer>> data) {
        if (columns.size() == 1) return;
        MutableRelation relation = createRelation(columns, data);
        IndexedListRow row = new IndexedListRow(columns.subList(1, columns.size()),
                nCopies(columns.size() - 1, 99));
        assertThrows(IllegalArgumentException.class, () -> relation.addRow(row));
    }

    @Test(dataProvider = "data")
    public void testAddRowWithExtraColumn(List<String> columns, List<List<Integer>> data) {
        MutableRelation relation = createRelation(columns, data);

        List<String> extColumns = new ArrayList<>(columns);
        List<Integer> extValues = new ArrayList<>(nCopies(columns.size(), 99));
        extColumns.add("zzz");
        extValues.add(98);
        IndexedListRow row = new IndexedListRow(extColumns, extValues);
        relation.addRow(row);

        IndexedListRow expected = new IndexedListRow(columns, nCopies(columns.size(), 99));
        assertTrue(findRow(relation, expected));
    }

    @Test(dataProvider = "data")
    public void testAddRowWithDifferentOder(List<String> columns, List<List<Integer>> data) {
        MutableRelation relation = createRelation(columns, data);

        List<String> revColumns = new ArrayList<>(columns);
        Collections.reverse(revColumns);
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < columns.size(); i++) values.add(90 + i);
        IndexedListRow row = new IndexedListRow(revColumns, values);
        Collections.reverse(values);
        IndexedListRow expected = new IndexedListRow(columns, values);

        assertTrue(relation.addRow(row));

        assertEquals(relation.getSizeEstimate(), data.size()+1);
        if (relation.isMaterialized())
            assertEquals(((MaterializedRelation) relation).getSize(), data.size() + 1);
        assertTrue(findRow(relation, expected));
    }
}
