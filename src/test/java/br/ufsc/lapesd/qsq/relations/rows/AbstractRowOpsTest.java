package br.ufsc.lapesd.qsq.relations.rows;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public abstract class AbstractRowOpsTest {

    @DataProvider
    public Object[][] setRelationData() {
        return new Object[][] {
                {
                        new String[]{"a", "b", "c"},
                        new int[]{1, 2, 3},
                        new String[]{"a", "b", "c"},
                        new int[]{1, 2, 3},
                        "equals"
                },
                {
                        new String[]{"a", "b", "c"},
                        new int[]{1, 2, 3},
                        new String[]{"a", "b", "c", "z"},
                        new int[]{1, 2, 3, 4},
                        "subset"
                },
                {
                        new String[]{"a"},
                        new int[]{1},
                        new String[]{"a", "b", "c", "z"},
                        new int[]{1, 2, 3, 4},
                        "subset"
                },
                {
                        new String[]{"a", "b", "c", "z"},
                        new int[]{1, 2, 3, 4},
                        new String[]{"a", "b", "c"},
                        new int[]{1, 2, 3},
                        ""
                },
                {
                        new String[]{"a", "b", "c"},
                        new int[]{2, 2, 3},
                        new String[]{"a", "b", "c"},
                        new int[]{1, 2, 3},
                        ""
                },
                {
                        new String[]{"a", "b", "c"},
                        new int[]{1, 3, 3},
                        new String[]{"a", "b", "c", "z"},
                        new int[]{1, 2, 3, 4},
                        ""
                },
                {
                        new String[]{"a"},
                        new int[]{2},
                        new String[]{"a", "b", "c", "z"},
                        new int[]{1, 2, 3, 4},
                        ""
                },
        };
    }

    protected abstract Row createRow(@Nonnull String[] columns, @Nonnull int[] data);

    @Test(dataProvider = "setRelationData")
    public void testEquals(@Nonnull String[] lColumns, @Nonnull int[] lData,
                           @Nonnull String[] rColumns, @Nonnull int[] rData,
                           @Nonnull String relation) {
        Row left = createRow(lColumns, lData);
        Row right = createRow(rColumns, rData);
        assertEquals(RowOps.equals(left, right), relation.equals("equals"));
    }

    @Test(dataProvider = "setRelationData")
    public void testIsSubset(@Nonnull String[] lColumns, @Nonnull int[] lData,
                             @Nonnull String[] rColumns, @Nonnull int[] rData,
                             @Nonnull String relation) {
        Row left = createRow(lColumns, lData);
        Row right = createRow(rColumns, rData);
        boolean expected = relation.equals("subset") || relation.equals("equals");
        assertEquals(RowOps.isSubset(left, right), expected);
    }

    @Test(dataProvider = "setRelationData")
    public void testIsSubsetIndexed(@Nonnull String[] lColumns, @Nonnull int[] lData,
                                    @Nonnull String[] rColumns, @Nonnull int[] rData,
                                    @Nonnull String relation) {
        Row left = createRow(lColumns, lData);
        boolean expected = relation.equals("subset") || relation.equals("equals");
        Map<String, Integer> col2pos = new HashMap<>();
        for (int i = 0; i < rColumns.length; i++) col2pos.put(rColumns[i], i);
        List<Integer> list = Arrays.stream(rData).boxed().collect(Collectors.toList());
        assertEquals(RowOps.isSubset(left, col2pos, list), expected);
    }
}