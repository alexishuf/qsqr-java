package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Marker;
import br.ufsc.lapesd.qsq.relations.SequencedRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static java.lang.String.format;
import static org.testng.Assert.*;

public abstract class AbstractMarkerTest {
    private @Nonnull List<String> columns = Collections.singletonList("a");

    protected abstract @Nonnull SequencedRelation createRelation(@Nonnull List<String> columns);

    private @Nonnull Row createRow(int value) {
        return new IndexedListRow(columns, Collections.singletonList(value));
    }

    @Test
    public void testEmpty() {
        SequencedRelation relation = createRelation(columns);
        Marker marker = relation.createMarker();
        assertTrue(marker.isEmpty());
        assertFalse(marker.iterator().hasNext());
    }

    @Test
    public void testNonEmpty() {
        SequencedRelation relation = createRelation(columns);
        relation.addRow(createRow(1));
        Marker marker = relation.createMarker();
        assertTrue(marker.isEmpty());
        assertFalse(marker.iterator().hasNext());
    }

    private void assertRowContent(@Nullable Row row, int value) {
        assertNotNull(row);
        Row expected = createRow(value);
        String message = format("Rows differ: actual=%s, expected=%s", row, expected);
        assertTrue(RowOps.equals(row, expected), message);
    }

    @Test
    public void testAddAfterMarker() {
        SequencedRelation relation = createRelation(columns);
        relation.addRow(createRow(1));
        Marker marker = relation.createMarker();
        relation.addRow(createRow(2));

        assertFalse(marker.isEmpty());
        assertTrue(marker.iterator().hasNext());

        Iterator<Row> it = marker.iterator();
        assertRowContent(it.next(), 2);
        assertFalse(it.hasNext());
    }

    @Test
    public void testCreateSecondMarkerNoAdd() {
        SequencedRelation relation = createRelation(columns);
        relation.addRow(createRow(1));
        Marker marker = relation.createMarker();
        relation.addRow(createRow(2));
        Marker marker2 = relation.createMarker();

        assertFalse(marker.isEmpty());
        assertTrue(marker.iterator().hasNext());

        assertTrue(marker2.isEmpty());
        assertFalse(marker2.iterator().hasNext());

        Iterator<Row> it = marker.iterator();
        assertRowContent(it.next(), 2);
        assertFalse(it.hasNext());

        Marker marker3 = relation.createMarker();
        assertTrue(marker3.isEmpty());
        assertFalse(marker3.iterator().hasNext());
    }

    @Test
    public void testCreateSecondMarkerAndAdd() {
        SequencedRelation relation = createRelation(columns);
        relation.addRow(createRow(1));
        Marker marker1 = relation.createMarker();
        relation.addRow(createRow(2));
        Marker marker2 = relation.createMarker();
        relation.addRow(createRow(3));
        Marker marker3 = relation.createMarker();

        Iterator<Row> it = marker1.iterator();
        assertRowContent(it.next(), 2);
        assertTrue(it.hasNext());
        assertRowContent(it.next(), 3);
        assertFalse(it.hasNext());

        it = marker2.iterator();
        assertTrue(it.hasNext());
        assertRowContent(it.next(), 3);
        assertFalse(it.hasNext());

        assertFalse(marker3.iterator().hasNext());
    }
}
