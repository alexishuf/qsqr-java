package br.ufsc.lapesd.qsq.relations.rows;

import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Test
public class IndexedListRowTest {

    @Test
    public class RowOpsTest extends AbstractRowOpsTest {
        @Override
        protected Row createRow(@Nonnull String[] columns, @Nonnull int[] data) {
            assert columns.length == data.length;
            List<String> columnsList = Arrays.stream(columns).collect(Collectors.toList());
            List<Integer> dataList = Arrays.stream(data).boxed().collect(Collectors.toList());
            return new IndexedListRow(columnsList, dataList);
        }
    }

}