package br.ufsc.lapesd.qsq.relations;

import br.ufsc.lapesd.qsq.relations.impl.MutableArrayListRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static java.util.Arrays.asList;
import static org.testng.Assert.assertEquals;

public class RelationUtilsTest {

    @Test
    public void testToCSVTwoColumns() throws UnsupportedEncodingException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MutableArrayListRelation relation = new MutableArrayListRelation("x", "y");
        IndexedListRow.Factory factory = new IndexedListRow.Factory(relation.getColumns());
        relation.addRow(factory.create(asList(1, 2)));
        relation.addRow(factory.create(asList(3, 4)));
        relation.addRow(factory.create(asList("5,1", "6\"")));
        RelationUtils.toCSV(relation, baos);
        String actual = baos.toString(StandardCharsets.UTF_8.name());
        String expected = "x, y\r\n" +
                "1, 2\r\n" +
                "3, 4\r\n" +
                "\"5,1\", 6\\\"\r\n";
        assertEquals(actual, expected);
    }

    @Test
    public void testToCSVSingleColumn() throws UnsupportedEncodingException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MutableArrayListRelation relation = new MutableArrayListRelation("x");
        IndexedListRow.Factory factory = new IndexedListRow.Factory(relation.getColumns());
        relation.addRow(factory.create(Collections.singletonList(1)));
        relation.addRow(factory.create(Collections.singletonList(3)));
        RelationUtils.toCSV(relation, baos);
        String actual = baos.toString(StandardCharsets.UTF_8.name());
        String expected = "x\r\n" +
                "1\r\n" +
                "3\r\n";
        assertEquals(actual, expected);
    }
}