package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import com.google.common.collect.Iterators;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public abstract class AbstractProjectionTest {

    protected abstract @Nonnull Relation createRelation(@Nonnull List<String> columns);
    protected @Nonnull Relation createProjection(@Nonnull Relation relation,
                                                 @Nonnull List<String> selected) {
        return relation.projection(selected);
    }
    protected @Nonnull Relation createProjection(@Nonnull Relation relation,
                                                 @Nonnull List<String> selected,
                                                 @Nonnull List<String> renamed) {
        return relation.projection(selected, renamed);
    }

    @DataProvider
    public Object[][] data() {
        return new Object[][] {
                {"a,b,c", "a,b,c"},
                {"a,b,c", "c,b,a"},
                {"a,b,c", "a,b"},
                {"a,b,c", "b,a"},
                {"a,b,c", "b"},
                {"a,b,c", "c"},
        };
    }

    @Test(dataProvider = "data")
    public void testProject(@Nonnull String columnsString, @Nonnull String selectedString) {
        List<String> columns = stream(columnsString.split("\\s*,\\s*")).collect(toList());
        List<String> selected = stream(selectedString.split("\\s*,\\s*")).collect(toList());

        Relation relation = createRelation(columns);
        Relation projection = createProjection(relation, selected);
        assertEquals(projection.getColumns(), selected);

        assertEquals(Iterators.size(projection.iterator()), Iterators.size(relation.iterator()));
        Iterator<Row> it = relation.iterator();
        Iterator<Row> pit = projection.iterator();
        while (it.hasNext()) {
            assertTrue(pit.hasNext());
            Row sub = pit.next();
            Row full = it.next();
            assertTrue(RowOps.isSubset(sub, full), "sub="+sub+", full="+full);
            assertEquals(sub.getColumns(), selected);
        }
    }

    @Test(dataProvider = "data")
    public void testProjectRenaming(@Nonnull String columnsString, @Nonnull String selectedString) {
        List<String> columns = stream(columnsString.split("\\s*,\\s*")).collect(toList());
        List<String> selected = stream(selectedString.split("\\s*,\\s*")).collect(toList());
        List<String> renamed = selected.stream().map(c -> c + "_").collect(toList());

        Relation relation = createRelation(columns);
        Relation projection = createProjection(relation, selected, renamed);
        assertEquals(projection.getColumns(), renamed);

        assertEquals(Iterators.size(projection.iterator()), Iterators.size(relation.iterator()));
        Iterator<Row> it = relation.iterator();
        Iterator<Row> pit = projection.iterator();
        while (it.hasNext()) {
            assertTrue(pit.hasNext());
            Row sub = pit.next();
            Row full = it.next();
            assertEquals(sub.getColumns(), renamed);
            List<Object> expected = new ArrayList<>(), actual = new ArrayList<>();
            for (int i = 0; i < renamed.size(); i++) {
                actual.add(sub.get(renamed.get(i)));
                expected.add(full.get(selected.get(i)));
            }
            assertEquals(actual, expected);
        }
    }
}
