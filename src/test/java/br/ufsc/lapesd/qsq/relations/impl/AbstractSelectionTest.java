package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static org.testng.Assert.assertTrue;

public abstract class AbstractSelectionTest {
    protected final List<String> columns = Arrays.asList("c1", "c2", "c3");

    protected abstract @Nonnull Relation createRelation(@Nonnull List<List<String>> dataRows);

    protected Relation createSelection(@Nonnull Relation relation, @Nonnull Row query) {
        return relation.selection(query);
    }

    @DataProvider
    public Object[][] data() {
        return new Object[][] {
                { "1,2,3",               "c1=1", "1,2,3" },
                { "1,2,3; 4,5,6",        "c1=1", "1,2,3" },
                { "1,2,3; 4,5,6",        "c1=4", "4,5,6" },
                { "1,2,3; 4,5,6; 1,7,8", "c1=1", "1,2,3; 1,7,8" },
                { "",                    "c1=1", "" },
                { "2,3,4; 5,6,7; 5,1,3", "c1=1", "" },
        };
    }

    @Test(dataProvider = "data")
    public void testSelect(@Nonnull String dataString, @Nonnull String queryString,
                           @Nonnull String expectedString) {
        List<List<String>> data = stream(dataString.split("\\s*;\\s*"))
                .map(s -> stream(s.split("\\s*,\\s*")).collect(Collectors.toList()))
                .filter(l -> l.size() == columns.size())
                .collect(Collectors.toList());

        Relation relation = createRelation(data);
        List<IndexedListRow> expected = stream(expectedString.split("\\s*;\\s*"))
                .map(r -> Arrays.asList(r.split("\\s*,\\s*")))
                .filter(l -> l.size() == columns.size())
                .map(l -> new IndexedListRow(columns, l))
                .collect(Collectors.toList());

        List<String> queryColumns = new ArrayList<>();
        List<String> queryValues = new ArrayList<>();
        for (String assignment : queryString.split("\\s*,\\s*")) {
            String[] pieces = assignment.split("\\s*=\\s*");
            assert pieces.length == 2;
            assert columns.contains(pieces[0]);
            assert pieces[1].length() > 0;
            queryColumns.add(pieces[0]);
            queryValues.add(pieces[1]);
        }
        IndexedListRow query = new IndexedListRow(queryColumns, queryValues);

        Relation selection = createSelection(relation, query);
        verifySelection(expected, selection);
    }

    protected void verifySelection(List<IndexedListRow> expected, Relation selection) {
        int i = 0;
        for (Row actual : selection) {
            String message = "Mismatch at row " + i;
            assertTrue(i < expected.size(), i+"-th result, expected " + expected.size());
            assertTrue(RowOps.equals(actual, expected.get(i)), message);
            ++i;
        }
    }


}