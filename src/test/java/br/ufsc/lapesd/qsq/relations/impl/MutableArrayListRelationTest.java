package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.MaterializedRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.testng.Assert.*;

public class MutableArrayListRelationTest {
    @DataProvider
    public Object[][] data() {
        return new Object[][] {
                {
                    asList("a", "b"), singletonList(asList(1, 2))
                },
                {
                    singletonList("a"), asList(singletonList(1), singletonList(2))
                },
                {
                    asList("a", "b", "c"), Collections.emptyList()
                },
        };
    }

    private static  @Nonnull MutableArrayListRelation
    createRelation(@Nonnull List<String> columns, @Nonnull List<?> data) {
        MutableArrayListRelation relation = new MutableArrayListRelation(columns);
        data.forEach(l -> relation.addRow((List)l));
        return relation;
    }

    private static @Nonnull Relation createUnitRelation(@Nonnull List<String> columns) {
        MutableArrayListRelation relation = new MutableArrayListRelation(columns);
        relation.addRow(columns);
        return relation;
    }

    @Test
    public static class AddRowTest extends AbstractAddRowTest {
        @Nonnull
        @Override
        protected MutableArrayListRelation createRelation(@Nonnull List<String> columns,
                                                          @Nonnull List<List<Integer>> data) {
            return MutableArrayListRelationTest.createRelation(columns, data);
        }

        @Test(dataProvider = "data")
        public void testAddSmallerRawRow(List<String> columns, List<List<Integer>> data) {
            MutableArrayListRelation relation = createRelation(columns, data);
            ArrayList<Object> list = new ArrayList<>();
            for (int i = 0; i < columns.size() - 1; i++) list.add(99);
            assertThrows(IllegalArgumentException.class, () -> relation.addRow(list));
        }

        @Test(dataProvider = "data")
        public void testAddLargerRawRow(List<String> columns, List<List<Integer>> data) {
            MutableArrayListRelation relation = createRelation(columns, data);
            ArrayList<Object> list = new ArrayList<>();
            for (int i = 0; i < columns.size() + 1; i++) list.add(99);
            assertThrows(IllegalArgumentException.class, () -> relation.addRow(list));
        }
    }

    @Test
    public static class SelectionTest extends AbstractSelectionTest {
        protected @Nonnull Relation createRelation(@Nonnull List<List<String>> dataRows) {
            return MutableArrayListRelationTest.createRelation(columns, dataRows);
        }
    }

    @Test
    public static class IteratorSelectionTest extends AbstractSelectionTest {
        protected @Nonnull Relation createRelation(@Nonnull List<List<String>> dataRows) {
            return MutableArrayListRelationTest.createRelation(columns, dataRows);
        }

        @Override
        protected Relation createSelection(@Nonnull Relation relation, @Nonnull Row query) {
            return new IteratorSelection(relation, query);
        }
    }

    @Test
    public static class ProjectionTest extends AbstractProjectionTest {
        protected @Nonnull Relation createRelation(@Nonnull List<String> columns) {
            return MutableArrayListRelationTest.createUnitRelation(columns);
        }

    }

    @Test
    public static class IteratorProjectionTest extends AbstractProjectionTest {
        protected @Nonnull Relation createRelation(@Nonnull List<String> columns) {
            return MutableArrayListRelationTest.createUnitRelation(columns);
        }

        @Override
        protected @Nonnull Relation createProjection(@Nonnull Relation relation,
                                                     @Nonnull List<String> selected) {
            return new IteratorProjection(relation, selected);
        }

        @Override
        protected @Nonnull Relation createProjection(@Nonnull Relation relation,
                                                     @Nonnull List<String> selected,
                                                     @Nonnull List<String> renamed) {
            return new IteratorProjection(relation, selected, renamed);
        }
    }

    @Test(dataProvider = "data")
    public void testSize(List<String> columns, List<List<Integer>> data) {
        MutableArrayListRelation relation = createRelation(columns, data);
        assertEquals(relation.getSize(), data.size());
        assertEquals(relation.getSizeEstimate(), data.size());
    }

    @Test(dataProvider = "data")
    public void testProjectionSize(List<String> columns, List<List<Integer>> data) {
        MutableArrayListRelation relation = createRelation(columns, data);
        for (String column : columns) {
            Relation projection = relation.projection(columns);
            assertTrue(projection instanceof MaterializedRelation);
            MaterializedRelation matProjection = (MaterializedRelation) projection;

            String message = "failed when column=" + column;
            assertEquals(matProjection.getSize(), data.size(), message);
            assertEquals(projection.getSizeEstimate(), data.size(), message);
        }
    }
}