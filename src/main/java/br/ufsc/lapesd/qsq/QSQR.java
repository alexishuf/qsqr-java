package br.ufsc.lapesd.qsq;

import br.ufsc.lapesd.qsq.detail.QSQRGlobals;
import br.ufsc.lapesd.qsq.detail.QSQTemplate;
import br.ufsc.lapesd.qsq.detail.QSQTemplateCache;
import br.ufsc.lapesd.qsq.detail.SupplementaryRelations;
import br.ufsc.lapesd.qsq.interactive.InteractionController;
import br.ufsc.lapesd.qsq.interactive.Value;
import br.ufsc.lapesd.qsq.interactive.impl.BatchInteractionController;
import br.ufsc.lapesd.qsq.joins.BindJoinExecutor;
import br.ufsc.lapesd.qsq.joins.LeftDeepJoin;
import br.ufsc.lapesd.qsq.rdf.ModelInstanceData;
import br.ufsc.lapesd.qsq.relations.*;
import br.ufsc.lapesd.qsq.relations.impl.EmptyRelation;
import br.ufsc.lapesd.qsq.relations.impl.NaiveUniqueSequencedRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.rules.*;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedRule;
import br.ufsc.lapesd.qsq.rules.adornment.Adornment;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import br.ufsc.lapesd.qsq.rules.plain.PlainRule;
import br.ufsc.lapesd.qsq.stats.LogEntry;
import br.ufsc.lapesd.qsq.stats.ModelInstanceDataTBoxContext;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.qsq.detail.QSQRGlobals.Type.ANSWER;
import static br.ufsc.lapesd.qsq.detail.QSQRGlobals.Type.INPUT;
import static br.ufsc.lapesd.qsq.interactive.Value.Type.MARKER;
import static br.ufsc.lapesd.qsq.interactive.Value.Type.STRING;
import static br.ufsc.lapesd.qsq.relations.ProjectionSpec.intersectWith;
import static br.ufsc.lapesd.qsq.relations.ProjectionSpec.intersectWithAliased;
import static br.ufsc.lapesd.qsq.rules.AtomUtils.rowFromBound;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.BOUND;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.FREE;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class QSQR {
    private final @Nonnull Program program;
    private final @Nonnull InstanceData data;
    private final @Nonnull QSQRGlobals globals;
    private final @Nonnull HashSet<AdornedAtom> unboundVisits;
    private final @Nonnull AdornedAtom query;
    private final @Nonnull UniqueSequencedRelation.Factory relationFactory;
    private @Nonnull List<LogEntry> logEntries;
    private boolean logEnabled;
    private @Nonnull InteractionController interactionCtl = new BatchInteractionController();
    private @Nullable String lastAction = null;
    private List<Term> conjunctiveQueryConstants;

    public QSQR(@Nonnull Program program, @Nonnull InstanceData data,
                @Nonnull Atom... atoms) {
        this(program, data, Arrays.asList(atoms), NaiveUniqueSequencedRelation::new);
    }

    public QSQR(@Nonnull Program program, @Nonnull InstanceData data,
                @Nonnull List<Atom> conjunctiveQuery) {
        this(program, data, conjunctiveQuery, NaiveUniqueSequencedRelation::new);
    }

    public QSQR(@Nonnull Program program, @Nonnull InstanceData data,
                @Nonnull List<Atom> conjunctiveQuery,
                @Nonnull UniqueSequencedRelation.Factory relationFactory) {
        Preconditions.checkArgument(!conjunctiveQuery.isEmpty(), "Empty conjunctive query");
        this.relationFactory = relationFactory;
        this.logEntries = new ArrayList<>();
        this.logEnabled = false;
        this.globals = new QSQRGlobals(relationFactory);
        this.unboundVisits = new HashSet<>();
        this.data = data;
        if (conjunctiveQuery.size() > 1) {
            AdornedRule rule = createQueryRule(program, conjunctiveQuery);
            String name = program.getName();
            if (name != null) {
                name = format("%s?(%s)", name, conjunctiveQuery.stream()
                        .map(Object::toString).collect(joining(", ")));
            }
            this.program = new CompositeProgram(name, program, new ListProgram(rule));
            this.query = rule.getHead();
        } else {
            this.program = program;
            query = AdornedAtom.adorn(conjunctiveQuery.iterator().next());
            conjunctiveQueryConstants = query.getBound();
            addQueryInputs();
        }
    }

    private void addQueryInputs() {
        if (!conjunctiveQueryConstants.isEmpty())
            globals.get(INPUT, query).addPositional(conjunctiveQueryConstants);
    }

    private AdornedRule createQueryRule(@Nonnull Program program,
                                        @Nonnull List<Atom> conjunctiveQuery) {
        Preconditions.checkArgument(!conjunctiveQuery.isEmpty());

        Set<String> predicates = program.streamRules().map(Rule::getHead).map(Atom::getPredicate)
                .collect(Collectors.toSet());
        String queryPredicate = "query";
        for (int i = 0; predicates.contains(queryPredicate); i++)
            queryPredicate = "query"+i;

        List<Term> vars = conjunctiveQuery.stream()
                .flatMap(a -> a.getTerms().stream().filter(Term::isVariable))
                .distinct().collect(toList());
        String[] dummyPrefix = {"dummy"};
        for (int i = 0; vars.stream().anyMatch(v -> v.getName().startsWith(dummyPrefix[0])); ++i)
            dummyPrefix[0] = "dummy"+i;
        dummyPrefix[0] += "_";

        BiMap<Term, Term> const2var = HashBiMap.create();
        int[] counter = {0};
        conjunctiveQuery.stream().flatMap(a -> a.getTerms().stream().filter(t -> !t.isVariable()))
                .distinct().forEachOrdered(c -> const2var.put(c,
                        Term.createVariable(dummyPrefix[0] + counter[0]++)));
        List<Term> allVars = new ArrayList<>(vars.size() + const2var.size());
        allVars.addAll(vars);
        allVars.addAll(const2var.values());

        List<Atom> plainBody = new ArrayList<>(conjunctiveQuery.size());
        for (Atom atom : conjunctiveQuery) {
            List<Term> terms = new ArrayList<>(atom.getTerms().size());
            for (Term term : atom.getTerms()) {
                if (term.isVariable()) terms.add(term);
                else terms.add(const2var.get(term));
            }
            plainBody.add(new PlainAtom(atom.getPredicate(), terms));
        }
        PlainRule plainRule = new PlainRule(new PlainAtom(queryPredicate, allVars), plainBody);
        StringBuilder adornmentStringBuilder = new StringBuilder();
        for (int i = 0; i < vars.size(); i++) adornmentStringBuilder.append(FREE.character());
        for (int i = 0; i < const2var.size(); i++) adornmentStringBuilder.append(BOUND.character());
        Adornment adornment = new Adornment(adornmentStringBuilder.toString());
        SupplementaryRelations relations = new SupplementaryRelations(plainRule, adornment);

        AdornedRule adornedQuery = relations.createAdornedRule();
        AdornedAtom head = adornedQuery.getHead();

        conjunctiveQueryConstants = new ArrayList<>(const2var.size());
        for (Term bound : head.getBound()) {
            assert const2var.inverse().containsKey(bound);
            conjunctiveQueryConstants.add(const2var.inverse().get(bound));
        }
        assert conjunctiveQueryConstants.isEmpty() == head.isUnbound();
        addQueryInputs();

        return adornedQuery;
    }

    public void setInteractionCtl(@Nonnull InteractionController interactionCtl) {
        this.interactionCtl = interactionCtl;
    }

    public boolean isLogEnabled() {
        return logEnabled;
    }

    public void setLogEnabled(boolean logEnabled) {
        this.logEnabled = logEnabled;
    }

    public @Nonnull List<LogEntry> getLogEntries() {
        return logEntries;
    }

    /**
     * Runs the query for which this QSQR instance was built.
     */
    public @Nonnull Relation run() {
        Relation relation;
        boolean extensional = program.isExtensional(query.getPredicate());
        if (extensional) {
            relation = data.get(query.getPredicate());
            if (relation == null)
                return EmptyRelation.fromTerms(query.getTerms());
        } else {
            run(0, query);
            relation = globals.get(ANSWER, query).getRelation();
        }
        Relation selection = relation;
        if (query.hasLiterals())
            selection = relation.selection(rowFromBound(relation.getColumns(), query));
        return AtomUtils.projectPerAdornment(selection, query);
    }


    protected boolean run(int depth, @Nonnull AdornedAtom atom) {
        assert program.isIntensional(atom.getPredicate());
        QSQRGlobals.Global answer = globals.get(ANSWER, atom);
        QSQTemplateCache templateCache = new QSQTemplateCache(program, relationFactory);

        // query extensional facts
        Relation relation = data.get(atom.getPredicate());
        if (relation != null) {
            UniqueSequencedRelation out = answer.getRelation();
            IndexedListRow.Factory fac = new IndexedListRow.Factory(out.getColumns());
            Relation selection = relation;
            if (atom.hasLiterals())
                selection = relation.selection(rowFromBound(relation.getColumns(), atom));
            for (Row row : selection)
                out.addRow(fac.createPositional(row));
        }

        boolean[] newResults = {false};
        if (atom.isUnbound()) {
            if (!unboundVisits.add(atom))
                return false; // no more work to do, since there is no in_atom for recursive feeding
            // special case for inference on unbound queries
            String name = "in^" + atom.toString();
            List<String> dummy = Collections.singletonList("0");
            Marker marker = new EmptyRelation(name, dummy).createMarker();
            templateCache.stream(atom).forEach(tpl ->
                    newResults[0] |= processTemplate(marker, tpl, depth, templateCache, answer));
        } else {
            // query derived tuples, repeating if new inputs are given
            QSQRGlobals.Global input = globals.get(INPUT, atom);
            for (boolean[] changed = {true}; changed[0]; ) {
                changed[0] = false;
                Marker marker = input.getMarker();
                if (depth == 0 && atom.equals(query)) {
                    // See reason in https://doi.org/10.1145/2362355.2362360, pg 9
                    globals.clearInputs();
                    addQueryInputs();
                }
                if (!marker.isEmpty()) {
                    input.updateMarker();
                    templateCache.stream(atom).forEach(tpl -> {
                        if (processTemplate(marker, tpl, depth, templateCache, answer))
                            newResults[0] = changed[0] = true;
                    });
                }
            }
        }
        return newResults[0];
    }

    protected boolean processTemplate(@Nonnull Marker marker, @Nonnull QSQTemplate template,
                                      int depth, @Nonnull QSQTemplateCache templateCache,
                                      @Nonnull QSQRGlobals.Global answer) {
        boolean changed = false;
        Marker suppMarker = template.updateMarker(0);
        template.addInputToSupp0(marker);
        String ruleString = template.getRule().toString();
        lastAction = format("Moved new tuples from input to supp_0 of %s", ruleString);
        interactionCtl.showState(() -> dumpState(depth, templateCache));
        int i = 0;
        for (Atom atom_ : template.getRule().getBody()) {
            AdornedAtom atom = (AdornedAtom)atom_;
            List<String> rightColumns = AtomUtils.getVarNamesOrNull(atom);
            String predicate = atom.getPredicate(); // lets call this R in the comments below
            if (program.isExtensional(predicate)) { // action B, case 1
                // supp_i ⨝ A to feed supp_{i+1}
                Relation right = data.get(predicate);
                if (right != null)
                    suppMarker = joinInto(template, i, right, rightColumns);
            } else { // action B, case 2
                    if (!atom.isUnbound()) {
                        globals.get(INPUT, atom).addPositional(atom, suppMarker);
                        lastAction = format("Moved tuples from supp_%d of %s to in_%s",
                                i, ruleString, atom);
                        interactionCtl.showState(() -> dumpState(depth, templateCache));
                    }
                    // recurse (action A)
                    changed |= run(depth+1, atom);
                // fill next supplementary relation
                UniqueSequencedRelation right = globals.get(ANSWER, atom).getRelation();
                suppMarker = joinInto(template, i, right, rightColumns);
            }
            lastAction = format("Moved tuples from supp_%d to supp_%d for %s", i, i+1, ruleString);
            interactionCtl.showState(() -> dumpState(depth, templateCache));
            ++i;
        }

        if (template.getLastSupp().getColumns().size() != answer.getAdornment().size())
            changed |= fillAndAdd(suppMarker, template.getRule().getHead(), answer.getRelation());
        else
            changed |= RelationUtils.addPositional(suppMarker, answer.getRelation());
        lastAction = format("Moved tuples from last supp of %s to ans_%s",
                ruleString, template.getRule().getHead());
        interactionCtl.showState(() -> dumpState(depth, templateCache));
        return changed;
    }

    private boolean fillAndAdd(@Nonnull Marker marker, @Nonnull AdornedAtom head,
                               @Nonnull UniqueSequencedRelation out) {
        IndexedListRow.Factory fac = new IndexedListRow.Factory(out.getColumns());
        List<Object> values = new ArrayList<>(fac.getLength());
        boolean added = false;
        for (Row row : marker) {
            for (Term term : head.getTerms())
                values.add(term.isVariable() ? row.get(term.getName()) : term.getValue());
            IndexedListRow projected = fac.create(values);
            added |= out.addRow(projected);
            values.clear();
        }
        return added;
    }

    protected Marker joinInto(@Nonnull QSQTemplate template, int suppIdx,
                              @Nonnull Relation right,
                              @Nonnull List<String> rightCols) {

        Marker input = template.getMarker(suppIdx);
        SequencedRelation out = template.getSupp(suppIdx+1);
        Marker outMarker = template.updateMarker(suppIdx+1);

        List<String> leftCols = input.getRelation().getColumns();
        Atom atom = template.getRule().getBody().get(suppIdx);
        if (leftCols.isEmpty()) {
            directQuery(out, right, rightCols, template.getRule().getBody().get(suppIdx));
            return outMarker;
        }
        ProjectionSpec queryP = intersectWithAliased(leftCols, rightCols, right.getColumns());
        Row constRow = createConstRow(right, atom);
        List<String> outCols = out.getColumns();
        ProjectionSpec resultP = intersectWith(outCols, leftCols)
                .overridden(intersectWithAliased(outCols, rightCols, right.getColumns())
                        .reversed());

        LeftDeepJoin.Builder opBuilder = LeftDeepJoin.builder().add(right, queryP, resultP);
        if (constRow != null)
            opBuilder.addQueryConstants(constRow);
        LeftDeepJoin op = opBuilder.build(input);
        BindJoinExecutor executor = new BindJoinExecutor();
        if (logEnabled) {
            executor.setLogConsumer(logEntries::add);
            if (data instanceof ModelInstanceData)
                executor.setTBoxContext(new ModelInstanceDataTBoxContext((ModelInstanceData) data));
        }
        executor.execute(op).forEach(out::addRow);
        return outMarker;
    }

    private @Nullable Row createConstRow(@Nonnull Relation relation, @Nonnull Atom atom) {
        List<String> constColumns = new ArrayList<>();
        List<Object> constValues = new ArrayList<>();
        for (int i = 0; i < atom.getTerms().size(); i++) {
            Term term = atom.getTerms().get(i);
            if (!term.isVariable()) {
                constColumns.add(relation.getColumns().get(i));
                constValues.add(term.getValue());
            }
        }
        IndexedListRow constRow = null;
        if (!constColumns.isEmpty())
            constRow = new IndexedListRow(constColumns, constValues);
        return constRow;
    }

    private void directQuery(@Nonnull SequencedRelation out, @Nonnull Relation right,
                             @Nonnull List<String> rightCols,
                             @Nonnull Atom atom) {
        List<String> actualCols = right.getColumns();
        ProjectionSpec projection;
        projection = intersectWithAliased(out.getColumns(), rightCols, actualCols).reversed();
        IndexedListRow.ProjectingFactory fac = new IndexedListRow.ProjectingFactory(projection);
        Relation result = right;
        if (atom.hasLiterals())
            result = right.selection(rowFromBound(right.getColumns(), atom));
        for (Row row : result)
            out.addRow(fac.project(row));
    }

    public @Nonnull String dump(@Nonnull QSQTemplateCache templateCache) {
        StringBuilder builder = new StringBuilder();
        Consumer<QSQRGlobals.Global> adder = global -> {
            builder.append(global);
            builder.append(": ");
            builder.append(global.getMarker().stream().map(Object::toString)
                    .collect(Collectors.joining(", ")));
            builder.append('\n');
        };
        globals.streamAll(INPUT).forEach(adder);
        globals.streamAll(ANSWER).forEach(adder);
        List<AdornedAtom> keys = templateCache.streamKeys().collect(toList());
        builder.append(keys.stream().flatMap(templateCache::stream)
                .map(QSQTemplate::dump).collect(Collectors.joining("\n")));
        return builder.toString();
    }

    private @Nonnull List<Value> dumpState(int depth, @Nonnull QSQTemplateCache templateCache) {
        List<Value> list = new ArrayList<>();
        list.add(new Value("Call depth", STRING, Integer.toString(depth)));
        if (lastAction != null)
            list.add(Value.empty(lastAction));
        lastAction = null;
        list.add(Value.separator());
        Consumer<QSQRGlobals.Global> addGlobal = g -> {
            list.add(Value.empty(g.toString()));
            list.add(new Value("marker", 1, MARKER, g.getMarker()));
        };
        globals.streamAll(INPUT).forEach(addGlobal);
        globals.streamAll(ANSWER).forEach(addGlobal);
        List<AdornedAtom> keys = templateCache.streamKeys().collect(toList());
        keys.stream().flatMap(templateCache::stream).forEach(tpl -> {
            list.add(Value.empty(tpl.getRule().toString()));
            for (int i = 0; i < tpl.getSuppCount(); i++)
                list.add(new Value("supp_" + i, 1, MARKER, tpl.getMarker(i)));
        });
        return list;
    }

    public void dumpToErr(@Nonnull QSQTemplateCache templateCache) {
        System.err.print(dump(templateCache));
    }
}
