package br.ufsc.lapesd.qsq.joins;

import br.ufsc.lapesd.qsq.relations.ProjectionSpec;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.stats.MutableQueryEntry;
import br.ufsc.lapesd.qsq.stats.QueryEntry;
import br.ufsc.lapesd.qsq.stats.TBoxContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;

import static br.ufsc.lapesd.qsq.relations.RelationUtils.getColumns;
import static br.ufsc.lapesd.qsq.relations.RelationUtils.select;

public class BindJoinExecutor {
    private @Nullable Consumer<QueryEntry> logConsumer = null;
    private @Nullable TBoxContext tBoxContext = null;

    public @Nullable Consumer<QueryEntry> getLogConsumer() {
        return logConsumer;
    }

    public void setLogConsumer(@Nullable Consumer<QueryEntry> logConsumer) {
        this.logConsumer = logConsumer;
    }

    public void setTBoxContext(@Nullable TBoxContext tBoxContext) {
        this.tBoxContext = tBoxContext;
    }

    public @Nonnull Iterable<Row> execute(@Nonnull LeftDeepJoin operation) {
        Iterable<Row> left = operation.getInitial();
        for (int i = 0; i < operation.getRelationsCount(); i++) {
            left = new PairwiseJoin(left, operation, i);
        }
        return left;
    }

    private class PairwiseJoin implements Iterable<Row> {
        private final @Nonnull Iterable<Row> left;
        private final @Nonnull LeftDeepJoin operation;
        private final int rightIdx;
        private MutableQueryEntry entry = null;
        private boolean entryAdded = false;

        public PairwiseJoin(@Nonnull Iterable<Row> left,
                            @Nonnull LeftDeepJoin operation, int rightIdx) {
            this.left = left;
            this.operation = operation;
            this.rightIdx = rightIdx;

            String name = null;
            Iterable<Row> right = operation.getRelation(rightIdx);
            if (right instanceof Relation)
                name = ((Relation) right).getName();
            if (logConsumer != null && name != null)
                this.entry = new MutableQueryEntry(name);
        }

        @Override
        public @Nonnull Iterator<Row> iterator() {
            Iterable<Row> right = operation.getRelation(rightIdx);
            ProjectionSpec left2qry = operation.getQueryProjection(rightIdx);
            Row constants = operation.getQueryConstants(rightIdx);
            if (constants != null)
                left2qry = left2qry.append(ProjectionSpec.nop(constants));
            IndexedListRow.ProjectingFactory queryFac;
            queryFac = new IndexedListRow.ProjectingFactory(left2qry, constants);

            ProjectionSpec right2int = operation.getResultProjection(rightIdx);
            ProjectionSpec left2int;
            List<String> leftCols = getColumns(left);
            if (rightIdx == 0)
                left2int = ProjectionSpec.nop(leftCols);
            else
                left2int = operation.getResultProjection(rightIdx-1);
            IntProjector intProjector = new IntProjector(leftCols, left2int, right2int);

            Iterator<Row> leftIt = left.iterator();
            return new Iterator<>() {
                private Row leftRow = null, query = null;
                private Iterator<Row> rightIt = null;
                private boolean stale = true;

                private boolean advance() {
                    if (!stale) return true;

                    while (leftRow == null || (rightIt != null && !rightIt.hasNext())) {
                        if (!leftIt.hasNext()) return false;
                        leftRow = leftIt.next();
                        query = queryFac.project(leftRow);
                        rightIt = select(right, query).iterator();
                        if (entry != null) {
                            if (!entryAdded && logConsumer != null) {
                                logConsumer.accept(entry);
                                entryAdded = true;
                            }
                            entry.incrementSentSubQueries();
                            entry.addSelectionClause(query);
                        }
                    }
                    assert rightIt != null /*&& rightIt.hasNext()*/;
                    return true;
                }

                @Override
                public boolean hasNext() {
                    return advance();
                }
                @Override
                public Row next() {
                    if (!advance())
                        throw new NoSuchElementException();
                    Row row = intProjector.project(leftRow, rightIt.next());
                    if (entry != null) {
                        entry.incrementReceivedTuples();
                        if (tBoxContext != null) {
                            if (tBoxContext.isTBoxRow(row))
                                entry.incrementReceivedTBoxTuples();
                            else
                                entry.incrementReceivedABoxTuples();
                        }
                    }
                    stale = true;
                    return row;
                }
            };
        }
    }

    private static class IntProjector {
        private final @Nonnull Set<String> fromLeft;
        private final @Nonnull List<String> sourceColumns;
        private final @Nonnull IndexedListRow.Factory factory;

        public IntProjector(@Nonnull List<String> leftColumns,
                            @Nonnull ProjectionSpec left2int,
                            @Nonnull ProjectionSpec right2int) {
            factory = new IndexedListRow.Factory(right2int.getRenamed());

            int maxSize = right2int.getRenamed().size();
            sourceColumns = right2int.getSelected();
            fromLeft = new HashSet<>(maxSize);
            for (String selected : right2int.getSelected()) {
                if (leftColumns.contains(selected))
                    fromLeft.add(selected);
            }
        }

        public @Nonnull IndexedListRow project(@Nonnull Row left, @Nonnull Row right) {
            List<Object> list = new ArrayList<>(factory.getLength());
            for (String column : sourceColumns)
                list.add((fromLeft.contains(column) ? left : right).get(column));
            return factory.create(list);
        }
    }
}
