package br.ufsc.lapesd.qsq.joins;

import br.ufsc.lapesd.qsq.relations.ProjectionSpec;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class LeftDeepJoin {
    private @Nonnull List<ProjectionSpec> queryProjections;
    private @Nonnull List<Row> queryConstants;
    private @Nonnull List<ProjectionSpec> resultProjections;
    private @Nonnull List<Iterable<Row>> relations;
    private @Nonnull Iterable<Row> initial;

    protected LeftDeepJoin(@Nonnull List<ProjectionSpec> queryProjections,
                           @Nonnull List<Row> queryConstants,
                           @Nonnull List<ProjectionSpec> resultProjections,
                           @Nonnull List<Iterable<Row>> relations,
                           @Nonnull Iterable<Row> initial) {
        assert relations.size() == queryProjections.size();
        assert relations.size() == resultProjections.size();
        assert resultProjections.stream().map(ProjectionSpec::getRenamed).count()
                == resultProjections.stream().map(ProjectionSpec::getRenamed).distinct().count();
        this.initial = initial;
        this.queryProjections = queryProjections;
        this.queryConstants = queryConstants;
        this.resultProjections = resultProjections;
        this.relations = relations;
    }

    public static Builder builder() {
        return new Builder();
    }

    public @Nonnull ProjectionSpec getQueryProjection(int i) {
        return queryProjections.get(i);
    }

    public @Nonnull ProjectionSpec getResultProjection(int i) {
        return resultProjections.get(i);
    }

    public @Nullable Row getQueryConstants(int i) {
        return queryConstants.get(i);
    }

    public @Nonnull Iterable<Row> getRelation(int i) {
        return relations.get(i);
    }

    public int getRelationsCount() {
        return relations.size();
    }

    public @Nonnull Iterable<Row> getInitial() {
        return initial;
    }

    public static class Builder {
        private @Nonnull List<ProjectionSpec> queryProjections = new ArrayList<>();
        private @Nonnull List<Row> queryConstants = new ArrayList<>();
        private @Nonnull List<ProjectionSpec> resultProjections = new ArrayList<>();
        private @Nonnull List<Iterable<Row>> relations = new ArrayList<>();

        public @Nonnull Builder add(@Nonnull Iterable<Row> relation,
                                    @Nonnull ProjectionSpec queryProjection,
                                    @Nonnull ProjectionSpec intermediateProjection) {
            queryProjections.add(queryProjection);
            queryConstants.add(null);
            resultProjections.add(intermediateProjection);
            relations.add(relation);
            return this;
        }
        public @Nonnull Builder add(@Nonnull Relation relation,
                                    @Nonnull ProjectionSpec queryProjection,
                                    @Nonnull ProjectionSpec resultProjection) {
            String missing = queryProjection.getRenamed().stream()
                    .filter(c -> !relation.getColumns().contains(c)).collect(joining(", "));
            Preconditions.checkArgument(missing.isEmpty(),
                    "Relation is missing columns used by the query: " + missing);
            return add((Iterable<Row>) relation, queryProjection, resultProjection);
        }

        public @Nonnull Builder add(@Nonnull Iterable<Row> relation,
                                    @Nonnull List<String> selectForJoin,
                                    @Nonnull List<String> renameBeforeJoin,
                                    @Nonnull List<String> selectAfterJoin,
                                    @Nonnull List<String> renameAfterJoin) {
            return add(relation, new ProjectionSpec(selectForJoin, renameBeforeJoin),
                                 new ProjectionSpec(selectAfterJoin, renameAfterJoin));
        }
        public @Nonnull Builder add(@Nonnull Relation relation,
                                    @Nonnull List<String> selectForJoin,
                                    @Nonnull List<String> renameBeforeJoin,
                                    @Nonnull List<String> selectAfterJoin,
                                    @Nonnull List<String> renameAfterJoin) {
            return add(relation, new ProjectionSpec(selectForJoin, renameBeforeJoin),
                                 new ProjectionSpec(selectAfterJoin, renameAfterJoin));
        }
        public @Nonnull Builder addQueryConstants(@Nonnull Row constants) {
            int idx = queryConstants.size() - 1;
            assert queryConstants.size() == queryProjections.size();
            Preconditions.checkState(queryConstants.get(idx) == null, "Already set constants");
            String repeated = constants.getColumns().stream()
                    .filter(queryProjections.get(idx).getSelected()::contains)
                    .collect(joining(", "));
            Preconditions.checkArgument(repeated.isEmpty(),
                    "both projected and bound to constants: " + repeated);

            queryConstants.set(idx, constants);
            return this;
        }
        public @Nonnull Builder addQueryConstants(@Nonnull List<String> columns,
                                                  @Nonnull List<String> values) {
            return addQueryConstants(new IndexedListRow(columns, values));
        }

        public @Nonnull LeftDeepJoin build(@Nonnull Row initial) {
            return build(Collections.singletonList(initial));
        }

        public @Nonnull LeftDeepJoin build(@Nonnull Iterable<Row> initial) {
            return new LeftDeepJoin(queryProjections, queryConstants, resultProjections,
                                    relations, initial);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeftDeepJoin that = (LeftDeepJoin) o;
        return queryProjections.equals(that.queryProjections) &&
                queryConstants.equals(that.queryConstants) &&
                resultProjections.equals(that.resultProjections) &&
                relations.equals(that.relations) &&
                getInitial().equals(that.getInitial());
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryProjections, queryConstants, resultProjections, relations, getInitial());
    }

    @Override
    public String toString() {
        return "LeftDeepJoin{" +
                "queryProjections=" + queryProjections +
                ", queryConstants=" + queryConstants +
                ", resultProjections=" + resultProjections +
                ", relations=" + relations +
                ", initial=" + initial +
                '}';
    }
}
