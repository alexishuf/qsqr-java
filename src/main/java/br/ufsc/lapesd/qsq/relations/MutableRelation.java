package br.ufsc.lapesd.qsq.relations;

import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface MutableRelation extends Relation {
    @FunctionalInterface
    interface Factory {
        @Nonnull MutableRelation create(@Nonnull Collection<String> columns);
    }

    @Override
    default boolean isMutable() {
        return true;
    }

    /**
     * Adds a row to the relation.
     *
     * The columns reported by row must be a superset of those of this relation. Values will
     * be reordered to match the ordering in this {@link Relation}. Values are
     * copied by reference.
     *
     * @param row row to add
     * @return true iff the row was added (e.g., a {@link UniqueRelation} refuse to add duplicates)
     */
    boolean addRow(@Nonnull Row row);

    /**
     * Erases all tuples of the relation
     */
    void clear();
}
