package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.ProjectionSpec;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IteratorProjection implements Relation {
    private @Nullable Relation base;
    private @Nonnull Iterable<Row> iterable;
    private @Nonnull List<String> columns;
    private @Nonnull List<String> newColumns;

    public IteratorProjection(@Nonnull Relation base, @Nonnull List<String> columns,
                              @Nonnull List<String> newColumns) {
        this.iterable = this.base = base;
        this.columns = Collections.unmodifiableList(columns);
        this.newColumns = Collections.unmodifiableList(newColumns);
    }
    public IteratorProjection(@Nonnull Iterable<Row> iterable, @Nonnull List<String> columns,
                              @Nonnull List<String> newColumns) {
        this.base = null;
        this.iterable = iterable;
        this.columns = Collections.unmodifiableList(columns);
        this.newColumns = Collections.unmodifiableList(newColumns);
    }

    public IteratorProjection(@Nonnull Relation base, @Nonnull List<String> columns) {
        this.iterable = this.base = base;
        this.newColumns = this.columns = Collections.unmodifiableList(new ArrayList<>(columns));
    }

    public IteratorProjection(@Nonnull Relation base, @Nonnull ProjectionSpec projection) {
        this(base, projection.getSelected(), projection.getRenamed());
    }
    public IteratorProjection(@Nonnull Iterable<Row> iterable, @Nonnull ProjectionSpec projection){
        this(iterable, projection.getSelected(), projection.getRenamed());
    }

    @Nullable
    @Override
    public String getName() {
        if (base != null && base.getName() != null) {
            return String.format("%s[%s]", base.getName(),
                    IntStream.range(0, columns.size()).boxed()
                            .map(i -> newColumns.get(i) + "=" + columns.get(i))
                            .collect(Collectors.joining(", "))
            );
        }
        return null;
    }

    @Nonnull
    @Override
    public List<String> getColumns() {
        return newColumns;
    }

    @Nonnull
    @Override
    public Iterator<Row> iterator() {
        Iterator<Row> baseIt = iterable.iterator();
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return baseIt.hasNext();
            }

            @Override
            public Row next() {
                Row next = baseIt.next();
                List<Object> values = new ArrayList<>(newColumns.size());
                for (String column : columns)
                    values.add(next.get(column));
                return new IndexedListRow(newColumns, values);
            }
        };
    }

    @Override
    public boolean isMaterialized() {
        return base != null && base.isMaterialized();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public boolean isUnique() {
        return false;
    }

    @Override
    public long getSizeEstimate() {
        if (base != null)
            return base.getSizeEstimate();
        else if (iterable instanceof Collection)
            return ((Collection<Row>) iterable).size();
        else
            return iterable.iterator().hasNext() ? 1 : 0;
    }
}
