package br.ufsc.lapesd.qsq.relations;

import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface MaterializedRelation extends Relation {
    @Override
    default boolean isMaterialized() {
        return true;
    }

    /**
     * Returns the precise current number of rows
     */
    long getSize();

    @Override
    default @Nonnull Stream<Row> stream() {
        int chars = Spliterator.NONNULL | Spliterator.SIZED;
        if (isUnique())
            chars |= Spliterator.DISTINCT;
        Spliterator<Row> spl = Spliterators.spliterator(iterator(), getSize(), chars);
        return StreamSupport.stream(spl, false);
    }
}
