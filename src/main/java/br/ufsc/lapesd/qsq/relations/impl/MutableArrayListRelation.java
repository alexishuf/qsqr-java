package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.MutableRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MutableArrayListRelation extends ArrayListRelation implements MutableRelation {

    public static class Factory implements ArrayListRelation.Factory {
        @Override
        public @Nonnull Relation create(@Nullable String name, @Nonnull Collection<String> columns) {
            return new MutableArrayListRelation(name, columns);
        }
    }

    public MutableArrayListRelation(@Nullable String name, @Nonnull List<String> columns) {
        super(name, columns);
    }
    public MutableArrayListRelation(@Nullable String name, @Nonnull Collection<String> columns) {
        super(name, new ArrayList<>(columns));
    }
    public MutableArrayListRelation(@Nonnull List<String> columns) {
        super(columns);
    }
    public MutableArrayListRelation(@Nonnull Collection<String> columns) {
        super(new ArrayList<>(columns));
    }
    public MutableArrayListRelation(@Nonnull String... columns) {
        this(Arrays.asList(columns));
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public boolean addRow(@Nonnull Row row) {
        data.add(toArrayList(row));
        return true;
    }

    /**
     * Adds the elements in rowData as a new row.
     *
     * The elements must be in the same order as the columns in getColumns(). No missing or
     * extra elements are allowed: an IllegalArgumentException will be thrown.
     *
     * @param rowData row values, in the same order as getColumns()
     */
    public void addRow(@Nonnull List<?> rowData) {
        int size = rowData.size();
        Preconditions.checkArgument(size == getColumns().size());
        ArrayList<Object> myRow = new ArrayList<>(size);
        myRow.addAll(rowData);
        this.data.add(myRow);
    }

    @Override
    public void clear() {
        this.data.clear();
    }

    @Override
    public boolean isUnique() {
        return false;
    }
}
