package br.ufsc.lapesd.qsq.relations;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface InstanceData {
    /**
     * Returns a relation for the given (predicate) name.
     *
     * @param name The name of the relation. Usually a predicate.
     * @return A {@link Relation} instance or null if there is no {@link Relation} mapped
     */
    @Nullable Relation get(@Nonnull String name);

    default  @Nonnull Relation require(@Nonnull String name) {
        Relation relation = get(name);
        if (relation == null)
            throw new IndexOutOfBoundsException("Could not find relation for " + name);
        return relation;
    }
}
