package br.ufsc.lapesd.qsq.relations;

import javax.annotation.Nonnull;
import java.util.stream.Stream;

public interface TBox {
    boolean isSubClassOf(@Nonnull Object subClass, @Nonnull Object superClass);
    default boolean isSuperClassOf(@Nonnull Object superClass, @Nonnull Object subClass) {
        return isSubClassOf(subClass, superClass);
    }
    @Nonnull Stream<Object> streamSubClasses(@Nonnull Object superClass);
    @Nonnull Stream<Object> streamSuperClasses(@Nonnull Object subClass);

    boolean isSubPropertyOf(@Nonnull Object subProperty, @Nonnull Object superProperty);
    default boolean isSuperPropertyOf(@Nonnull Object superProperty, @Nonnull Object subProperty) {
        return isSubPropertyOf(subProperty, superProperty);
    }
    @Nonnull Stream<Object> streamSubProperties(@Nonnull Object superProperty);
    @Nonnull Stream<Object> streamSuperProperties(@Nonnull Object subProperty);
}
