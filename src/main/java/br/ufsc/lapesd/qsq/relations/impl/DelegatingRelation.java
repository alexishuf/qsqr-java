package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static java.lang.String.format;

public class DelegatingRelation implements Relation {
    private @Nonnull Relation delegate;

    public DelegatingRelation(@Nonnull Relation delegate) {
        this.delegate = delegate;
    }

    @Nonnull
    public Relation getDelegate() {
        return delegate;
    }

    @Nullable
    @Override
    public String getName() {
        return delegate.getName() == null ? null : format("Delegate(%s)", delegate.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DelegatingRelation rows = (DelegatingRelation) o;
        return getDelegate().equals(rows.getDelegate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDelegate());
    }

    @Override
    public String toString() {
        return format("DelegatingRelation{%s}", getDelegate());
    }

    @Override
    @Nonnull
    public List<String> getColumns() {
        return delegate.getColumns();
    }

    @Override
    @Nonnull
    public Relation projection(@Nonnull List<String> columns, @Nonnull List<String> newColumns) {
        return delegate.projection(columns, newColumns);
    }

    @Override
    @Nonnull
    public Relation projection(@Nonnull List<String> columns) {
        return delegate.projection(columns);
    }

    @Override
    @Nonnull
    public Relation projection(String... columns) {
        return delegate.projection(columns);
    }

    @Override
    @Nonnull
    public Relation selection(@Nonnull Row equalityQuery) {
        return delegate.selection(equalityQuery);
    }

    @Override
    @Nonnull
    public Iterator<Row> iterator() {
        return delegate.iterator();
    }

    @Override
    @Nonnull
    public Stream<Row> stream() {
        return delegate.stream();
    }

    @Override
    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    @Override
    public boolean isMaterialized() {
        return delegate.isMaterialized();
    }

    @Override
    public boolean isMutable() {
        return delegate.isMutable();
    }

    @Override
    public boolean isUnique() {
        return delegate.isUnique();
    }

    @Override
    public long getSizeEstimate() {
        return delegate.getSizeEstimate();
    }

    @Override
    public void forEach(Consumer<? super Row> action) {
        delegate.forEach(action);
    }

    @Override
    public Spliterator<Row> spliterator() {
        return delegate.spliterator();
    }
}
