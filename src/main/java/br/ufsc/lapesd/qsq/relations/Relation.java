package br.ufsc.lapesd.qsq.relations;

import br.ufsc.lapesd.qsq.relations.impl.IteratorProjection;
import br.ufsc.lapesd.qsq.relations.impl.IteratorSelection;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Relation extends Iterable<Row> {
    interface Factory {
        default @Nonnull Relation create(@Nonnull Collection<String> columns) {
            return create(null, columns);
        }
        @Nonnull Relation create(@Nullable String name, @Nonnull Collection<String> columns);
    }

    @Nullable String getName();

    /**
     * Get the list of columns in this Relation.
     *
     * This list retains the ordering of its creation.
     */
    @Nonnull List<String> getColumns();

    default @Nonnull Relation projection(@Nonnull ProjectionSpec projection) {
        String extra = projection.getSelected().stream().filter(c -> !getColumns().contains(c))
                .collect(Collectors.joining(", "));
        Preconditions.checkArgument(extra.isEmpty(), "Extraneous projection columns: " + extra);
        return new IteratorProjection(this, projection);
    }
    default @Nonnull Relation projection(@Nonnull List<String> columns,
                                         @Nonnull List<String> newColumns) {
        return projection(new ProjectionSpec(columns, newColumns));
    }
    default @Nonnull Relation projection(@Nonnull List<String> columns) {
        return projection(columns, columns);
    }
    default @Nonnull Relation projection(String... columns) {
        List<String> list = Arrays.asList(columns);
        return projection(list, list);
    }

    @Nonnull
    default Relation selection(@Nonnull Row equalityQuery) {
        if (equalityQuery.getColumns().isEmpty())
            return this;
        String extra = equalityQuery.getColumns().stream().filter(c -> !getColumns().contains(c))
                .collect(Collectors.joining(", "));
        Preconditions.checkArgument(extra.isEmpty(), "Extraneous selection columns: " + extra);
        return new IteratorSelection(this, equalityQuery);
    }

    /**
     * Iterates over all rows in the relation.
     *
     * @return an {@link Iterator}, which may not be modifiable, even if isMutable()
     */
    @Nonnull
    Iterator<Row> iterator();

    default @Nonnull Stream<Row> stream() {
        return RelationUtils.stream(this);
    }

    /**
     * Indicates whether the relation is empty (has no rows).
     */
    default boolean isEmpty() {return !iterator().hasNext();}

    /**
     * A materialized relation is one where all rows are stored somewhere (main memory or disk).
     *
     * Materialized relations have a precise number of rows, altough this number may change if
     * the relation is changed concurrently.
     *
     *
     * @return true iff the relation materialized
     */
    boolean isMaterialized();

    /**
     * A mutable relation can have rows added to it.
     *
     * @return true iff the relation is mutable
     */
    boolean isMutable();

    /**
     * A unique relation is one without duplicate rows (duplicate rows are equal on all columns).
     *
     * @return true iff the relation is unique
     */
    boolean isUnique();

    /**
     * @return a best-effort, quickly computed estimate of the row count in this relation.
     */
    long getSizeEstimate();
}
