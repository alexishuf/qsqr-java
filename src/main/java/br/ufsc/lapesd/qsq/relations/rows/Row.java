package br.ufsc.lapesd.qsq.relations.rows;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface Row {
    /**
     * @return true iff the row has the given column. If true, the value may still be null
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean hasColumn(@Nonnull String column);

    /**
     * The list of columns that apply to this row.
     */
    @Nonnull List<String> getColumns();

    /**
     * Gets the object at the given column in this row.
     *
     * Attempting to modify the object is implementation-defined. Some memory-based
     * implementations may transparently persist the modification, while others may return
     * a copy or persist the modification only for an unspecified lifecycle.
     *
     * @param column The column name
     * @return The value at that column in this row, which may be null
     */
    @Nullable Object get(@Nonnull String column);
}
