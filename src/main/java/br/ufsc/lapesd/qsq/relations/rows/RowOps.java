package br.ufsc.lapesd.qsq.relations.rows;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public class RowOps {
    /**
     * Returns true iff rows are exactly equal (same columns and same data)
     */
    public static boolean equals(@Nullable Row l, @Nullable Row r) {
        if ((l == null) != (r == null) ) return false;
        else if (l == null) return true;
        else if (l.getColumns().size() != r.getColumns().size()) return false;
        return isSubset(l, r);
    }

    /**
     * Returns true iff l and r have the same size and {@link RowOps}.equals(l[i], r[i]) for all i.
     */
    public static boolean equals(@Nullable Iterable<? extends Row> l,
                                 @Nullable Iterable<? extends Row> r) {
        if ((l == null) != (r == null)) return false;
        if (l == null) return true;
        Iterator<? extends Row> lit = l.iterator();
        Iterator<? extends Row> rit = r.iterator();
        while (lit.hasNext()) {
            if (!rit.hasNext()) return false;
            if (!equals(lit.next(), rit.next())) return false;
        }
        return !rit.hasNext(); //if l ended, then r should have ended as well
    }

    /**
     * Returns true iff l and r have the same elements, compared by {@link RowOps}.equals().
     */
    public static boolean equals(@Nullable Set<? extends Row> l,
                                 @Nullable Set<? extends Row> r) {
        if ((l == null) != (r == null)) return false;
        if (l == null) return true;
        if (l.size() != r.size()) return false;
        left_loop:
        for (Row left : l) {
            for (Row right : r) {
                if (equals(left, right)) continue left_loop;
            }
            return false; // no equivalent in r found
        }
        return true; // every left had a equivalent right
    }

    /**
     * Returns the list of {@link Row}s present in superset but missing in subset
     */
    public static List<Row> missing(@Nonnull Collection<Row> superset,
                                    @Nonnull Collection<Row> subset) {
        List<Row> list = new ArrayList<>();
        outer:
        for (Row left : superset) {
            for (Row right : subset) {
                if (equals(left, right)) continue outer;
            }
            list.add(left);
        }
        return list;
    }

    /**
     * A row sub is a subset of another row sup iff sup has all columns that sub has,
     * and for all of such column c, <code>sub.get(c) == sup.get(c)</code>
     *
     * @return true iff Row subset is a subset of superset
     */
    public static boolean isSubset(@Nonnull Row subset, @Nonnull Row superset) {
        for (String column : subset.getColumns()) {
            if (!superset.hasColumn(column)) return false;
            if (!Objects.equals(subset.get(column), superset.get(column))) return false;
        }
        return true;
    }

    /**
     * Same as isSubset(Row, Row), but the superset is given as a List and a Map from column
     * to positions in that list
     *
     * @param subset candidate subset row
     * @param col2pos Map from column to positions in the superset list
     * @param list list of values at a row
     * @return true iff subset is a subset of the superset row
     */
    public static boolean isSubset(@Nonnull Row subset, @Nonnull Map<String, Integer> col2pos,
                                   @Nonnull List<?> list) {
        for (String column : subset.getColumns()) {
            Integer pos = col2pos.getOrDefault(column, null);
            if (pos == null) return false;
            if (!Objects.equals(subset.get(column), list.get(pos))) return false;
        }
        return true;
    }

    /**
     * Gets a List with all values in the row, in the same order as {@link Row}.getColumn()
     */
    public static @Nonnull List<Object> toValuesList(@Nonnull Row row) {
        List<Object> list = new ArrayList<>(row.getColumns().size());
        for (String column : row.getColumns()) list.add(row.get(column));
        return list;
    }
}
