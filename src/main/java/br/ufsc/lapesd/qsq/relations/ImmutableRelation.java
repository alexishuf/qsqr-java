package br.ufsc.lapesd.qsq.relations;

public interface ImmutableRelation extends Relation {
    @Override
    default boolean isMutable() {return false;}
}
