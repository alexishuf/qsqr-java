package br.ufsc.lapesd.qsq.relations;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * A relation that allows creating {@link Marker}s
 */
public interface SequencedRelation extends MutableRelation {
    @FunctionalInterface
    interface Factory {
        @Nonnull SequencedRelation create(@Nonnull Collection<String> columns);
    }

    /**
     * Creates a {@link Marker} to monitor future insertions.
     */
    @Nonnull Marker createMarker();
}
