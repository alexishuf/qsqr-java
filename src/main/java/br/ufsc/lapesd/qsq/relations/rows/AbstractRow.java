package br.ufsc.lapesd.qsq.relations.rows;

import javax.annotation.Nonnull;
import java.util.StringJoiner;

public abstract class AbstractRow implements Row {
    public @Nonnull String toString() {
        StringJoiner joiner = new StringJoiner(", ", "{", "}");
        getColumns().stream().map(c -> c + "=" + get(c)).forEach(joiner::add);
        return joiner.toString();
    }
}
