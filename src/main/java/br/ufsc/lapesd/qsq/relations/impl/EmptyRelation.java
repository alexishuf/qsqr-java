package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Marker;
import br.ufsc.lapesd.qsq.relations.MaterializedRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.UniqueSequencedRelation;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.rules.Term;

import javax.annotation.Nonnull;
import java.util.*;

public class EmptyRelation implements MaterializedRelation, UniqueSequencedRelation {
    private @Nonnull List<String> columns;
    private @Nonnull String name;

    public EmptyRelation(@Nonnull String name, @Nonnull List<String> columns) {
        this.name = name;
        this.columns = columns;
    }

    public EmptyRelation() {
        this(Collections.emptyList());
    }

    public EmptyRelation(@Nonnull List<String> columns) {
        this("EmptyRelation", columns);
    }

    public static EmptyRelation fromTerms(@Nonnull List<Term> terms) {
        int i = 0;
        List<String> list = new ArrayList<>();
        for (Term term : terms) {
            if (term.isVariable()) list.add(term.getName());
            else list.add(Integer.toString(i));
            ++i;
        }
        return new EmptyRelation(list);
    }
    public static EmptyRelation fromCount(int columnCount) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < columnCount; i++) list.add(Integer.toString(i));
        return new EmptyRelation(list);
    }

    @Override
    public @Nonnull String getName() {
        return name;
    }

    @Override
    public @Nonnull List<String> getColumns() {
        return columns;
    }

    @Override
    public @Nonnull Iterator<Row> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Row next() {
                throw new NoSuchElementException("EmptyRelation has no rows!");
            }
        };
    }

    @Override
    public boolean isMaterialized() {
        return true;
    }

    @Override
    public long getSize() {
        return 0;
    }

    @Override
    public boolean isUnique() {
        return true;
    }

    @Override
    public long getSizeEstimate() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmptyRelation rows = (EmptyRelation) o;
        return getColumns().equals(rows.getColumns()) &&
                getName().equals(rows.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getColumns(), getName());
    }

    @Nonnull
    @Override
    public Marker createMarker() {
        return new Marker() {
            @Override
            public @Nonnull Relation getRelation() {
                return EmptyRelation.this;
            }

            @Override
            public @Nonnull Iterator<Row> iterator() {
                return new Iterator<>() {
                    @Override
                    public boolean hasNext() {
                        return false;
                    }

                    @Override
                    public Row next() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    @Override
    public boolean addRow(@Nonnull Row row) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        /* no-op */
    }
}
