package br.ufsc.lapesd.qsq.relations.rows;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import javax.annotation.Nonnull;
import java.lang.reflect.Type;

public class RowGsonSerializer implements JsonSerializer<Row> {
    @Override
    public JsonElement serialize(@Nonnull Row src, @Nonnull Type typeOfSrc,
                                 @Nonnull JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        for (String column : src.getColumns())
            obj.add(column, context.serialize(src.get(column)));
        return obj;
    }
}
