package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.InstanceData;
import br.ufsc.lapesd.qsq.relations.Relation;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class MapInstanceData implements InstanceData {
    private @Nonnull Map<String, Relation> map;

    public MapInstanceData() {
        map = new HashMap<>();
    }

    /**
     * Adds a new mapping from the given name to the {@link Relation}.
     *
     * This discards the previously mapped {@link Relation} and returns it.
     *
     * @param name String naming the {@link Relation}, usually a predicate name
     * @param relation The {@link Relation}
     * @return previously mapped {@link Relation} or null.
     */
    public Relation set(@Nonnull String name, @Nonnull Relation relation) {
        return map.put(name, relation);
    }

    @Override
    public @Nullable Relation get(@Nonnull String name) {
        return map.getOrDefault(name, null);
    }
}
