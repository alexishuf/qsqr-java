package br.ufsc.lapesd.qsq.relations.rows;

import br.ufsc.lapesd.qsq.relations.ProjectionSpec;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class IndexedListRow extends AbstractRow {
    private @Nonnull Map<String, Integer> col2pos;
    private @Nonnull List<String> columns;
    private @Nonnull List<?> data;

    public static class Factory {
        private @Nonnull Map<String, Integer> col2pos;
        private @Nonnull List<String> columns;

        public Factory(@Nonnull List<String> columns) {
            this.columns = columns;
            this.col2pos = createCol2Pos(columns);
        }

        public @Nonnull IndexedListRow create(List<?> data) {
            return new IndexedListRow(col2pos, columns, data);
        }
        public @Nonnull IndexedListRow create(@Nonnull Row row) {
            String missing = columns.stream().filter(c -> !row.hasColumn(c))
                    .collect(Collectors.joining(", "));
            Preconditions.checkArgument(missing.isEmpty(), "Missing columns at row: " + missing);

            List<Object> list = new ArrayList<>();
            for (String column : columns) list.add(row.get(column));
            return new IndexedListRow(col2pos, columns, list);
        }
        public @Nonnull IndexedListRow createPositional(@Nonnull Row row) {
            Preconditions.checkArgument(row.getColumns().size() == columns.size());

            List<Object> list = new ArrayList<>(columns.size());
            for (String column : row.getColumns()) list.add(row.get(column));
            return new IndexedListRow(col2pos, columns, list);
        }

        public @Nonnull List<String> getColumns() {
            return columns;
        }

        public int getLength() {
            return columns.size();
        }
    }

    public static class ProjectingFactory  {
        private @Nonnull Map<String, Integer> col2pos;
        private @Nonnull List<String> columns;
        private final @Nonnull ProjectionSpec projection;
        private @Nullable Row override;

        public ProjectingFactory(@Nonnull ProjectionSpec projection, @Nullable Row override) {
            this.col2pos = createCol2Pos(this.columns = projection.getRenamed());
            this.projection = projection;
            this.override = override;
        }
        public ProjectingFactory(@Nonnull ProjectionSpec projection) {
            this(projection, null);
        }

        public @Nonnull IndexedListRow project(@Nonnull Row row) {
            int size = columns.size();
            List<Object> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                String selected = projection.getSelected().get(i);
                if (override != null && override.hasColumn(selected))
                    list.add(override.get(selected));
                else
                    list.add(row.get(selected));
            }
            return new IndexedListRow(col2pos, columns, list);
        }
    }


    public IndexedListRow(@Nonnull Map<String, Integer> col2pos,
                          @Nonnull List<String> columns,
                          @Nonnull List<?> data) {
        Preconditions.checkArgument(!columns.isEmpty(), "Empty columns list");
        Preconditions.checkArgument(columns.size() >= col2pos.keySet().size(),
                format("columns/col2pos size mismatch: %d != %d",
                        columns.size(), col2pos.keySet().size()));
        Preconditions.checkArgument(data.size() >= columns.size(),
                format("data < columns: %d < %d", data.size(), columns.size()));

        assert col2pos.values().stream().distinct().count() == columns.size();
        assert col2pos.keySet().equals(new HashSet<>(columns));
        assert col2pos.values().stream().max(Integer::compareTo).orElse(0) < data.size();
        assert col2pos.values().stream().min(Integer::compareTo).orElse(0) >= 0;

        this.col2pos = col2pos;
        this.columns = columns;
        this.data = new ArrayList<>(data);
    }

    public IndexedListRow(@Nonnull List<String> columns,
                          @Nonnull List<?> data) {
        this(createCol2Pos(columns), columns, data);
    }

    public IndexedListRow(@Nonnull Row row) {
        this(row.getColumns(), RowOps.toValuesList(row));
    }

    private static Map<String, Integer> createCol2Pos(List<String> columns) {
        Map<String, Integer> map = new HashMap<>();
        Iterator<String> it = columns.iterator();
        for (int i = 0; i < columns.size(); i++) {
            Integer old = map.put(it.next(), i);
            Preconditions.checkArgument(old == null, "Column " + columns.get(i) + " is duplicated");
        }
        return map;
    }

    @Override
    public boolean hasColumn(@Nonnull String column) {
        return col2pos.containsKey(column);
    }

    @Nonnull
    @Override
    public List<String> getColumns() {
        return columns;
    }

    @Nullable
    @Override
    public Object get(@Nonnull String column) {
        Integer pos = col2pos.get(column);
        Preconditions.checkArgument(pos != null, column + " is not a column for this row");
        return data.get(pos);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexedListRow that = (IndexedListRow) o;
        return col2pos.equals(that.col2pos) &&
                getColumns().equals(that.getColumns()) &&
                data.equals(that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(col2pos, getColumns(), data);
    }
}
