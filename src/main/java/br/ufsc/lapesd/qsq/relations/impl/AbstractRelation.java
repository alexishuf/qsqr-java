package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;

public abstract class AbstractRelation  implements Relation {
    protected @Nonnull List<String> columns;
    protected @Nonnull Map<String, Integer> col2pos;
    protected @Nullable String name;

    protected AbstractRelation(@Nonnull List<String> columns,
                           @Nonnull Map<String, Integer> col2pos) {
        this(null, columns, col2pos);
    }

    protected AbstractRelation(@Nullable String name, @Nonnull List<String> columns,
                               @Nonnull Map<String, Integer> col2pos) {
        Preconditions.checkArgument(columns.size() > 0, "Cannot have a relation with no column");
        Preconditions.checkArgument(col2pos.keySet().size() == columns.size(),
                "Size mismatch between columns and col2pos");
        assert col2pos.values().stream().distinct().count() == columns.size() :
                "There are two column names mapping to the same position";
        this.name = name;
        this.columns = columns;
        this.col2pos = col2pos;
    }

    protected static Map<String, Integer> createCol2Pos(List<String> columns) {
        int arity = columns.size();
        HashMap<String, Integer> col2pos = new HashMap<>(arity + 1, 1);
        for (int i = 0; i < arity; i++) col2pos.put(columns.get(i), i);
        return col2pos;
    }

    @Override
    public @Nullable String getName() {
        return name;
    }

    @Override
    public @Nonnull List<String> getColumns() {
        return columns;
    }

    protected String getMissingColumns(@Nonnull Row row) {
        return columns.stream()
                    .filter(c -> !row.hasColumn(c)).collect(Collectors.joining(", "));
    }

    @Nonnull
    protected ArrayList<Object> toArrayList(@Nonnull Row row) {
        String missing = getMissingColumns(row);
        Preconditions.checkArgument(missing.isEmpty(), "Missing columns in row: " + missing);
        return getColumns().stream().map(row::get)
                .collect(toCollection(() -> new ArrayList<>(getColumns().size())));
    }
}
