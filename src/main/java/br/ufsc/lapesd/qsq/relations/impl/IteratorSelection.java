package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.RelationUtils;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class IteratorSelection implements Relation {
    private @Nonnull Iterable<Row> iterable;
    private @Nullable Relation base;
    private @Nonnull Row query;

    public IteratorSelection(@Nonnull Relation base, @Nonnull Row query) {
        this.iterable = this.base = base;
        this.query = query;
    }

    public IteratorSelection(@Nonnull Iterable<Row> iterable, @Nonnull Row query) {
        this.iterable = iterable;
        this.base = null;
        this.query = query;
    }

    @Nullable
    @Override
    public String getName() {
        return base != null && base.getName() != null ? base.getName() + "@" + query : null;
    }

    @Nonnull
    @Override
    public List<String> getColumns() {
        return RelationUtils.getColumns(iterable);
    }

    @Nonnull
    @Override
    public Iterator<Row> iterator() {
        Iterator<Row> baseIt = iterable.iterator();
        return new Iterator<>() {
            private Row nextValue = null;

            private boolean advance() {
                while (baseIt.hasNext()) {
                    Row row = baseIt.next();
                    if (RowOps.isSubset(query, row)) {
                        nextValue = row;
                        return true;
                    }
                }
                return false;
            }

            @Override
            public boolean hasNext() {
                return nextValue != null || advance();
            }

            @Override
            public Row next() {
                if (nextValue == null && !advance())
                    throw new NoSuchElementException();
                Row value = nextValue;
                nextValue = null;
                return value;
            }
        };
    }

    @Override
    public boolean isMaterialized() {
        return false;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public boolean isUnique() {
        return base != null && base.isUnique();
    }

    @Override
    public long getSizeEstimate() {
        if (base != null) return base.getSizeEstimate();
        if (iterable instanceof Collection) return ((Collection<Row>) iterable).size();
        else return iterable.iterator().hasNext() ? 10 : 0;
    }
}
