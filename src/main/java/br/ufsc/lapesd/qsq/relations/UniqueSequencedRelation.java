package br.ufsc.lapesd.qsq.relations;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

/**
 * A {@link UniqueRelation} and {@link SequencedRelation}.
 */
public interface UniqueSequencedRelation extends UniqueRelation, SequencedRelation {
    interface Factory extends Relation.Factory {
        @Override
        default @Nonnull UniqueSequencedRelation create(@Nonnull Collection<String> columns) {
            return create(null, columns);
        }

        @Override
        @Nonnull UniqueSequencedRelation create(@Nullable String name, @Nonnull Collection<String> columns);
    }
}
