package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.Marker;
import br.ufsc.lapesd.qsq.relations.MaterializedRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.UniqueSequencedRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

public class NaiveUniqueSequencedRelation extends AbstractRelation
        implements UniqueSequencedRelation, MaterializedRelation {
    private @Nonnull LinkedList<List<List<Object>>> segments;
    private int size;
    private @Nonnull HashSet<List<Object>> set;

    public static class Factory implements UniqueSequencedRelation.Factory {
        @Override
        public @Nonnull UniqueSequencedRelation create(@Nullable String name,
                                                       @Nonnull Collection<String> columns) {
            return new NaiveUniqueSequencedRelation(name, columns);
        }
    }

    private class RowIterator implements Iterator<Row> {
        private int segment, row;

        private boolean advance() {
            for (; segment < segments.size(); ++segment, row = -1) {
                if (++row < segments.get(segment).size())
                    return true;
            }
            return false;
        }

        public RowIterator(int segment, int row) {
            this.segment = segment;
            this.row = row;
        }

        public RowIterator() { this(0, -1); }

        @Override
        public boolean hasNext() {
            return new RowIterator(segment, row).advance();
        }

        @Override
        public Row next() {
            Preconditions.checkArgument(advance());
            return new IndexedListRow(col2pos, columns, segments.get(segment).get(this.row));
        }
    }

    private class SegmentMarker implements Marker {
        private final int idx;

        public SegmentMarker(int idx) {
            assert idx > 0;
            assert idx < segments.size();
            this.idx = idx;
        }

        @Nonnull
        @Override
        public Relation getRelation() {
            return NaiveUniqueSequencedRelation.this;
        }

        @Nonnull
        @Override
        public Iterator<Row> iterator() {
            return new RowIterator(idx, -1);
        }

        @Override
        public String toString() { return String.format("SegmentMarker(%d)", idx); }
    }

    protected NaiveUniqueSequencedRelation(@Nullable String name, @Nonnull List<String> columns) {
        super(name, Collections.unmodifiableList(columns), createCol2Pos(columns));
        this.segments = new LinkedList<>();
        this.segments.add(new ArrayList<>(20));
        this.set = new HashSet<>();
    }
    protected NaiveUniqueSequencedRelation(@Nonnull List<String> columns) {
        this(null, columns);
    }
    public NaiveUniqueSequencedRelation(@Nonnull Collection<String> columns) {
        this(new ArrayList<>(columns));
    }
    public NaiveUniqueSequencedRelation(@Nullable String name, @Nonnull Collection<String> columns) {
        this(name, new ArrayList<>(columns));
    }

    @Nonnull
    @Override
    public Marker createMarker() {
        int idx = segments.size();
        segments.add(new ArrayList<>(idx == 0 ? 20 : segments.getLast().size()));
        return new SegmentMarker(idx);
    }

    @Override
    public boolean addRow(@Nonnull Row row) {
        assert !segments.isEmpty();
        String missing = getColumns().stream().filter(c -> !row.hasColumn(c))
                .collect(Collectors.joining(", "));
        Preconditions.checkArgument(missing.isEmpty(), "Missing columns in row: " + missing);
        ArrayList<Object> list = toArrayList(row);
        if (set.add(list)) {
            segments.getLast().add(list);
            ++size;
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        set.clear();
        // do not remove the segments themselves, otherwise we would invalidate existing Markers
        segments.forEach(List::clear);
        size = 0;
    }

    @Nonnull
    @Override
    public Iterator<Row> iterator() {
        return new RowIterator();
    }

    @Override
    public boolean isMaterialized() {
        return true;
    }

    @Override
    public long getSize() {
        return size;
    }

    @Override
    public long getSizeEstimate() {
        return getSize();
    }

}
