package br.ufsc.lapesd.qsq.relations;

public interface UniqueRelation extends Relation {
    @Override
    default boolean isUnique() { return true; }
}
