package br.ufsc.lapesd.qsq.relations;

import br.ufsc.lapesd.qsq.relations.rows.Row;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.stream.Collectors.joining;

public class ProjectionSpec {
    private @Nonnull List<String> selected;
    private @Nonnull List<String> renamed;

    public ProjectionSpec(@Nonnull List<String> selected, @Nonnull List<String> renamed) {
        Preconditions.checkArgument(selected.size() == renamed.size(),
                format("Selected and renamed size mismatch: %d != %d",
                        selected.size(), renamed.size()));
        Preconditions.checkArgument(!selected.isEmpty(), "Projection with no columns not allowed!");
        Preconditions.checkArgument(selected.stream().distinct().count() == selected.size(),
                "Repeated columns in selected");
        Preconditions.checkArgument(renamed.stream().distinct().count() == renamed.size(),
                "Repeated columns in renamed");
        this.selected = Collections.unmodifiableList(new ArrayList<>(selected));
        this.renamed = Collections.unmodifiableList(new ArrayList<>(renamed));
    }

    protected ProjectionSpec(@Nonnull Collection<String> columns) {
        Preconditions.checkArgument(columns.stream().distinct().count() == columns.size(),
                "Repeated columns");
        this.selected = this.renamed = Collections.unmodifiableList(new ArrayList<>(columns));
    }

    public static ProjectionSpec nop(@Nonnull Collection<String> columns) {
        return new ProjectionSpec(columns);
    }

    public static ProjectionSpec nop(@Nonnull Row row) {
        return new ProjectionSpec(row.getColumns());
    }


    /**
     * Creates a projection for the intersection between desired and aliases, desired gives the
     * selected columns and columnNames which is assumed to positionally correspond to the items
     * in aliases gives the renamed columns.
     *
     * @param desired Columns that should be selected
     * @param aliases List of aliases for the columns in columnNames
     * @param columnNames List of columns that will yield the renamed columns in the projection
     */
    public static @Nonnull ProjectionSpec intersectWithAliased(@Nonnull Collection<String> desired,
                                                               @Nonnull List<String> aliases,
                                                               @Nonnull List<String> columnNames) {
        Preconditions.checkArgument(!desired.isEmpty());
        Preconditions.checkArgument(aliases.size() == columnNames.size());
        List<String> selected = new ArrayList<>(), renamed = new ArrayList<>();
        for (int i = 0; i < aliases.size(); i++) {
            String column = aliases.get(i);
            if (desired.contains(column)) {
                selected.add(column);
                renamed.add(columnNames.get(i));
            }
        }
        return new ProjectionSpec(selected, renamed);
    }

    /**
     * Create a no-op projection from the intersection between the two column collections.
     */
    public static @Nonnull ProjectionSpec intersectWith(@Nonnull Collection<String> left,
                                                        @Nonnull Collection<String> right) {
        return nop(left.stream().filter(right::contains).collect(Collectors.toList()));
    }

    /**
     * Return a {@link ProjectionSpec} that contains all mappings of spec appended to the right
     * of the ones in this instance.
     */
    public @Nonnull ProjectionSpec append(@Nonnull ProjectionSpec spec) {
        String repeatedSelected = spec.getSelected().stream().filter(getSelected()::contains)
                .collect(joining(", "));
        String repeatedRenamed = spec.getRenamed().stream().filter(getRenamed()::contains)
                .collect(joining(", "));
        Preconditions.checkArgument(repeatedSelected.isEmpty(),
                "There are repeated selected columns " + repeatedSelected);
        Preconditions.checkArgument(repeatedRenamed.isEmpty(),
                "There are repeated renamed columns " + repeatedRenamed);

        List<String> newSelected = new ArrayList<>(getSelected());
        List<String> newRenamed = new ArrayList<>(getRenamed());
        newSelected.addAll(spec.getSelected());
        newRenamed.addAll(spec.getRenamed());
        return new ProjectionSpec(newSelected, newRenamed);
    }

    public @Nonnull ProjectionSpec reversed() {
        return new ProjectionSpec(renamed, selected);
    }

    public @Nonnull ProjectionSpec overridden(@Nonnull ProjectionSpec other) {
        ArrayList<String> selected = new ArrayList<>(this.selected);
        ArrayList<String> renamed = new ArrayList<>(this.renamed);
        for (int i = 0; i < other.getColumns(); i++) {
            String otherRenamed = other.getRenamed().get(i);
            int idx = renamed.indexOf(otherRenamed);
            if (idx >= 0) { // update selected
                selected.set(idx, other.getSelected().get(i));
            } else { // add new mapping
                selected.add(other.getSelected().get(i));
                renamed.add(otherRenamed);
            }
        }
        return new ProjectionSpec(selected, renamed);
    }

    public boolean isNop() {
        return selected == renamed;
    }

    public int getColumns() {
        return selected.size();
    }

    public @Nonnull List<String> getSelected() {
        return selected;
    }

    public @Nonnull String getSelected(@Nonnull String renamed) {
        int idx = this.renamed.indexOf(renamed);
        if (idx < 0) throw new NoSuchElementException(renamed);
        return selected.get(idx);
    }

    public @Nonnull List<String> getRenamed() {
        return renamed;
    }

    public @Nonnull String getRenamed(@Nonnull String selected) {
        int idx = this.selected.indexOf(selected);
        if (idx < 0) throw new NoSuchElementException(selected);
        return renamed.get(idx);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectionSpec that = (ProjectionSpec) o;
        return selected.equals(that.selected) &&
                renamed.equals(that.renamed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(selected, renamed);
    }

    @Override
    public String toString() {
        return format("%s -> %s", join(", ", selected), join(", ", renamed));
    }
}
