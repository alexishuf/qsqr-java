package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.ImmutableRelation;
import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

public class IteratorRelation implements ImmutableRelation {
    private @Nonnull List<String> columns;
    private @Nonnull Supplier<Iterator<Row>> iteratorSupplier;
    private @Nullable String name;
    private boolean unique;

    public IteratorRelation(@Nonnull List<String> columns,
                            @Nonnull Supplier<Iterator<Row>> iteratorSupplier, boolean unique) {
        this(columns, iteratorSupplier, unique, null);
    }

    public IteratorRelation(@Nonnull List<String> columns,
                            @Nonnull Supplier<Iterator<Row>> iteratorSupplier,
                            boolean unique, @Nullable String name) {
        this.columns = Collections.unmodifiableList(new ArrayList<>(columns));
        this.iteratorSupplier = iteratorSupplier;
        this.name = name;
        this.unique = unique;
    }

    @Override
    public @Nullable String getName() {
        return name;
    }

    @Override
    public @Nonnull List<String> getColumns() {
        return columns;
    }

    @Override
    public @Nonnull Iterator<Row> iterator() {
        return iteratorSupplier.get();
    }

    @Override
    public boolean isMaterialized() {
        return false;
    }

    @Override
    public boolean isUnique() {
        return unique;
    }

    @Override
    public long getSizeEstimate() {
        return 1;
    }
}
