package br.ufsc.lapesd.qsq.relations;

import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.Spliterators.spliteratorUnknownSize;

public interface Marker extends Iterable<Row> {
    @Nonnull Relation getRelation();

    /**
     * Returns an iterator that iterates only over items added after the creation of this Marker
     */
    @Override
    @Nonnull Iterator<Row> iterator();

    default @Nonnull Stream<Row> stream() {
        int chars = Spliterator.NONNULL;
        if (getRelation().isUnique())
            chars |= Spliterator.DISTINCT;
        return StreamSupport.stream(spliteratorUnknownSize(iterator(), chars), false);
    }

    /**
     * A Marker is empty iff no row has been added to its {@link Relation} since its creation.
     */
    default boolean isEmpty() { return !iterator().hasNext(); }
}
