package br.ufsc.lapesd.qsq.relations.impl;

import br.ufsc.lapesd.qsq.relations.ImmutableRelation;
import br.ufsc.lapesd.qsq.relations.MaterializedRelation;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.String.format;

public class ArrayListRelation extends AbstractRelation implements MaterializedRelation {
    protected @Nonnull List<ArrayList<Object>> data;

    protected ArrayListRelation(@Nullable String name, @Nonnull List<String> columns,
                                @Nonnull Map<String, Integer> col2pos,
                                @Nonnull List<ArrayList<Object>> data) {
        super(name, columns, col2pos);
        assert data.stream().map(Collection::size).min(Integer::compareTo)
                .orElse(columns.size()) >= columns.size() :
                "There are some rows on data with less columns tha in the schema!";
        this.data = data;
    }

    public ArrayListRelation(@Nonnull List<String> columns) {
        this(null, columns);
    }

    public ArrayListRelation(@Nullable String name, @Nonnull List<String> columns) {
        this(name, Collections.unmodifiableList(columns), createCol2Pos(columns), new ArrayList<>());
    }

    @Override
    public long getSize() {
        return data.size();
    }

    @Override
    public @Nonnull Relation projection(@Nonnull List<String> columns, @Nonnull List<String> newColumns) {
        Preconditions.checkArgument(!columns.isEmpty());
        Preconditions.checkArgument(columns.size() == newColumns.size(),
                format("columns lists size mismatch: %d != %d",
                        columns.size(), newColumns.size()));
        Map<String, Integer> renamed2pos = new HashMap<>(columns.size()+1, 1);
        List<String> renamed = Collections.unmodifiableList(newColumns);
        for (int i = 0; i < renamed.size(); i++) {
            String column = columns.get(i);
            assert col2pos.getOrDefault(column, null) != null : column + " missing in col2pos";
            renamed2pos.put(renamed.get(i), col2pos.get(column));
        }
        String name = null;
        if (getName() != null) {
            name = String.format("%s[%s]", getName(), IntStream.range(0, renamed.size()).boxed()
                    .map(i ->  renamed.get(i) + "=" + columns.get(i))
                    .collect(Collectors.joining(", ")));
        }
        return new View(name, renamed, renamed2pos, data);
    }

    @Override
    public @Nonnull Relation projection(@Nonnull List<String> columns) {
        return projection(columns, columns);
    }

    @Override
    public @Nonnull Relation selection(@Nonnull Row query) {
        LinkedList<ArrayList<Object>> subset = data.stream()
                .filter(list -> RowOps.isSubset(query, col2pos, list))
                .collect(Collectors.toCollection(LinkedList::new));
        return new View(name != null ? name + "@" + query : null, columns, col2pos, subset);
    }

    @Override
    public @Nonnull Iterator<Row> iterator() {
        return data.stream().map(r -> (Row)new IndexedListRow(col2pos, columns, r)).iterator();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public boolean isUnique() {
        return false;
    }

    @Override
    public long getSizeEstimate() {
        return data.size();
    }

    public static class View extends ArrayListRelation implements ImmutableRelation {
        private View(@Nullable String name, @Nonnull List<String> columns,
                     @Nonnull Map<String, Integer> col2pos,
                     @Nonnull List<ArrayList<Object>> data) {
            super(name, columns, col2pos, data);
        }
    }
}
