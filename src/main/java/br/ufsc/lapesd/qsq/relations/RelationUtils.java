package br.ufsc.lapesd.qsq.relations;

import br.ufsc.lapesd.qsq.relations.impl.IteratorSelection;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.String.format;
import static java.util.Spliterators.spliteratorUnknownSize;

public class RelationUtils {
    /**
     * Adds all given rows into out, ignoring column names and assuming that a positional
     * mapping between columns is valid.
     *
     * @param rows input rows. All must have the same set of columns
     * @param out output relation where new rows will be inserted
     * @return true iff new rows where effectively added
     */
    public static boolean addPositional(@Nonnull Iterable<Row> rows, @Nonnull MutableRelation out) {
        Iterator<Row> peekIt = rows.iterator();
        if (!peekIt.hasNext())
            return false;
        int inCount = peekIt.next().getColumns().size();
        int outCount = out.getColumns().size();
        Preconditions.checkArgument(inCount == outCount,
                format("Column count mismatch: %d != %d", inCount, outCount));

        boolean added = false;
        IndexedListRow.Factory factory = new IndexedListRow.Factory(out.getColumns());
        for (Row row : rows) {
            IndexedListRow projected = factory.createPositional(row);
            added |= out.addRow(projected);
        }
        return added;
    }

    public static @Nonnull Stream<Row> stream(@Nonnull Relation relation) {
        int chars = Spliterator.NONNULL;
        if (relation.isUnique())
            chars |= Spliterator.DISTINCT;
        Spliterator<Row> spliterator = spliteratorUnknownSize(relation.iterator(), chars);
        return StreamSupport.stream(spliterator, false);
    }

    public static @Nonnull List<String> getColumns(@Nonnull Iterable<Row> iterable) {
        if (iterable instanceof Relation) {
            return ((Relation) iterable).getColumns();
        } else if (iterable instanceof Marker) {
            return ((Marker) iterable).getRelation().getColumns();
        } else {
            Iterator<Row> it = iterable.iterator();
            return it.hasNext() ? it.next().getColumns() : Collections.emptyList();
        }
    }

    public static @Nonnull Iterable<Row> select(@Nonnull Iterable<Row> iterable,
                                                @Nonnull Row query) {
        if (iterable instanceof Relation)
            return ((Relation) iterable).selection(query);
        return new IteratorSelection(iterable, query);
    }

    public static void  toCSV(@Nonnull Relation relation, @Nonnull OutputStream out) {
        try (PrintStream ps = new PrintStream(out, true, StandardCharsets.UTF_8.name())) {
            writeCSVRow(relation.getColumns(), ps);
            for (Row row : relation)
                writeCSVRow(RowOps.toValuesList(row), ps);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e); // never occurs
        }
    }

    private static void writeCSVRow(@Nonnull Collection<?> values, @Nonnull PrintStream out) {
        int idx = 0;
        for (Object value : values) {
            String str = value.toString();
            str = str.replaceAll("(?<!\\\\)\"", "\\\\\"");
            if (str.contains(","))
                str = "\"" + str + "\"";
            out.printf("%s%s", idx++ > 0 ? ", " : "", str);
        }
        out.print("\r\n");
    }
}
