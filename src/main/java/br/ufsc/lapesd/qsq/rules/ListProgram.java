package br.ufsc.lapesd.qsq.rules;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListProgram implements Program {
    private @Nonnull List<Rule> rules;
    private @Nullable String name;
    private @Nonnull Multimap<String, Integer> predicate2rule;

    public ListProgram(@Nonnull List<Rule> rules, @Nullable String name) {
        this.name = name;
        this.rules = rules;
        this.predicate2rule = ArrayListMultimap.create(rules.size(), 4);
        for (int i = 0; i < rules.size(); i++) predicate2rule.put(rules.get(i).getPredicate(), i);
    }
    public ListProgram(@Nonnull List<Rule> rules) {
        this(rules, null);
    }
    public ListProgram(@Nonnull Rule... rules) { this(Arrays.asList(rules)); }

    @Override
    public @Nullable String getName() {
        return name;
    }
    @Override
    public @Nonnull Rule getRule(int i) { return rules.get(i); }

    @Override
    public @Nullable Rule getRule(@Nonnull String name) {
        for (Rule rule : rules) {
            if (Objects.equals(rule.getName(), name)) return rule;
        }
        return null;
    }

    @Override
    public @Nonnull List<Rule> getRules() { return rules; }
    @Override
    public int getRuleCount() { return rules.size();}

    @Override
    public boolean isIntensional(@Nonnull String predicate) {
        return predicate2rule.containsKey(predicate);
    }

    @Override
    public @Nonnull Stream<Integer> streamIndices(@Nonnull String predicate) {
        return predicate2rule.get(predicate).stream();
    }

    @Override
    public @Nonnull Stream<Rule> streamRules() { return rules.stream(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListProgram that = (ListProgram) o;
        return getRules().equals(that.getRules()) &&
                Objects.equals(getName(), that.getName()) &&
                predicate2rule.equals(that.predicate2rule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRules(), getName(), predicate2rule);
    }

    @Override
    public String toString() {
        if (getName() != null)
            return String.format("[%s]", getName());
        else
            return rules.stream().map(Object::toString).collect(Collectors.joining("\n"));
    }
}
