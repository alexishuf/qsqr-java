package br.ufsc.lapesd.qsq.rules;

import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class AtomUtils {
    public static @Nonnull List<Integer> getVarIndices(@Nonnull Atom atom) {
        List<Integer> list = new ArrayList<>(atom.getTerms().size());
        int idx = 0;
        for (Term term : atom.getTerms()) {
            if (term.isVariable())
                list.add(idx);
            ++idx;
        }
        return list;
    }

    public static @Nonnull List<String> getBoundVarNamesOrNull(@Nonnull AdornedAtom atom) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < atom.getTerms().size(); i++) list.add(null);
        for (int i : atom.getAdornment().getBound()) {
            Term term = atom.getTerms().get(i);
            if (term.isVariable())
                list.set(i, term.getName());
        }
        return list;

    }

    public static @Nonnull List<String> getBoundVarNames(@Nonnull AdornedAtom atom) {
        List<String> list = new ArrayList<>();
        for (Term term : atom.getBound()) {
            if (term.isVariable()) list.add(term.getName());
        }
        return list;
    }

    public static @Nonnull List<String> getVarNamesOrNull(@Nonnull Atom atom) {
        List<String> list = new ArrayList<>(atom.getTerms().size());
        for (Term term : atom.getTerms())
            list.add(term.isVariable() ? term.getName() : null);
        return list;
    }

    public static @Nonnull List<String> getVarNames(@Nonnull Atom atom) {
        List<String> list = new ArrayList<>(atom.getTerms().size());
        for (Term term : atom.getTerms()) {
            if (term.isVariable()) list.add(term.getName());
        }
        return list;
    }

    public static int countVars(@Nonnull Atom atom) {
        int count = 0;
        for (Term term : atom.getTerms()) {
            if (term.isVariable()) ++count;
        }
        return count;
    }

    public static @Nonnull Relation projectPerAdornment(@Nonnull Relation relation,
                                                        @Nonnull AdornedAtom atom) {
        List<String> columns = relation.getColumns();
        List<String> wanted = new ArrayList<>();
        int i = 0;
        for (Term term : atom.getTerms()) {
            if (term.isVariable()) wanted.add(columns.get(i));
            ++i;
        }
        return relation.projection(wanted, getVarNames(atom));
    }

    public static @Nonnull Row rowFromBound(@Nonnull List<String> columns,
                                            @Nonnull Atom atom) {
        List<Term> terms = atom.getTerms();
        Preconditions.checkArgument(columns.size() == terms.size(), "Size mismatch");
        List<String> headers = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        for (int i = 0; i < terms.size(); i++) {
            Term term = terms.get(i);
            if (!term.isVariable()) {
                headers.add(columns.get(i));
                values.add(term.getValue());
            }
        }
        return new IndexedListRow(headers, values);
    }
}
