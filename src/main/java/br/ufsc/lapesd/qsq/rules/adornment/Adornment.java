package br.ufsc.lapesd.qsq.rules.adornment;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Adornment {
    public enum Term {
        FREE,
        BOUND;

        public char character() {
            switch (this) {
                case FREE: return 'f';
                case BOUND: return 'b';
            }
            throw new UnsupportedOperationException("Unknown Term value: " + this);
        }
        public String letter() {
            return "" + character();
        }

        public static Term fromChar(char c) {
            switch (c) {
                case 'f': return FREE;
                case 'b': return BOUND;
            }
            throw new IllegalArgumentException("Unknown Term char: " + c);
        }
    }

    private final @Nonnull String adornment;

    public Adornment(@Nonnull List<Term> adornment) {
        this.adornment = adornment.stream().map(Term::letter).collect(Collectors.joining());
    }
    public Adornment(@Nonnull Term... adornment) {
        this(Arrays.asList(adornment));
    }
    public Adornment(@Nonnull String adornment) {
        requireValidAdornmentString(adornment);
        this.adornment = adornment;
    }

    public static Adornment all(@Nonnull Term term, int length) {
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; i++) builder.append(term.character());
        return new Adornment(builder.toString());
    }

    public static void requireValidAdornmentString(@Nonnull String string) {
        Preconditions.checkArgument(!string.isEmpty());
        for (int i = 0; i < string.length(); i++)
            Preconditions.checkArgument(Term.fromChar(string.charAt(i)) != null);
    }

    public boolean isBound(int idx) {
        return get(idx) == Term.BOUND;
    }
    public boolean isFree(int idx) {
        return get(idx) == Term.FREE;
    }

    public int size() {
        return adornment.length();
    }

    public @Nonnull Term get(int idx) {
        return Term.fromChar(adornment.charAt(idx));
    }

    public @Nonnull int[] getBound() {
        return getTermPositions(Term.BOUND);
    }
    public @Nonnull int[] getFree() {
        return getTermPositions(Term.FREE);
    }

    public @Nonnull int[] getTermPositions(@Nonnull Term term) {
        char character = term.character();
        int count = 0;
        for (int i = 0; i < adornment.length(); i++) {
            if (adornment.charAt(i) == character) ++count;
        }
        int[] list = new int[count];
        count = 0;
        for (int i = 0; i < adornment.length(); i++) {
            if (adornment.charAt(i) == character) list[count++] = i;
        }
        return list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Adornment adornment = (Adornment) o;
        return this.adornment.equals(adornment.adornment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adornment);
    }

    @Override
    public String toString() {
        return adornment;
    }
}
