package br.ufsc.lapesd.qsq.rules.parser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.regex.Matcher;

public class PlainConstantParser implements ConstantParser {
    @Override
    public @Nullable Parse parse(@Nonnull String text) {
        Matcher matcher = DatalogParser.Token.CONSTANT.rx.matcher(text);
        if (matcher.find()) {
            String value = null;
            if      (matcher.group(1) != null) value = matcher.group(1);
            else if (matcher.group(2) != null) value = matcher.group(2);
            else if (matcher.group(3) != null) value = matcher.group(3);
            assert value != null : "Pattern regex does not match the Java code!";
            String erased = matcher.replaceFirst("");
            return new Parse(value, text.length() - erased.length());
        }
        return null;
    }

    @Override
    public boolean tryPrefix(@Nonnull String line) {
        return false;
    }
}
