package br.ufsc.lapesd.qsq.rules.plain;

import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Term;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PlainAtom implements Atom {
    private @Nonnull String predicate;
    private @Nonnull List<Term> terms;

    public PlainAtom(@Nonnull String predicate, @Nonnull List<Term> terms) {
        Preconditions.checkArgument(terms.size() > 0);
        Preconditions.checkArgument(predicate.length() > 0);
        this.predicate = predicate;
        this.terms = Collections.unmodifiableList(terms);
    }

    public PlainAtom(@Nonnull String predicate, Term... terms) {
        this(predicate, Arrays.asList(terms));
    }

    @Override
    @Nonnull
    public String getPredicate() {
        return predicate;
    }

    @Override
    @Nonnull
    public List<Term> getTerms() {
        return terms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Atom atom = (Atom) o;
        return getPredicate().equals(atom.getPredicate()) &&
                getTerms().equals(atom.getTerms());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPredicate(), getTerms());
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", getPredicate(),
                getTerms().stream().map(Term::toString).collect(Collectors.joining(", ")));
    }
}
