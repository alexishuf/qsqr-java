package br.ufsc.lapesd.qsq.rules;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface Program {
    @Nonnull Rule getRule(int i);
    @Nullable Rule getRule(@Nonnull String name);
    @Nonnull Iterable<Rule> getRules();
    int getRuleCount();

    @Nullable String getName();

    /**
     * An intensional predicate is a predicate that is the head of at least one rule.
     * @param predicate the predicate name
     * @return true iff the predicate is intensional
     */
    boolean isIntensional(@Nonnull String predicate);

    /**
     * The opposite of an intensional predicate, i.e., it does not appear as a rule head.
     * @param predicate the predicate name
     * @return true iff the predicate is extensional.
     */
    default boolean isExtensional(@Nonnull String predicate) { return !isIntensional(predicate); }

    /**
     * Streams the indices of rules with the given predicate on the head.
     */
    @Nonnull Stream<Integer> streamIndices(@Nonnull String predicate);

    /**
     * Streams the rules which have the given predicate on their head.
     */
    default @Nonnull Stream<Rule> streamRules(@Nonnull String predicate) {
        return streamIndices(predicate).map(this::getRule);
    }

    default @Nonnull Stream<Rule> streamRules() {
        return StreamSupport.stream(getRules().spliterator(), false);
    }
}
