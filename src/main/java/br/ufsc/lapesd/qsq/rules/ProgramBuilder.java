package br.ufsc.lapesd.qsq.rules;

import br.ufsc.lapesd.qsq.rules.parser.DatalogParser;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import br.ufsc.lapesd.qsq.rules.plain.PlainRule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class ProgramBuilder {
    protected final @Nonnull List<Rule> rules = new ArrayList<>();
    protected @Nullable String name;

    public ProgramBuilder() {
        this(null);
    }
    public ProgramBuilder(@Nullable String name) {
        this.name = name;
    }

    public static class SyntaxException extends RuntimeException {
        public SyntaxException(DatalogParser.SyntaxException cause) {
            super(cause);
        }
        public SyntaxException(@Nonnull String message) {
            super(message);
        }
    }

    public static @Nonnull ProgramBuilder start(@Nullable String name) {
        return new ProgramBuilder(name);
    }
    public static @Nonnull ProgramBuilder start() {
        return start(null);
    }

    public RuleHeadBuilder beginRule(@Nullable String name) {
        return new RuleHeadBuilder(name);
    }
    public RuleHeadBuilder beginRule() { return beginRule(null); }

    public @Nonnull Program build() {
        return new ListProgram(rules, name);
    }

    private static List<Term> parseTerms(@Nonnull Object... terms) {
        List<Term> list = new ArrayList<>(terms.length);
        for (Object term : terms) {
            if (term instanceof Term)
                list.add((Term) term);
            else if (term instanceof String && ((String) term).startsWith("?"))
                list.add(Term.createVariable(term.toString().substring(1)));
            else
                list.add(Term.createConstant(term));
        }
        return list;
    }

    public class RuleHeadBuilder {
        private @Nullable String name;

        private RuleHeadBuilder(@Nullable String name) {
            this.name = name;
        }

        public @Nonnull RuleBuilder head(@Nonnull String atom) {
            return new RuleBuilder(name, parseAtom(atom));
        }
        public @Nonnull RuleBuilder head(@Nonnull String predicate, @Nonnull Object... terms) {
            return new RuleBuilder(name, new PlainAtom(predicate, parseTerms(terms)));
        }
    }

    public class RuleBuilder {
        private final @Nullable String name;
        private final @Nonnull
        PlainAtom head;
        private final @Nonnull List<Atom> body = new ArrayList<>();

        private RuleBuilder(@Nullable String name, @Nonnull PlainAtom head) {
            this.name = name;
            this.head = head;
        }

        public @Nonnull RuleBuilder add(@Nonnull String atomString) {
            body.add(parseAtom(atomString));
            return this;
        }
        public @Nonnull RuleBuilder add(@Nonnull String predicate, @Nonnull Object... terms) {
            body.add(new PlainAtom(predicate, parseTerms(terms)));
            return this;
        }

        public @Nonnull ProgramBuilder endRule() {
            ProgramBuilder.this.rules.add(new PlainRule(name, head, body));
            return ProgramBuilder.this;
        }
    }

    @Nonnull
    private static PlainAtom parseAtom(@Nonnull String atomString) {
        PlainAtom atom;
        try {
            atom = new DatalogParser().parseAtom(atomString);
        } catch (DatalogParser.SyntaxException e) {
            throw new SyntaxException(e);
        }
        if (atom == null)
            throw new SyntaxException(String.format("Expected %s, got %s", "Atom", atomString));
        return atom;
    }
}
