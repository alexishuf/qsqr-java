package br.ufsc.lapesd.qsq.rules;

import com.google.common.collect.Iterators;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class CompositeProgram implements Program {
    private @Nonnull List<Program> list;
    private int totalSize;
    private @Nullable String name;

    private static @Nullable String generateName(@Nonnull Collection<Program> programs) {
        if (programs.stream().map(Program::getName).allMatch(Objects::isNull))
            return null;
        return String.format("[%s]", programs.stream().map(p -> format("[%s]", p.getName()))
                .collect(joining("+")));
    }

    public CompositeProgram(@Nonnull Collection<Program> collection) {
        this(collection, generateName(collection));
    }

    public CompositeProgram(@Nonnull Collection<Program> collection, @Nullable String name) {
        this.list = new ArrayList<>(collection);
        totalSize = this.list.stream().map(Program::getRuleCount).reduce(Integer::sum).orElse(0);
        this.name = name;
    }
    public CompositeProgram(@Nonnull Program... programs) {
        this(Arrays.asList(programs));
    }
    public CompositeProgram(@Nullable String name, @Nonnull Program... programs) {
        this(Arrays.asList(programs), name);
    }

    @Override
    public @Nonnull Rule getRule(int i) {
        if (i < 0)
            throw new IndexOutOfBoundsException("Index "+i+" is out of bounds!");
        int base = 0;
        for (Program p : list) {
            int count = p.getRuleCount();
            if (base+count > i)
                return p.getRule(i-base);
            base += count;
        }
        throw new IndexOutOfBoundsException(format("Index %d > %d is out of bounds", i, totalSize));
    }

    @Override
    public @Nullable Rule getRule(@Nonnull String name) {
        for (Program program : list) {
            Rule rule = program.getRule(name);
            if (rule != null) return rule;
        }
        return null;
    }

    @Override
    public @Nullable String getName() {
        return name;
    }
    @Override
    public int getRuleCount() {
        return totalSize;
    }

    @Override
    public boolean isIntensional(@Nonnull String predicate) {
        return list.stream().anyMatch(p -> p.isIntensional(predicate));
    }

    @Override
    public @Nonnull Iterable<Rule> getRules() {
        return () -> Iterators.concat(list.stream().map(p -> p.getRules().iterator()).iterator());
    }

    @Nonnull
    @Override
    public Stream<Integer> streamIndices(@Nonnull String predicate) {
        int base = 0;
        Stream<Integer> stream = Stream.empty();
        for (Program program : list) {
            if (program.isIntensional(predicate)) {
                int finalBase = base;
                stream = Stream.concat(stream,
                        program.streamIndices(predicate).map(i -> i + finalBase));
            }
            base += program.getRuleCount();
        }
        return stream;
    }

    @Override
    public @Nonnull Stream<Rule> streamRules() {
        Stream<Rule> s = list.get(0).streamRules();
        for (int i = 1; i < list.size(); i++) s = Stream.concat(s, list.get(i).streamRules());
        return s;
    }
}
