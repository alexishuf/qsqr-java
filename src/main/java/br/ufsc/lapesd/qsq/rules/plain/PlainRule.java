package br.ufsc.lapesd.qsq.rules.plain;

import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Rule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public class PlainRule implements Rule {
    private @Nullable String name;
    private @Nonnull PlainAtom head;
    private @Nonnull List<Atom> body;

    public PlainRule(@Nullable String name, @Nonnull PlainAtom head, @Nonnull List<Atom> body) {
        this.name = name;
        this.head = head;
        this.body = Collections.unmodifiableList(body);
    }
    public PlainRule(@Nonnull PlainAtom head, @Nonnull List<Atom> body) {
        this(null, head, body);
    }
    public PlainRule(@Nullable String name, @Nonnull PlainAtom head, @Nonnull Atom... body) {
        this(name, head, Arrays.asList(body));
    }
    public PlainRule(@Nonnull PlainAtom head, @Nonnull Atom... body) {
        this(null, head, body);
    }

    @Override
    public @Nullable String getName() {
        return name;
    }

    @Override
    public @Nonnull
    PlainAtom getHead() {
        return head;
    }

    @Override
    public @Nonnull String getPredicate() {
        return head.getPredicate();
    }

    @Override
    public @Nonnull List<Atom> getBody() {
        return body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rule rule = (Rule) o;
        return Objects.equals(getName(), rule.getName()) &&
                getHead().equals(rule.getHead()) &&
                getBody().equals(rule.getBody());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getHead(), getBody());
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ", " <- ", "");
        body.stream().map(Object::toString).forEach(joiner::add);
        String namePrefix = getName() != null ? String.format("[%s] ", getName()) : "";
        return namePrefix + head + joiner.toString();
    }
}
