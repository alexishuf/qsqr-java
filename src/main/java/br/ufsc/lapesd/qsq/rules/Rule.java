package br.ufsc.lapesd.qsq.rules;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface Rule {
    @Nullable
    String getName();

    @Nonnull
    Atom getHead();

    @Nonnull
    String getPredicate();

    @Nonnull
    List<Atom> getBody();
}
