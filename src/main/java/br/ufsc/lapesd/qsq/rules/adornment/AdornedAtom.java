package br.ufsc.lapesd.qsq.rules.adornment;

import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Term;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.BOUND;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.FREE;

public class AdornedAtom implements Atom {
    private @Nonnull Atom atom;
    private @Nonnull Adornment adornment;

    public AdornedAtom(@Nonnull Atom atom, @Nonnull Adornment adornment) {
        this.atom = atom;
        this.adornment = adornment;
        String violating = getFree().stream().filter(t -> !t.isVariable())
                .map(Term::toString).collect(Collectors.joining(", "));
        Preconditions.checkArgument(violating.isEmpty(),
                "There are constants adorned as FREE: " + violating);
    }

    public static AdornedAtom adorn(@Nonnull Atom atom) {
        StringBuilder builder = new StringBuilder(atom.getTerms().size());
        for (Term term : atom.getTerms())
            builder.append(term.isVariable() ? FREE.character() : BOUND.character());
        return adorn(atom, builder.toString());
    }

    public static AdornedAtom adorn(@Nonnull Atom atom, @Nonnull Adornment adornment) {
        return new AdornedAtom(atom, adornment);
    }
    public static AdornedAtom adorn(@Nonnull Atom atom, @Nonnull String adornment) {
        return new AdornedAtom(atom, new Adornment(adornment));
    }

    @Nonnull
    public Atom getAtom() {
        return atom;
    }

    public @Nonnull List<Term> getBound() { return getAdorned(BOUND); }
    public @Nonnull List<Term> getFree() { return getAdorned(FREE); }
    public @Nonnull List<Term> getAdorned(@Nonnull Adornment.Term term) {
        int[] positions = adornment.getTermPositions(term);
        List<Term> list = new ArrayList<>(positions.length);
        List<Term> terms = getTerms();
        for (int position : positions) list.add(terms.get(position));
        return list;
    }

    /**
     * A ground adornment is a fully bound adornment.
     */
    public boolean isGround() {
        return adornment.getFree().length == 0;
    }

    /**
     * An unbound adornment is one without any bound values. Such adornments are dangerous, since
     * queries produced from them will cause an enumeration of all data.
     */
    public boolean isUnbound() {
        return adornment.getBound().length == 0;
    }

    @Override
    public @Nonnull String getPredicate() {
        return atom.getPredicate();
    }

    public @Nonnull Adornment getAdornment() {
        return adornment;
    }

    @Override
    public @Nonnull List<Term> getTerms() {
        return atom.getTerms();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdornedAtom that = (AdornedAtom) o;
        return atom.equals(that.atom) &&
                getAdornment().equals(that.getAdornment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(atom, getAdornment());
    }

    @Override
    public String toString() {
        return String.format("%s^%s(%s)", getPredicate(), getAdornment(), getTerms().stream()
                .map(Term::toString).collect(Collectors.joining(", ")));
    }
}
