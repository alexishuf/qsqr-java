package br.ufsc.lapesd.qsq.rules.classifiers;

import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.Rule;

import javax.annotation.Nonnull;
import java.util.List;

public interface TBoxClassifier {
    @Nonnull List<Rule> getTBoxRules(@Nonnull Program program);
}
