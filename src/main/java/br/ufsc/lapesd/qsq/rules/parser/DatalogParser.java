package br.ufsc.lapesd.qsq.rules.parser;

import br.ufsc.lapesd.qsq.rules.*;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import br.ufsc.lapesd.qsq.rules.plain.PlainRule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

public class DatalogParser {
    enum Token {
        COMMENT("(?:#|//)(.*)(\r\n|\n|$)"),
        COMMA("^\\s*,"),
        RULE_NAME("^\\s*(?:\\[([^]]*)\\]|([^:]*):)"),
        //                        ^grp1 ^    ^ grp2^
        L_PAR("^\\s*\\("),
        R_PAR("^\\s*\\)"),
        LEFT_ARROW("^\\s*<-"),
        VAR("^\\s*\\?(\\w+)"),
        CONSTANT("^\\s*(?:" +
                "([A-Za-z0-9_\\-]+)|"                    + // group 1
                "\"((?:[A-Za-z0-9, .'_\\-]|\\\\\")+)\"|" + // group 2
                "'((?:[A-Za-z0-9, .\"_\\-]|\\\\')+)'"    + // group 3
                ")"),
        PREDICATE("^\\s*([A-Za-z0-9_\\-]+)"),
        ATOM_LIST_END("^\\s*(<-|\\.\\s*$|\r\n|$)");

        Token(@Nonnull String rx) {
            this.rx = Pattern.compile(rx);
        }
        public final @Nonnull Pattern rx;
    }

    private @Nonnull ConstantParser constantParser = new PlainConstantParser();


    public static class SyntaxException extends Exception {
        public SyntaxException(String message) {
            super(message);
        }
    }

    public void setConstantParser(@Nonnull ConstantParser constantParser) {
        this.constantParser = constantParser;
    }

    @Nonnull
    public ConstantParser getConstantParser() {
        return constantParser;
    }

    /**
     * Attempts to parse a Rule from a line.
     *
     * Only one rule per line is allowed. If the line contains only a comment, then this
     * method will return null. If there are syntax errors (including multiple rules in a line)
     * An {@link IllegalArgumentException} will be thrown.
     *
     * @param string rule string
     * @return Rule or null if the whole line is a comment.
     */
    public @Nullable Rule 
    parseRule(@Nonnull String string) throws SyntaxException {
        return parseRule(string, 0);
    }

    /**
     * Attempts to parse a Rule from a line.
     *
     * Only one rule per line is allowed. If the line contains only a comment, then this
     * method will return null. If there are syntax errors (including multiple rules in a line)
     * An {@link IllegalArgumentException} will be thrown.
     *
     * @param string rule string
     * @param line line number within a file
     * @return Rule or null if the whole line is a comment.
     */
    public @Nullable Rule 
    parseRule(@Nonnull String string, int line) throws SyntaxException {
        string = Token.COMMENT.rx.matcher(string).replaceAll("");
        StringState state = new StringState(string, line);
        String ruleName = parseRuleName(state);
        PlainAtom head = parseAtom(state, false);
        if (head == null) {
            if (ruleName != null)
                throw state.createExpectedException("head atom");
            return null;
        }

        state.consume(Token.LEFT_ARROW, false);

        List<Atom> body = new ArrayList<>();
        for (Atom atom = parseAtom(state); atom != null; atom = parseAtom(state))
            body.add(atom);
        return new PlainRule(ruleName, head, body);
    }

    public @Nonnull Program
    parseProgram(@Nonnull List<String> lines) throws SyntaxException {
        List<Rule> rules = new ArrayList<>();
        int number = 1;
        for (String line : lines) {
            if (!constantParser.tryPrefix(line))
                rules.add(parseRule(line, number++));
        }
        return new ListProgram(rules);
    }

    public @Nonnull Program parseProgram(@Nonnull String program) throws SyntaxException {
        return parseProgram(Arrays.stream(program.split("\r?\n")).collect(toList()));
    }

    public @Nonnull Program parseProgram(@Nonnull InputStream stream)
            throws SyntaxException, IOException {
        return parseProgram(stream, StandardCharsets.UTF_8);
    }

    public @Nonnull Program parseProgram(@Nonnull InputStream stream, Charset charset)
            throws SyntaxException, IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset))) {
            return parseProgram(reader);
        }
    }

    public Program parseProgram(BufferedReader reader) throws IOException, SyntaxException {
        List<Rule> rules = new ArrayList<>();
        int number = 0;
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            ++number;
            if (constantParser.tryPrefix(line))
                continue;
            Rule rule = parseRule(line, number);
            if (rule != null)
                rules.add(rule);
        }
        return new ListProgram(rules);
    }

    private @Nullable String parseRuleName(@Nonnull StringState state) {
        Matcher matcher = Token.RULE_NAME.rx.matcher(state.string);
        String ruleName = null;
        if (matcher.find()) {
            ruleName = matcher.group(1);
            if (ruleName == null)
                ruleName = matcher.group(2);
            state.string = matcher.replaceAll("");
        }
        return ruleName;
    }

    public @Nullable
    PlainAtom parseAtom(@Nonnull String atom) throws SyntaxException {
        StringState state = new StringState(atom, 0);
        return parseAtom(state);
    }

    public @Nonnull List<Atom> parseAtoms(@Nonnull String csAtoms) throws SyntaxException {
        StringState state = new StringState(csAtoms, 0);
        List<Atom> list = new ArrayList<>();
        for (Atom atom = parseAtom(state); atom != null; atom = parseAtom(state))
            list.add(atom);
        return list;
    }

    private @Nullable
    PlainAtom parseAtom(@Nonnull StringState state) throws SyntaxException {
        return parseAtom(state, true);
    }

    private @Nullable
    PlainAtom parseAtom(@Nonnull StringState state,
                        boolean allowList) throws SyntaxException {
        if (state.lookahead(Token.ATOM_LIST_END) != null)
            return null; //nothing more to parse!

        // predicate
        Matcher matcher = Token.PREDICATE.rx.matcher(state.string);
        if (!matcher.find())
            throw state.createExpectedException(Token.PREDICATE);
        String predicate = matcher.group(1);
        state.string = matcher.replaceFirst("");

        state.consume(Token.L_PAR, false);
        // terms
        List<Term> terms = new ArrayList<>();
        for (Term term = parseTerm(state); term != null; term = parseTerm(state))
            terms.add(term);
        if (terms.isEmpty())
            throw state.createExpectedException("term (var or constant)");
        state.consume(Token.R_PAR, false);
        
        if (allowList) {
            if (state.consume(Token.COMMA, true))
                state.requireLookahead(Token.PREDICATE);
            else
                state.requireLookahead(Token.ATOM_LIST_END);
        } else {
            state.requireLookahead(Token.ATOM_LIST_END);
        }

        return new PlainAtom(predicate, terms);
    }

    private @Nullable Term parseTerm(@Nonnull StringState state) throws SyntaxException {
        if (Token.R_PAR.rx.matcher(state.string).find())
            return null; //no more terms

        Term term;
        Matcher matcher = Token.VAR.rx.matcher(state.string);
        if (!matcher.find()) {
            ConstantParser.Parse parse = constantParser.parse(state.string);
            if (parse == null)
                throw state.createExpectedException("term (var or constant)");
            term = Term.createConstant(parse.getValue());
            state.string = state.string.substring(parse.endPos);
        } else {
            term = Term.createVariable(matcher.group(1));
            state.string = matcher.replaceFirst("");
        }

        if (state.consume(Token.COMMA, true))
            state.requireLookahead(Token.VAR, Token.CONSTANT);

        return term;
    }

    private class StringState {
        private @Nonnull String string;
        private int line;

        public StringState(@Nonnull String string, int line) {
            this.string = string;
            this.line = line;
        }

        public Matcher requireLookahead(@Nonnull Token... orList) throws SyntaxException {
            Matcher matcher = lookahead(orList);
            if (matcher != null) {
                return matcher;
            } else {
                throw createExpectedException(Arrays.stream(orList).map(Token::name)
                        .collect(Collectors.joining(", ")));
            }
        }
        public Matcher lookahead(@Nonnull Token... orList) {
            for (Token token : orList) {
                Matcher matcher = token.rx.matcher(string);
                if (matcher.find()) return matcher;
            }
            return null;
        }

        public boolean consume(@Nonnull Token token, boolean optional) throws SyntaxException {
            Matcher matcher = optional ? lookahead(token) : requireLookahead(token);
            if (matcher != null)
                string = matcher.replaceFirst("");
            return matcher != null;
        }

        public @Nonnull SyntaxException createExpectedException(@Nonnull Token token) {
            return createExpectedException(token.name());
        }
        public @Nonnull SyntaxException createExpectedException(@Nonnull String expected) {
            String msg = format("Datalog syntax error at line %d. Expected %s, found: %s",
                    line, expected, string);
            return new SyntaxException(msg);
        }
    }

}
