package br.ufsc.lapesd.qsq.rules;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.Objects;

import static java.lang.String.format;

public class Term {
    private boolean variable;
    private @Nonnull Object value;

    protected Term(boolean variable, @Nonnull Object value) {
        this.variable = variable;
        this.value = value;
    }

    public static @Nonnull Term createVariable(@Nonnull String name) {
        return new Term(true, name);
    }

    public static @Nonnull Term createConstant(@Nonnull Object value) {
        return new Term(false, value);
    }

    public boolean isVariable() {
        return variable;
    }

    public @Nonnull String getName() {
        Preconditions.checkArgument(isVariable(), "Only variables have names!");
        return (String) value;
    }

    public @Nonnull Object getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Term term = (Term) o;
        return isVariable() == term.isVariable() &&
                getValue().equals(term.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isVariable(), getValue());
    }

    @Override
    public String toString() {
        String str = value.toString();
        boolean hasDoubleQuote = str.contains("\"");
        boolean hasSpace = str.contains(" ");
        boolean hasSingleQuote = str.contains("'");
        if (hasDoubleQuote || hasSingleQuote || hasSpace) {
            if (hasDoubleQuote && !hasSingleQuote) {
                str = format("'%s'", str);
            } else if (hasSingleQuote && !hasDoubleQuote) {
                str = format("\"%s\"", str);
            } else { //hasDoubleQuote && hasSingleQuote;
                if (hasDoubleQuote)
                    str = format("\"%s\"", str.replaceAll("(?<!\\\\)\"", "\\\""));
            }
        }
        return (isVariable() ? "?" : "") + str;
    }
}
