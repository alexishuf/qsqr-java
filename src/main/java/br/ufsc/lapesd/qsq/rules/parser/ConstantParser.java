package br.ufsc.lapesd.qsq.rules.parser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

public interface ConstantParser {
    class Parse {
        final @Nonnull Object value;
        final int endPos;

        public Parse(@Nonnull Object value, int endPos) {
            this.value = value;
            this.endPos = endPos;
        }

        @Nonnull
        public Object getValue() {
            return value;
        }

        public int getEndPos() {
            return endPos;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Parse parse = (Parse) o;
            return getEndPos() == parse.getEndPos() &&
                    getValue().equals(parse.getValue());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getValue(), getEndPos());
        }

        @Override
        public String toString() {
            return String.format("%d:%s", getEndPos(), getValue());
        }
    }

    @Nullable Parse parse(@Nonnull String text);
    boolean tryPrefix(@Nonnull String line);
}
