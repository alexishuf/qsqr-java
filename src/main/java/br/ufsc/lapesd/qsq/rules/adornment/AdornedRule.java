package br.ufsc.lapesd.qsq.rules.adornment;

import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

import static br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom.adorn;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.BOUND;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.FREE;
import static java.lang.String.format;

public class AdornedRule implements Rule {
    private @Nonnull Rule rule;
    private @Nonnull Adornment adornment;
    private @Nonnull AdornedAtom head;
    private @Nonnull List<Atom> adornedBody;

    public AdornedRule(@Nonnull Rule rule, @Nonnull Adornment headAdornment) {
        this.rule = rule;
        this.adornment = headAdornment;
        head = adorn(rule.getHead(), headAdornment);

        List<Term> free = head.getBound();
        Set<Term> freeSet = new HashSet<>(free.size()+1, 1);
        freeSet.addAll(free);

        adornedBody = new ArrayList<>(rule.getBody().size());
        for (Atom atom : rule.getBody()) {
            StringBuilder builder = new StringBuilder();
            for (Term term : atom.getTerms()) {
                if (!term.isVariable()) builder.append(BOUND.character());
                else if (freeSet.contains(term)) builder.append(BOUND.character());
                else builder.append(FREE.character());
            }
            adornedBody.add(adorn(atom, builder.toString()));
        }
    }
    public AdornedRule(@Nonnull Rule rule, @Nonnull String adornment) {
        this(rule, new Adornment(adornment));
    }

    public AdornedRule(@Nonnull Rule rule, @Nonnull AdornedAtom head,
                       @Nonnull Collection<AdornedAtom> body) {
        Preconditions.checkArgument(head.getAtom().equals(rule.getHead()));
        Preconditions.checkArgument(body.size() == rule.getBody().size());
        this.rule = rule;
        this.adornment = head.getAdornment();
        this.head = head;
        adornedBody = new ArrayList<>(body.size());
        adornedBody.addAll(body);

        if (AdornedRule.class.desiredAssertionStatus()) {
            Set<Atom> extra = body.stream().map(AdornedAtom::getAtom).collect(Collectors.toSet());
            Set<Atom> missing = new HashSet<>(rule.getBody());
            missing.removeAll(extra);
            extra.removeAll(rule.getBody());
            assert extra.isEmpty() && missing.isEmpty() :
                    format("extraneous rules: %s, missing rules: %s", extra, missing);
        }
    }

    /**
     * @return the original rule which this instance adorns.
     */
    public @Nonnull Rule getPlainRule() {
        return rule;
    }

    /**
     * The {@link Adornment} that is applied to the rule head (and consequently to the rule itself).
     */
    public @Nonnull Adornment getAdornment() {
        return adornment;
    }

    /**
     * Gets the free terms of the head atom.
     */
    public @Nonnull List<Term> getFree() { return head.getFree(); }

    /**
     * Gets the bound terms of the head atom.
     */
    public @Nonnull List<Term> getBound() { return head.getBound(); }

    @Override
    @Nonnull
    public String getPredicate() {
        return rule.getPredicate();
    }

    @Override
    @Nullable
    public String getName() {
        return rule.getName();
    }

    /**
     * Gets the rule head, adorned.
     *
     * @return A {@link AdornedAtom} wrapping the plain rule head.
     */
    @Override
    @Nonnull
    public AdornedAtom getHead() {
        return head;
    }

    /**
     * Gets the body atoms, adorned.
     *
     * @return A instance of {@link AdornedAtom} for each {@link Atom} in the plain {@link Rule}.
     */
    @Override
    @Nonnull
    public List<Atom> getBody() {
        return adornedBody;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdornedRule that = (AdornedRule) o;
        return getPlainRule().equals(that.getPlainRule()) &&
                getAdornment().equals(that.getAdornment()) &&
                getHead().equals(that.getHead()) &&
                adornedBody.equals(that.adornedBody);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlainRule(), getAdornment(), getHead(), adornedBody);
    }

    @Override
    public String toString() {
        String name = "";
        if (getPlainRule().getName() != null)
            name = format("[%s^%s] ", getPlainRule().getName(), getAdornment());
        return format("%s%s <- %s.", name, head, adornedBody.stream().map(Objects::toString)
                .collect(Collectors.joining(", ")));
    }
}
