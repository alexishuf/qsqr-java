package br.ufsc.lapesd.qsq.rules;

import javax.annotation.Nonnull;
import java.util.List;

public interface Atom {
    @Nonnull
    String getPredicate();

    /**
     * @return immutable list of terms
     */
    @Nonnull
    List<Term> getTerms();

    default boolean hasLiterals() {
        return getTerms().stream().anyMatch(t -> !t.isVariable());
    }
}
