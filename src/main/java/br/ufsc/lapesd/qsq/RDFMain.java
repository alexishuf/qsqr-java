package br.ufsc.lapesd.qsq;

import br.ufsc.lapesd.qsq.rdf.JenaConstantParser;
import br.ufsc.lapesd.qsq.rdf.ModelInstanceData;
import br.ufsc.lapesd.qsq.rdf.RDFNodeGsonSerializer;
import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import br.ufsc.lapesd.qsq.relations.InstanceData;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.RelationUtils;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowGsonSerializer;
import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.parser.DatalogParser;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.List;

@SuppressWarnings("MismatchedReadAndWriteOfArray")
public class RDFMain {
    public enum ProgramName {
        RDFS,
        RDFS_COMPLETE;

        public @Nonnull Program getProgram() {
            switch (this) {
                case RDFS: return RDFPrograms.RDFS_T;
                case RDFS_COMPLETE: return RDFPrograms.RDFS_FULL;
            }
            throw new UnsupportedOperationException("Cannot handle " + this);
        }

        public static @Nonnull Program getProgram(String programName) {
            String canon = programName.replace('-', '_').toUpperCase();
            return valueOf(canon).getProgram();
        }
    }

    @Option(name = "--help", aliases = {"-h"}, usage = "Shows help", help = true)
    private boolean help;

    @Option(name = "--program", aliases = {"-p"}, usage = "Datalog program name")
    private String programName = ProgramName.RDFS.name();
    private Program program = null;

    @Option(name = "--query-log", usage = "Writes a list of JSON objects for each query " +
            "issued by QSQ to the given file. The file will be created and truncated.")
    private File queryLogFile = null;

    @Option(name = "--out-csv", aliases = {"-o"}, usage = "Saves the query results, as " +
            "CSV into the given file which will be created and truncated")
    private File outFile = null;

    @Option(name = "--query", aliases = {"-q"}, required = true, metaVar = "QUERY",
            usage = "Process the given conjunctive query")
    private String queryString;
    private List<Atom> query = null;

    @Argument
    private File[] inputFiles = {};

    public static void main(String[] args) throws Exception {
        RDFMain app = new RDFMain();
        CmdLineParser parser = new CmdLineParser(app);
        try {
            parser.parseArgument(args);
            app.processArguments();
        } catch (CmdLineException | IllegalArgumentException e) {
            System.err.printf("Bad command line options: %s\n", e.getLocalizedMessage());
            parser.printUsage(System.err);
            return;
        }
        app.run();
    }

    private void processArguments() {
        Preconditions.checkArgument(inputFiles.length == 1,
                "Currently, exactly one RDF input file must be given");
        DatalogParser parser = new DatalogParser();
        parser.setConstantParser(new JenaConstantParser());
        try {
            query = parser.parseAtoms(queryString);
        } catch (DatalogParser.SyntaxException e) {
            throw new IllegalArgumentException("Bad query", e);
        }
        program = ProgramName.getProgram(programName);
    }

    private void run() throws IOException {
        QSQR qsqr = new QSQR(program, getInstanceData(), query);
        qsqr.setLogEnabled(queryLogFile != null);
        Relation results = qsqr.run();
        if (outFile != null) {
            try (FileOutputStream os = new FileOutputStream(queryLogFile)) {
                RelationUtils.toCSV(results, os);
            }
        } else {
            RelationUtils.toCSV(results, System.out);
        }

        if (queryLogFile != null) {
            try (FileWriter writer = new FileWriter(queryLogFile)) {
                Gson gson = new GsonBuilder().setPrettyPrinting()
                        .registerTypeHierarchyAdapter(Row.class, new RowGsonSerializer())
                        .registerTypeHierarchyAdapter(RDFNode.class, new RDFNodeGsonSerializer())
                        .create();
                writer.write(gson.toJson(qsqr.getLogEntries()));
            }
        }
    }

    private @Nonnull InstanceData getInstanceData() throws IOException {
        Model model = ModelFactory.createDefaultModel();
        for (File file : inputFiles) {
            try (FileInputStream is = new FileInputStream(file)) {
                Lang lang = RDFLanguages.filenameToLang(file.getName());
                RDFDataMgr.read(model, is, lang);
            }
        }
        return new ModelInstanceData("T", model);
    }
}
