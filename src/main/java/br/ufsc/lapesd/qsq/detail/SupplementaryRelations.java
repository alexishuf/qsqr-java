package br.ufsc.lapesd.qsq.detail;

import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedRule;
import br.ufsc.lapesd.qsq.rules.adornment.Adornment;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.BOUND;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.FREE;

public class SupplementaryRelations {
    private final @Nonnull Rule rule;
    private final @Nonnull Adornment headAdornment;
    private final @Nonnull List<String> varNames;
    private final @Nonnull List<Integer> ruleOrder;
    private final @Nonnull List<Integer> lastUse;
    private final @Nonnull List<BitSet> adornmentBound;
    private final @Nonnull List<BitSet> suppColumns;
    private final @Nonnull List<Integer> bodyReordering;

    public SupplementaryRelations(@Nonnull Rule rule,
                                  @Nonnull Adornment headAdornment) {
        this.rule = rule;
        this.headAdornment = headAdornment;
        varNames = Stream.concat(rule.getHead().getTerms().stream(),
                rule.getBody().stream().flatMap(a -> a.getTerms().stream()))
                .filter(Term::isVariable).map(Term::getName)
                .sorted().distinct().collect(Collectors.toList());
        // lists all idxs into varNames in the order the variable first appears in the rule defn.
        ruleOrder = Stream.concat(rule.getHead().getTerms().stream(),
                rule.getBody().stream().flatMap(a -> a.getTerms().stream())
        ).filter(Term::isVariable).distinct().map(this::getVarIndex).collect(Collectors.toList());
        lastUse = new ArrayList<>(varNames.size());
        for (int i = 0; i < varNames.size(); i++) lastUse.add(0);

        int bodyAtoms = rule.getBody().size();
        adornmentBound = new ArrayList<>(bodyAtoms);
        suppColumns = new ArrayList<>(bodyAtoms);
        bodyReordering = new ArrayList<>(bodyAtoms);

        BitSet bounded = boundedVarsToBitSet(rule, headAdornment);
        for (int i = 0; i < bodyAtoms; i++) {
            int bestIdx = findBest(bounded, rule.getBody());
            assert !bodyReordering.contains(bestIdx);
            bodyReordering.add(bestIdx);
            BitSet vars = varsToBitSet(rule.getBody().get(bestIdx).getTerms());
            BitSet boundVars = (BitSet) vars.clone();
            boundVars.and(bounded); //restrict to those already bound
            updateLastUse(boundVars, i); //mark last usage point
            adornmentBound.add(boundVars); // b's in the adornment
            suppColumns.add((BitSet) bounded.clone());  // supp headers
            bounded.or(vars); // all free vars are now considered bounded
        }

        // Last supplementary relation has all head vars
        BitSet headVars = varsToBitSet(rule.getHead().getTerms());
        assertBounded(bounded, headVars);
        suppColumns.add(headVars);
        updateLastUse(headVars, bodyAtoms+1);

        /* at this point suppColumns stores all x that were bound. Remove the unnecessary */
        for (int i = 1; i < bodyAtoms+1; i++) {
            BitSet vars = suppColumns.get(i);
            for (int j = vars.nextSetBit(0); j >= 0; j = vars.nextSetBit(j +1)) {
                if (lastUse.get(j) < i) vars.clear(j);
            }
        }
    }

    private void assertBounded(@Nonnull BitSet bounded, @Nonnull BitSet vars) {
        if (!SupplementaryRelations.class.desiredAssertionStatus()) return;
        BitSet missing = (BitSet) vars.clone();
        missing.andNot(bounded);
        assert missing.cardinality() == 0 : "Missing vars indices at supp_n: " + missing;
    }

    /**
     * Get a version of the {@link Rule} given to the constructor, addorned according to
     * the supplementary relations.
     */
    public @Nonnull AdornedRule createAdornedRule() {
        List<AdornedAtom> body = new ArrayList<>(bodyReordering.size());
        int adornedIdx = 0;
        for (Integer origIdx : bodyReordering) {
            Atom atom = rule.getBody().get(origIdx);
            BitSet bound = adornmentBound.get(adornedIdx++);
            StringBuilder adornmentStringBuilder = new StringBuilder();
            for (Term term : atom.getTerms()) {
                if (!term.isVariable() || bound.get(getVarIndex(term)))
                    adornmentStringBuilder.append(BOUND.character());
                else
                    adornmentStringBuilder.append(FREE.character());
            }
            AdornedAtom adornedAtom = AdornedAtom.adorn(atom, adornmentStringBuilder.toString());
            body.add(adornedAtom);
        }
        return new AdornedRule(rule, new AdornedAtom(rule.getHead(), headAdornment), body);
    }

    /**
     * The number of supplementary relations: the number of body atoms, plus 1
     */
    public int getSupplementaryCount() {
        return suppColumns.size();
    }

    /**
     * Get the columns of the i-th supplementary relation
     */
    public @Nonnull List<String> getColumnsOfSupplementary(int i) {
        Preconditions.checkArgument(i >= 0);
        if (i >= suppColumns.size()) throw new IndexOutOfBoundsException(i);
        BitSet bs = suppColumns.get(i);
        List<String> list = new ArrayList<>(bs.cardinality());
        for (Integer idx : ruleOrder) { //ensure order used in the rule definition
            if (bs.get(idx)) list.add(varNames.get(idx));
        }
//        for (int j = bs.nextSetBit(0); j >= 0; j = bs.nextSetBit(j+1))
//            list.add(varNames.get(j));
        return list;
    }


    private static class Cost implements Comparable<Cost> {
        private int free;
        private int bound;

        public static final @Nonnull Cost MIN_COST = new Cost(0, Integer.MAX_VALUE);
        public static final @Nonnull Cost MAX_COST = new Cost(Integer.MAX_VALUE, 0);

        public Cost(int free, int bound) {
            this.free = free;
            this.bound = bound;
        }

        @Override
        public int compareTo(@Nonnull Cost o) {
            int diff = Integer.compare(free, o.free);
            return diff == 0 ? Integer.compare(o.bound, bound) : diff;
        }
    }

    private Cost getCost(@Nonnull BitSet bounded, @Nonnull Atom atom) {
        BitSet vars = varsToBitSet(atom.getTerms());
        int total = vars.cardinality();
        vars.and(bounded);
        int bound = vars.cardinality();
        return new Cost(total - bound, bound);
    }

    private int findBest(@Nonnull BitSet bounded, @Nonnull List<Atom> body) {
        int idx = IntStream.range(0, body.size()).boxed()
                .filter(i -> !bodyReordering.contains(i))
                .min(Comparator.comparing(i -> getCost(bounded, body.get(i)))).orElse(-1);
        assert idx >= 0;
        return idx;
    }

    private void updateLastUse(@Nonnull BitSet vars, int atSuppIdx) {
        for (int i = vars.nextSetBit(0); i >= 0; i = vars.nextSetBit(i+1))
            lastUse.set(i, atSuppIdx);
    }

    private @Nonnull BitSet varsToBitSet(@Nonnull Collection<Term> terms) {
        BitSet bitSet = new BitSet(varNames.size());
        for (Term term : terms) {
            if (term.isVariable()) bitSet.set(getVarIndex(term));
        }
        return bitSet;
    }

    @Nonnull
    private BitSet boundedVarsToBitSet(@Nonnull Rule rule,
                                       @Nonnull Adornment headAdornment) {
        List<Term> headTerms = rule.getHead().getTerms();
        BitSet bs = new BitSet(varNames.size());
        for (int i : headAdornment.getBound()) {
            Term term = headTerms.get(i);
            if (term.isVariable()) bs.set(getVarIndex(term));
        }
        return bs;
    }

    private int getVarIndex(@Nonnull Term t) {
        Preconditions.checkArgument(t.isVariable(), "Only variables allowed");
        int idx = Collections.binarySearch(varNames, t.getName());
        assert idx >= 0;
        return idx;
    }
}
