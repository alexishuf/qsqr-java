package br.ufsc.lapesd.qsq.detail;

import br.ufsc.lapesd.qsq.relations.UniqueSequencedRelation;
import br.ufsc.lapesd.qsq.relations.impl.NaiveUniqueSequencedRelation;
import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import br.ufsc.lapesd.qsq.rules.adornment.Adornment;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QSQTemplateCache {
    private @Nonnull final Program program;
    private @Nonnull final HashMap<Key, QSQTemplate> cache;
    private @Nonnull UniqueSequencedRelation.Factory relationFactory;

    private class Key {
        public int idx;
        public @Nonnull Adornment adornment;

        public Key(int idx, @Nonnull Adornment adornment) {
            Preconditions.checkArgument(idx >= 0);
            this.idx = idx;
            Atom head = program.getRule(idx).getHead();
            int size = head.getTerms().size();
            StringBuilder builder = new StringBuilder(size);
            for (int i = 0; i < size; i++) {
                if (head.getTerms().get(i).isVariable() && adornment.isFree(i))
                    builder.append(Adornment.Term.FREE.character());
                else
                    builder.append(Adornment.Term.BOUND.character());
            }
            this.adornment = new Adornment(builder.toString());
        }

        public @Nonnull QSQTemplate compute() {
            return new QSQTemplate(program.getRule(idx), adornment, relationFactory);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return idx == key.idx &&
                    adornment.equals(key.adornment);
        }

        @Override
        public int hashCode() {
            return Objects.hash(idx, adornment);
        }

        @Override
        public String toString() {
            String name = program.getRule(idx).getName();
            if (name != null)
                name = String.format(" [%s]", name);
            else
                name = "";
            return String.format("QSQTemplateCache.Key(%d%s, %s)", idx, name, adornment);
        }
    }

    public QSQTemplateCache(@Nonnull Program program,
                            @Nonnull UniqueSequencedRelation.Factory relationFac) {
        this.program = program;
        this.relationFactory = relationFac;
        int ruleCount = program.getRuleCount();
        cache = new HashMap<>(ruleCount /2 + 1, 1);
    }

    public QSQTemplateCache(@Nonnull Program program) {
        this(program, NaiveUniqueSequencedRelation::new);
    }

    public @Nonnull UniqueSequencedRelation.Factory getRelationFactory() {
        return relationFactory;
    }

    public void setRelationFactory(@Nonnull UniqueSequencedRelation.Factory factory) {
        this.relationFactory = factory;
    }

    public @Nonnull Stream<QSQTemplate> stream(@Nonnull AdornedAtom forAtom) {
        Adornment adornment = forAtom.getAdornment();
        return program.streamIndices(forAtom.getPredicate())
                .map(i -> cache.computeIfAbsent(new Key(i, adornment), Key::compute));
    }

    public @Nonnull Stream<AdornedAtom> streamKeys() {
        return cache.keySet().stream()
                .map(k -> new AdornedAtom(program.getRule(k.idx).getHead(), k.adornment))
                .distinct();
    }

    public @Nonnull List<QSQTemplate> get(@Nonnull AdornedAtom forAtom) {
        return stream(forAtom).collect(Collectors.toList());
    }
}
