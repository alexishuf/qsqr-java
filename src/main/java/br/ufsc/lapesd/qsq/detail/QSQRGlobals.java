package br.ufsc.lapesd.qsq.detail;

import br.ufsc.lapesd.qsq.relations.Marker;
import br.ufsc.lapesd.qsq.relations.UniqueSequencedRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import br.ufsc.lapesd.qsq.rules.adornment.Adornment;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static br.ufsc.lapesd.qsq.detail.QSQRGlobals.Type.ANSWER;
import static br.ufsc.lapesd.qsq.rules.adornment.Adornment.Term.FREE;
import static java.lang.String.format;

public class QSQRGlobals {
    private @Nonnull Map<RelationName, Global> map;
    private @Nonnull UniqueSequencedRelation.Factory relationFactory;

    public enum Type {
        INPUT,
        ANSWER;

        @Nonnull
        String prefix() {
            switch (this) {
                case INPUT: return "in";
                case ANSWER: return "an";
            }
            throw new UnsupportedOperationException("Unexpected RelationType member: " + this);
        }
    }

    public static class RelationName {
        private @Nonnull Type type;
        private @Nonnull String predicate;
        private @Nonnull Adornment adornment;

        public RelationName(@Nonnull Type type, @Nonnull AdornedAtom adornedAtom) {
            this.type = type;
            this.predicate = adornedAtom.getPredicate();
            Adornment adornment = adornedAtom.getAdornment();
            this.adornment = type == ANSWER ? Adornment.all(FREE, adornment.size()) : adornment;
        }

        @Nonnull
        public List<String> getColumns() {
            List<String> list = new ArrayList<>();
            if (type == Type.INPUT) {
                for (int i : adornment.getBound())
                    list.add(Integer.toString(i+1));
            } else {
                for (int i = 0; i < this.adornment.toString().length(); i++)
                    list.add(Integer.toString(i+1));
            }
            return list;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RelationName that = (RelationName) o;
            return type == that.type &&
                    predicate.equals(that.predicate) &&
                    adornment.equals(that.adornment);
        }

        @Override
        public int hashCode() {
            return Objects.hash(type, predicate, adornment);
        }

        @Override
        public String toString() {
            return format("%s_%s^%s", type.prefix(), predicate, adornment);
        }
    }

    public static class Global {
        private @Nonnull Type type;
        private @Nonnull String predicate;
        private @Nonnull Adornment adornment;
        private @Nonnull UniqueSequencedRelation relation;
        private @Nonnull Marker marker;
        private @Nonnull List<String> orderedCols;
        private @Nonnull Map<String, Integer> orderedCols2Pos;

        public Global(@Nonnull Type type, @Nonnull String predicate, @Nonnull Adornment adornment,
                      @Nonnull UniqueSequencedRelation relation) {
            int adornmentColumns = adornment.toString().length();
            int columns = relation.getColumns().size();
            assert type == Type.INPUT || columns == adornmentColumns;
            assert type == ANSWER || columns == adornment.getBound().length;
            this.type = type;
            this.predicate = predicate;
            this.adornment = adornment;
            this.relation = relation;
            this.marker = relation.createMarker();

            this.orderedCols = new ArrayList<>(columns);
            this.orderedCols2Pos = new HashMap<>(columns+1, 1);
            if (type == ANSWER) {
                for (int i = 0; i < columns; i++) {
                    String name = Integer.toString(i + 1);
                    orderedCols.add(name);
                    orderedCols2Pos.put(name, i);
                }
            } else {
                int pos = 0;
                for (int i : adornment.getBound()) {
                    String name = Integer.toString(i + 1);
                    orderedCols.add(name);
                    orderedCols2Pos.put(name, pos++);
                }
            }
        }

        public void addPositional(@Nonnull AdornedAtom atom, @Nonnull Iterable<Row> iterable) {
            Preconditions.checkArgument(atom.getAdornment().getBound().length == orderedCols.size());

            List<String> names = new ArrayList<>();
            List<Object> constants = new ArrayList<>();
            for (Term term : atom.getBound()) {
                names.add(term.isVariable() ? term.getName() : null);
                constants.add(term.isVariable() ? null : term.getValue());
            }
            assert names.size() == orderedCols.size();
            List<Object> values = new ArrayList<>(orderedCols.size());
            for (Row row : iterable) {
                int i = 0;
                for (String name : names) {
                    values.add(name != null ? row.get(name) : constants.get(i));
                    ++i;
                }
                IndexedListRow encoded = new IndexedListRow(orderedCols2Pos, orderedCols, values);
                relation.addRow(encoded);
                values.clear();
            }
        }

        public void addPositional(@Nonnull List<Term> terms) {
            String free = terms.stream().filter(Term::isVariable).map(Term::getName)
                    .collect(Collectors.joining());
            Preconditions.checkArgument(free.isEmpty(), "Variables not allowed: " + free);
            int columnsCount = relation.getColumns().size();
            Preconditions.checkArgument(terms.size() == columnsCount,
                    format("terms.size()=%d, expected %d", terms.size(), columnsCount));

            List<Object> values = new ArrayList<>(terms.size());
            for (Term term : terms) values.add(term.getValue());
            IndexedListRow row = new IndexedListRow(orderedCols2Pos, orderedCols, values);
            relation.addRow(row);
        }


        public @Nonnull String getPredicate() { return predicate; }
        public @Nonnull Adornment getAdornment() { return adornment; }
        public @Nonnull UniqueSequencedRelation getRelation() { return relation; }
        public @Nonnull Type getType() {
            return type;
        }
        public @Nonnull Marker getMarker() { return marker; }
        public void updateMarker() { marker = relation.createMarker(); }

        @Override
        public String toString() {
            return String.format("%s_%s^%s{%d cols,marker=%s}", type.prefix(), predicate,
                    adornment, orderedCols.size(), marker);
        }
    }

    public QSQRGlobals(@Nonnull UniqueSequencedRelation.Factory relationFactory) {
        map = new HashMap<>();
        this.relationFactory = relationFactory;
    }

    public void clearInputs() {
        map.values().stream().filter(g -> g.getType() == Type.INPUT)
                .forEach(g -> g.getRelation().clear());
    }

    /**
     * Gets the relation of the given type for the given adorned predicate
     */
    public @Nonnull Global get(@Nonnull Type type, @Nonnull AdornedAtom atom) {
        RelationName name = new RelationName(type, atom);
        List<String> cols = name.getColumns();
        return map.computeIfAbsent(name, k -> new Global(type, atom.getPredicate(),
                atom.getAdornment(), relationFactory.create(name.toString(), cols)));
    }

    /**
     * Gets a {@link Stream} over all {@link Global}s of the given type.
     */
    public Stream<Global> streamAll(@Nonnull Type type) {
        return map.values().stream().filter(g -> g.type == type);
    }
}
