package br.ufsc.lapesd.qsq.detail;

import br.ufsc.lapesd.qsq.relations.Marker;
import br.ufsc.lapesd.qsq.relations.RelationUtils;
import br.ufsc.lapesd.qsq.relations.UniqueSequencedRelation;
import br.ufsc.lapesd.qsq.relations.impl.EmptyRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.rules.AtomUtils;
import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedAtom;
import br.ufsc.lapesd.qsq.rules.adornment.AdornedRule;
import br.ufsc.lapesd.qsq.rules.adornment.Adornment;

import javax.annotation.Nonnull;
import java.util.*;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class QSQTemplate {
    private final @Nonnull AdornedRule rule;
    private final @Nonnull List<UniqueSequencedRelation> supp;
    private final @Nonnull List<Marker> markers;

    public QSQTemplate(@Nonnull Rule rule, @Nonnull Adornment adornment,
                       @Nonnull UniqueSequencedRelation.Factory relationFactory) {
        SupplementaryRelations relations = new SupplementaryRelations(rule, adornment);
        this.rule = relations.createAdornedRule();
        int suppCount = relations.getSupplementaryCount();
        supp = new ArrayList<>(suppCount);
        markers = new ArrayList<>(suppCount);
        for (int i = 0; i < suppCount; i++) {
            List<String> columns = relations.getColumnsOfSupplementary(i);
            supp.add(columns.isEmpty() ? new EmptyRelation() : relationFactory.create(columns));
            markers.add(supp.get(i).createMarker());
        }
    }

    public QSQTemplate(@Nonnull Rule rule, @Nonnull String adornment,
                       @Nonnull UniqueSequencedRelation.Factory relationFactory) {
        this(rule, new Adornment(adornment), relationFactory);
    }

    public @Nonnull AdornedRule getRule() {
        return rule;
    }
    public @Nonnull UniqueSequencedRelation getSupp(int i) {
        return supp.get(i);
    }
    public @Nonnull UniqueSequencedRelation getLastSupp() {
        return supp.get(supp.size()-1);
    }
    public int getSuppCount() {
        return supp.size();
    }

    public @Nonnull Marker getMarker(int i) {
        return markers.get(i);
    }
    public @Nonnull Marker updateMarker(int i) {
        return markers.set(i, supp.get(i).createMarker());
    }
    public @Nonnull Marker getLastMarker() {
        return markers.get(markers.size()-1);
    }

    public void addInputToSupp0(@Nonnull Iterable<Row> rows) {
        AdornedAtom head = getRule().getHead();
        UniqueSequencedRelation supp0 = getSupp(0);
        if (supp0 instanceof EmptyRelation)
            return;
        IndexedListRow.Factory factory = new IndexedListRow.Factory(supp0.getColumns());
        List<Object> values = new ArrayList<>(factory.getLength());

        List<String> sources = new ArrayList<>();
        List<String> varNamesOrNull = AtomUtils.getVarNamesOrNull(head);
        Set<String> rowsColumns = new HashSet<>(RelationUtils.getColumns(rows));
        for (String name : factory.getColumns()) {
            for (int i = 0; i < varNamesOrNull.size(); i++) {
                if (Objects.equals(varNamesOrNull.get(i), name)) {
                    String str = Integer.toString(i + 1);
                    if (rowsColumns.contains(str)) {
                        sources.add(str);
                        break;
                    }
                }
            }
        }

        rows_loop:
        for (Row row : rows) {
            for (int i = 0; i < head.getTerms().size(); i++) {
                Term t = head.getTerms().get(i);
                String str = Integer.toString(i + 1);
                boolean unifies = t.isVariable() || !rowsColumns.contains(str)
                               || Objects.equals(t.getValue(), row.get(str));
                if (!unifies)
                    continue rows_loop; //unification failure
            }
            for (String source : sources) values.add(row.get(source));
            supp0.addRow(factory.create(values));
            values.clear();
        }
    }

    public @Nonnull String dump() {
        StringBuilder builder = new StringBuilder();
        builder.append("QSQTemplate | ");
        builder.append(getRule());
        builder.append('\n');
        List<String> columnLists = new ArrayList<>();
        for (int i = 0; i < getSuppCount(); i++)
            columnLists.add(String.join(",", getSupp(i).getColumns()));
        int  columnListsLen = columnLists.stream().map(String::length)
                .max(Integer::compareTo).orElse(0);
        for (int i = 0; i < getSuppCount(); i++) {
            builder.append(format("  supp_%03d[%"+columnListsLen+"s]: ", i, columnLists.get(i)));
            builder.append(getSupp(i).stream().map(Object::toString).collect(joining(", ")));
            builder.append('\n');
        }
        return builder.toString();
    }
    public void dumpToErr() {
        System.err.print(dump());
    }

}
