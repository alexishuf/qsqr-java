package br.ufsc.lapesd.qsq.rdf.tbox;

import br.ufsc.lapesd.qsq.rdf.program.RDFProgram;
import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import br.ufsc.lapesd.qsq.relations.TBox;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import static br.ufsc.lapesd.qsq.rdf.ModelUtils.list;

public class RDFTBoxLoader {
    private boolean fetchImports = true;
    private Set<Resource> fetched = new HashSet<>();
    private Model model;
    private @Nullable String name;
    private @Nonnull RDFProgram program = RDFPrograms.EMPTY;
    private @Nullable RDFTBoxReasoner.Factory reasonerFactory = null;

    /* --- configurations --- */

    public boolean getFetchImports() {
        return fetchImports;
    }
    public @Nonnull RDFProgram getProgram() {
        return program;
    }
    public @Nullable RDFTBoxReasoner.Factory getReasonerFactory() {
        return reasonerFactory;
    }

    public RDFTBoxLoader fetchingImports(boolean fetchImports) {
        this.fetchImports = fetchImports;
        return this;
    }
    public RDFTBoxLoader withReasonerFactory(@Nullable RDFTBoxReasoner.Factory reasonerFactory) {
        this.reasonerFactory = reasonerFactory;
        return this;
    }
    public RDFTBoxLoader withProgram(@Nonnull RDFProgram program) {
        this.program = program;
        return this;
    }

    /* --- general --- */

    public RDFTBoxLoader(@Nullable String name) {
        this.model = ModelFactory.createDefaultModel();
        this.name = name;
    }

    public RDFTBoxLoader() {
        this((String) null);
    }

    public RDFTBoxLoader(@Nonnull Model model) {
        this((String) null);
        addModel(model);
    }

    @Override
    public String toString() {
        return name != null ? "["+name+"]" : super.toString();
    }

    public @Nonnull Model getModel() {
        return model;
    }
    public @Nonnull TBox getTBox() {
        return new ModelTBox(getModel());
    }

    public RDFTBoxLoader reason() {
        if (reasonerFactory == null || program.getRuleCount() == 0)
            return this; //nothing to reason
        try (RDFTBoxReasoner reasoner = reasonerFactory.create(program.getDataPredicate())) {
            reasoner.setProgram(getProgram());
            Model old = getModel();
            this.model = reasoner.materialize(old);
            old.close();
        }
        return this;
    }

    /* --- load stuff --- */

    /**
     * Copies the contents of model into the TBox. Ownership of model is not transferred
     */
    public RDFTBoxLoader addModel(@Nonnull Model model) {
        list(model, null, RDF.type, OWL2.Ontology, Statement::getSubject).forEach(fetched::add);
        this.model.add(model);
        if (fetchImports) {
            list(model, null, OWL2.imports, null, Statement::getResource)
                    .filter(r -> !fetched.contains(r))
                    .forEach(this::fetchOntology);
        }
        return this;
    }

    private @Nullable
    Model loadFromResource(@Nonnull String path, @Nonnull ClassLoader loader,
                           @Nonnull Lang lang) {
        Model model = ModelFactory.createDefaultModel();
        try (InputStream stream = loader.getResourceAsStream(path)) {
            if (stream == null) return null;
            RDFDataMgr.read(model, stream, lang);
            return model;
        } catch (IOException e) {
            throw new RuntimeException("IOException reading from resource " + path, e);
        }
    }

    /**
     * Loads RDF into the TBox from a resource file.
     *
     * @param path absolute or relative (to this class) path to the resource path
     */
    public RDFTBoxLoader addFromResource(@Nonnull String path) {
        Lang lang = RDFLanguages.filenameToLang(path);
        ClassLoader local = RDFTBoxLoader.class.getClassLoader();
        ClassLoader system = ClassLoader.getSystemClassLoader();
        String pathNoTrailing = path.startsWith("/") ? path.substring(1) : path;

        Model model = loadFromResource(path, local, lang);
        if (model == null)
            model = loadFromResource(pathNoTrailing, system, lang);
        if (model == null && !path.startsWith("/"))
            model = loadFromResource("br/ufsc/lapesd/qsq/rdf/" + path, system, lang);
        if (model == null && !path.startsWith("/"))
            model = loadFromResource("br/ufsc/lapesd/qsq/rdf/" + path, local, lang);
        if (model == null && !path.startsWith("/"))
            model = loadFromResource("/br/ufsc/lapesd/qsq/rdf/" + path, local, lang);
        if (model == null)
            throw new RuntimeException("Resource not found", new FileNotFoundException(path));
        else
            addModel(model);
        return this;
    }

    public RDFTBoxLoader addRDF() {
        addFromResource("rdf.ttl");
        return this;
    }

    public RDFTBoxLoader addRDFS() {
        addFromResource("rdf-schema.ttl");
        return this;
    }

    public RDFTBoxLoader addOWL() {
        addFromResource("owl.ttl");
        return this;
    }

    public RDFTBoxLoader fetchOntology(@Nonnull Resource ontology) {
        if (ontology.isAnon()) return this;
        String uri = ontology.getURI();
        if (!uri.endsWith("#"))
            uri += "#";

        if (uri.equals(RDF.getURI()))
            return addRDF();
        else if(uri.equals(RDFS.getURI()))
            return addRDFS();
        else if (uri.equals(OWL2.getURI()))
            return addOWL();
        else
            return fetchOntology(ontology.getURI());
    }
    public RDFTBoxLoader fetchOntology(@Nonnull String uri) {
        Model model = ModelFactory.createDefaultModel();
        RDFDataMgr.read(model, uri);
        addModel(model);
        return this;
    }
}
