package br.ufsc.lapesd.qsq.rdf.classifiers;

import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.classifiers.TBoxClassifier;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.*;

import static br.ufsc.lapesd.qsq.rdf.RDFTermUtils.getURI;

public class RDFTBoxClassifier implements TBoxClassifier {
    private static @Nonnull Set<String> defaultTBoxPredicates;
    private static @Nonnull Set<String> defaultTBoxClasses;
    private @Nonnull Set<String> tboxPredicates;
    private @Nonnull Set<String> tboxClasses;

    public RDFTBoxClassifier(@Nonnull Set<String> tboxPredicates,
                             @Nonnull Set<String> tboxClasses) {
        this.tboxPredicates = tboxPredicates;
        this.tboxClasses = tboxClasses;
    }

    public RDFTBoxClassifier() {
        this(defaultTBoxPredicates, defaultTBoxClasses);
    }

    @Override
    public @Nonnull List<Rule> getTBoxRules(@Nonnull Program program) {
        List<Rule> list = new ArrayList<>();
        for (Rule rule : program.getRules()) {
            List<Term> terms = rule.getHead().getTerms();
            if (terms.size() != 3)
                break; //special Atom, not a triple
            String predicate = getURI(rule.getHead().getTerms().get(1));
            boolean select = tboxPredicates.contains(predicate)
                    || (Objects.equals(predicate, RDF.type.getURI())
                        && tboxClasses.contains(getURI(terms.get(2))))
                    || terms.stream().noneMatch(Term::isVariable);
            if (select)
                list.add(rule);
        }
        return list;
    }

    static {
        Set<String> set = new HashSet<>();
        /* predicates that appear as heads in https://www.w3.org/TR/owl2-profiles */
        set.add(RDFS.subClassOf.getURI());
        set.add(RDFS.subPropertyOf.getURI());
        set.add(RDFS.domain.getURI());
        set.add(RDFS.range.getURI());
        set.add(OWL2.equivalentClass.getURI());
        set.add(OWL2.equivalentProperty.getURI());
        /* other predicates that TBox membership of a triple */
        set.add(OWL2.allValuesFrom.getURI());
        set.add(OWL2.cardinality.getURI());
        set.add(OWL2.complementOf.getURI());
        set.add(OWL2.datatypeComplementOf.getURI());
        set.add(OWL2.disjointUnionOf.getURI());
        set.add(OWL2.disjointWith.getURI());
        set.add(OWL2.hasKey.getURI());
        set.add(OWL2.hasValue.getURI());
        set.add(OWL2.intersectionOf.getURI());
        set.add(OWL2.maxCardinality.getURI());
        set.add(OWL2.maxQualifiedCardinality.getURI());
        set.add(OWL2.minCardinality.getURI());
        set.add(OWL2.minQualifiedCardinality.getURI());
        set.add(OWL2.onClass.getURI());
        set.add(OWL2.onDataRange.getURI());
        set.add(OWL2.onDatatype.getURI());
        set.add(OWL2.onProperties.getURI());
        set.add(OWL2.onProperty.getURI());
        set.add(OWL2.propertyChainAxiom.getURI());
        set.add(OWL2.propertyDisjointWith.getURI());
        set.add(OWL2.qualifiedCardinality.getURI());
        set.add(OWL2.someValuesFrom.getURI());
        set.add(OWL2.sourceIndividual.getURI());
        set.add(OWL2.targetIndividual.getURI());
        set.add(OWL2.targetValue.getURI());
        set.add(OWL2.inverseOf.getURI());
        set.add(OWL2.unionOf.getURI());
        set.add(OWL2.withRestrictions.getURI());
        defaultTBoxPredicates = set;

        set = new HashSet<>();
        set.add(RDF.Property.getURI());
        set.add(RDFS.Class.getURI());
        set.add(OWL2.Class.getURI());
        set.add(OWL2.ObjectProperty.getURI());
        set.add(OWL2.DatatypeProperty.getURI());
        set.add(OWL2.Restriction.getURI());
        defaultTBoxClasses = set;
    }
}
