package br.ufsc.lapesd.qsq.rdf.tbox;

import br.ufsc.lapesd.qsq.rdf.ModelUtils;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.system.PrefixMap;
import org.apache.jena.riot.system.PrefixMapStd;
import org.semanticweb.vlog4j.core.model.api.PositiveLiteral;
import org.semanticweb.vlog4j.core.reasoner.KnowledgeBase;
import org.semanticweb.vlog4j.core.reasoner.QueryResultIterator;
import org.semanticweb.vlog4j.core.reasoner.implementation.VLogReasoner;
import org.semanticweb.vlog4j.parser.RuleParser;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import static java.lang.ProcessBuilder.Redirect.INHERIT;
import static java.lang.ProcessBuilder.Redirect.PIPE;
import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.jena.riot.RDFFormat.TURTLE_PRETTY;

public class RobustVLogRDFTBoxReasoner extends VLogRDFTBoxReasoner {
    public static final int DEFAULT_TIMEOUT_MS = 20000;
    public static final int DEFAULT_RETRIES = 3;

    private int timeoutMs = DEFAULT_TIMEOUT_MS;
    private int retries = DEFAULT_RETRIES;

    public RobustVLogRDFTBoxReasoner(@Nonnull String dataPredicate) {
        super(dataPredicate);
    }

    public RobustVLogRDFTBoxReasoner(@Nonnull String dataPredicate, @Nonnull PrefixMap prefixMap) {
        super(dataPredicate, prefixMap);
    }

    public static class Factory implements RDFTBoxReasoner.Factory {
        @Override
        public @Nonnull RDFTBoxReasoner create(@Nonnull String dataPredicate) {
            return new RobustVLogRDFTBoxReasoner(dataPredicate);
        }
        @Override
        public String toString() {
            return RobustVLogRDFTBoxReasoner.class.getSimpleName()+"$Factory";
        }
    }
    public static final Factory FACTORY = new Factory();

    public int getTimeoutMs() {
        return timeoutMs;
    }

    public int getRetries() {
        return retries;
    }

    public RobustVLogRDFTBoxReasoner withTimeoutMs(int timeoutMs) {
        this.timeoutMs = timeoutMs;
        return this;
    }

    public RobustVLogRDFTBoxReasoner withRetries(int retries) {
        this.retries = retries;
        return this;
    }

    @Override
    public @Nonnull Model materialize(@Nonnull Model model) {
        for (int i = 0; i < retries; i++) {
            Model result = tryMaterialize(model);
            if (result == null) {
                /* sometimes the chase diverges. re-hash the model to generate a slightly
                 * reordered serialization which may converge. */
                Model copy = ModelFactory.createDefaultModel();
                model.listStatements().forEachRemaining(copy::add);
                model = copy;
            } else {
                return result;
            }
        }
        throw new Exception(this, "Tried "+retries+" times, but failed. Giving up.");
    }

    private Model tryMaterialize(@Nonnull Model model) {
        String sep = getProperty("file.separator");
        try (VLogSpec spec = new VLogSpec(model)) {
            List<String> args = Arrays.asList(getProperty("java.home") + sep + "bin" + sep + "java",
//                    "-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=*:5005",
                    "-cp", getProperty("java.class.path"), Main.class.getName(),
                    getDataPredicate(), spec.vlogRulesToFile().getAbsolutePath());
            Process child = new ProcessBuilder(args).redirectInput(PIPE)
                    .redirectOutput(PIPE).redirectError(INHERIT).start();
            boolean aborted = false;
            try (ObjectOutputStream out = new ObjectOutputStream(child.getOutputStream())) {
                out.writeObject(getPrefixMap().getMappingCopyStr());
            } catch (IOException e) {
                System.err.println(this+": problem sending PrefixMap to child: ");
                e.printStackTrace();
                child.destroy();
                aborted = true;
            }
            if (!child.waitFor(timeoutMs, MILLISECONDS))
                child.destroyForcibly().waitFor(5, SECONDS);
            if (aborted)
                return null;
            try (ObjectInputStream in = new ObjectInputStream(child.getInputStream())) {
                Object obj = in.readObject();
                if (obj == null) return null; //retry
                if (obj instanceof java.lang.Exception) {
                    throw Exception.wrap(this, (java.lang.Exception)obj);
                } else if (obj instanceof File) {
                    Model result = ModelFactory.createDefaultModel();
                    try (FileInputStream fileIn = new FileInputStream((File) obj);
                         GZIPInputStream gzIn = new GZIPInputStream(fileIn)) {
                        RDFDataMgr.read(result, gzIn, Lang.TURTLE);
                    }
                    return result;
                }
            } catch (IOException e) {
                System.err.println(this+": problem reading result from child: ");
                e.printStackTrace();
                return null;
            }
        } catch (ClassNotFoundException | InterruptedException | IOException e) {
            throw new Exception(this, e);
        }
        return null;
    }

    public static class Main {
        public static void main(String[] args) throws IOException {
            File file;
            try (ObjectOutputStream out = new ObjectOutputStream(System.out)) {
                try {
                    file = run(args);
                    out.writeObject(file);
                } catch (java.lang.Exception e) {
                    out.writeObject(e);
                }
            }
        }

        private static @Nonnull File run(String[] args) throws java.lang.Exception {
            Preconditions.checkArgument(args.length == 2, "Expected argc==2, got"+args.length);
            String dataPredicate = args[0].trim();
            Preconditions.checkArgument(!dataPredicate.isEmpty(), "dataPredicate cannot be empty");
            File rulesFile = getFile(args[1], "rulesFile");
            PrefixMap prefixMap = readPrefixMap();

            KnowledgeBase kb;
            try (FileInputStream in = new FileInputStream(rulesFile)) {
                kb = RuleParser.parse(in, "UTF-8");
            }

            Model result = ModelFactory.createDefaultModel();
            try (VLogReasoner reasoner = new VLogReasoner(kb)) {
                reasoner.reason();
                String allStr = format("%s(?S, ?P, ?O)", dataPredicate);
                PositiveLiteral all = RuleParser.parsePositiveLiteral(allStr);
                try (QueryResultIterator it = reasoner.answerQuery(all, false)) {
                    it.forEachRemaining(ans -> addSPO(result, prefixMap, ans));
                }
            }
            return ModelUtils.toTemp(result, true, TURTLE_PRETTY, false);
        }

        private static @Nonnull File getFile(@Nonnull String arg,
                                             @Nonnull String name) throws FileNotFoundException {
            File file = new File(arg.trim());
            String path = file.getAbsolutePath();
            if (!file.exists())
                throw new FileNotFoundException(path);
            if (!file.isFile())
                throw new IllegalArgumentException(name+"="+path+" is not a file");
            return file;
        }

        private @Nonnull
        static PrefixMap readPrefixMap()
                throws IOException, ClassNotFoundException {
            try (ObjectInputStream oin = new ObjectInputStream(System.in)) {
                @SuppressWarnings("unchecked")
                Map<String, String> map = (Map<String, String>) oin.readObject();
                Preconditions.checkArgument(map != null, "null map received on in");
                PrefixMapStd prefixMap = new PrefixMapStd();
                map.forEach(prefixMap::add);
                return prefixMap;
            }
        }
    }
}
