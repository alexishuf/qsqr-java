package br.ufsc.lapesd.qsq.rdf.program;

import br.ufsc.lapesd.qsq.rules.Program;

import javax.annotation.Nonnull;

public interface RDFProgram extends Program {
    @Nonnull String getDataPredicate();
}
