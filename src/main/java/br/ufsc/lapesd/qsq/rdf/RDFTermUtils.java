package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.rules.Term;
import com.google.common.base.Preconditions;
import org.apache.jena.atlas.io.StringWriterI;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.out.NodeFormatterTTL;
import org.apache.jena.riot.system.PrefixMap;
import org.apache.jena.riot.system.PrefixMapStd;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

public class RDFTermUtils {
    private static final @Nonnull Pattern URI_PATTERN = compile("(?i)\\s*<?([a-z]+):(.*)>?\\s*");
    private static final @Nonnull PrefixMapStd emptyPrefixMap = new PrefixMapStd();
    private static final @Nonnull Pattern ANGLE_RX = compile("<([^>]*)>");
    private static final @Nonnull Pattern PREFIXED_RX = compile("(?i)([a-z0-9_+.\\-]+):(.*)");
    private static final @Nonnull Pattern LIT_RX = compile("\"(.*)\"(?:\\^\\^(.*))?(?:@(\\w+))?");
    private static final @Nonnull Set<String> protocols =
            new HashSet<>(Arrays.asList("http", "https", "ftp", "coap"));
    public static PrefixMap defaultPrefixMap;


    public static String getURI(@Nonnull Term term) {
        return  (term.isVariable()) ? null : getURI(term.getValue());
    }

    public static String getURI(@Nullable Object value) {
        if (value == null) return null;
        if (value instanceof Resource)
            return ((Resource) value).isURIResource() ? ((Resource) value).getURI() : null;
        String str = value.toString();
        return URI_PATTERN.matcher(str).find() ? str : null;
    }

    public static Resource toResource(@Nullable Object object) {
        if (object instanceof Term)
            object = ((Term) object).getValue();
        if (object == null) return null;
        String str = object.toString();
        Matcher m = URI_PATTERN.matcher(str);
        if (!m.matches())
            return null;
        if (m.group(1).equals("_"))
            return ResourceFactory.createResource();
        else
            return ResourceFactory.createResource(m.group(1) + ":" + m.group(2));
    }

    public static RDFNode toRDFNode(@Nullable Object object) {
        if (object instanceof Term)
            object = ((Term) object).getValue();
        if (object == null) return null;
        if (object instanceof RDFNode) return (RDFNode) object;
        Resource resource = toResource(object);
        if (resource != null)
            return resource;

        String message = "Do not know how to transform " + object + " into a RDFNode";
        throw new UnsupportedOperationException(message);
    }

    /**
     * Converts a RDFNode into its representation in Turtle, using the given prefix map.
     *
     * The node cannot be a blank node.
     */
    public static @Nonnull String toTTL(@Nonnull RDFNode node,
                                        @Nonnull PrefixMap prefixMap) {
        Preconditions.checkArgument(!node.isAnon(), "BNodes are not supported");

        NodeFormatterTTL formatter = new NodeFormatterTTL(null, prefixMap);
        StringWriterI writer = new StringWriterI();
        formatter.format(writer, node.asNode());
        return writer.toString();
    }
    public static @Nonnull String toTTL(@Nonnull Term term) {
        return toTTL(term, emptyPrefixMap);
    }
    public static @Nonnull String toTTL(@Nonnull Term term, @Nonnull PrefixMap prefixMap) {
        return toTTL(toRDFNode(term), prefixMap);
    }
    public static @Nonnull String toTTL(@Nonnull RDFNode node) {
        return toTTL(node, emptyPrefixMap);
    }

    public static @Nonnull RDFNode fromTTL(@Nonnull String fragment) {
        return fromTTL(fragment, emptyPrefixMap);
    }
    public static @Nonnull RDFNode fromTTL(@Nonnull Term termWithString) {
        return fromTTL(termWithString, emptyPrefixMap);
    }
    public static @Nonnull RDFNode fromTTL(@Nonnull Term termWithString,
                                           @Nonnull PrefixMap prefixMap) {
        Object value = termWithString.getValue();
        Preconditions.checkNotNull(value, "Term value cannot be null");
        if (value instanceof RDFNode)
            return (RDFNode)value;
        return fromTTL(value.toString(), prefixMap);
    }

    public static @Nonnull RDFNode fromTTL(@Nonnull String fragment,
                                           @Nonnull PrefixMap prefixMap) {
        Matcher matcher = ANGLE_RX.matcher(fragment);
        if (matcher.matches())
            return ResourceFactory.createResource(matcher.group(1));
        matcher = PREFIXED_RX.matcher(fragment);
        if (matcher.matches()) {
            String prefix = matcher.group(1), name = matcher.group(2);
            if (protocols.contains(prefix) && name.startsWith("//"))
                return ResourceFactory.createResource(fragment); //fragment is an URL
            else if (prefix.equals("urn") && !prefixMap.contains("urn"))
                return ResourceFactory.createResource(fragment);
            String expanded = prefixMap.expand(prefix, name);
            if (expanded == null)
                throw new NoSuchElementException("Unknown prefix: "+prefix);
            return ResourceFactory.createResource(expanded);
        }
        matcher = LIT_RX.matcher(fragment);
        if (!matcher.matches())
            throw new IllegalArgumentException("Fragment is not valid Turtle: " + fragment);
        if (matcher.group(2) != null) {
            RDFNode dtNode = fromTTL(matcher.group(2), prefixMap);
            if (!dtNode.isURIResource())
                throw new IllegalArgumentException("Datatype must be an URI");

            TypeMapper tm = TypeMapper.getInstance();
            RDFDatatype dt = tm.getSafeTypeByName(dtNode.asResource().getURI());
            return ResourceFactory.createTypedLiteral(matcher.group(1), dt);
        } else if (matcher.group(3) != null) {
            return ResourceFactory.createLangLiteral(matcher.group(1), matcher.group(3));
        } else {
            return ResourceFactory.createPlainLiteral(matcher.group(1));
        }
    }


    static {
        PrefixMap map = new PrefixMapStd();
        map.add("rdf", RDF.getURI());
        map.add("rdfs", RDFS.getURI());
        map.add("owl", OWL2.getURI());
        map.add("xsd", XSD.NS);
        map.add("foaf", FOAF.NS);
        map.add("skos", SKOS.getURI());
        map.add("dct", DCTerms.NS);
        RDFTermUtils.defaultPrefixMap = map;
    }
}
