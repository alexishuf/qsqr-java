package br.ufsc.lapesd.qsq.rdf.tbox;

import br.ufsc.lapesd.qsq.rdf.program.RDFPrograms;
import br.ufsc.lapesd.qsq.rules.Program;
import org.apache.jena.rdf.model.Model;

import javax.annotation.Nonnull;

import static java.lang.String.format;

public interface RDFTBoxReasoner extends AutoCloseable {
    /**
     * Sets the program to be used. Can be {@link RDFPrograms}.EMPTY
     * @return The same reasoner instance
     */
    @Nonnull RDFTBoxReasoner setProgram(@Nonnull Program program);

    /**
     * Creates a new Model with all triples in the input, plus any triples entailed by the program
     *
     * @return non-null model. In case of error, throws an {@link Exception}
     */
    @Nonnull Model materialize(@Nonnull Model model);

    @FunctionalInterface
    interface Factory {
        @Nonnull RDFTBoxReasoner create(@Nonnull String dataPredicate);
    }

    class Exception extends RuntimeException {
        public Exception(@Nonnull RDFTBoxReasoner reasoner,
                         @Nonnull String message) {
            super(format("Reasoner %s failed. Cause: %s", reasoner, message));
        }

        public Exception(@Nonnull RDFTBoxReasoner reasoner, @Nonnull java.lang.Exception cause) {
            super(format("Reasoner %s failed. Cause: %s", reasoner, cause.getMessage()), cause);
        }

        public static @Nonnull Exception wrap(@Nonnull RDFTBoxReasoner reasoner,
                                              @Nonnull java.lang.Exception cause) {
            if (cause instanceof Exception) return (Exception) cause;
            return new Exception(reasoner, cause);
        }
    }

    @Override
    void close(); // do not throw checked exceptions
}
