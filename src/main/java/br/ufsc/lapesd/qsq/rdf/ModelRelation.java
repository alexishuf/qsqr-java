package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.relations.MaterializedRelation;
import br.ufsc.lapesd.qsq.relations.MutableRelation;
import br.ufsc.lapesd.qsq.relations.ProjectionSpec;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.impl.IteratorProjection;
import br.ufsc.lapesd.qsq.relations.impl.IteratorRelation;
import br.ufsc.lapesd.qsq.relations.rows.IndexedListRow;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import org.apache.jena.rdf.model.*;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

public class ModelRelation implements MutableRelation, MaterializedRelation {
    private final @Nonnull String predicateName;
    private final @Nonnull Model model;
    public final static @Nonnull List<String> SPO = asList("s", "p", "o");
    private final @Nonnull List<String> columnMap;
    private final @Nonnull List<String> columns;

    protected ModelRelation(@Nonnull String predicateName, @Nonnull Model model) {
        this.predicateName = predicateName;
        this.model = model;
        this.columnMap = this.columns = SPO;
    }

    protected ModelRelation(@Nonnull String predicateName, @Nonnull Model model,
                            @Nonnull List<String> columnMap) {
        this.predicateName = predicateName;
        this.model = model;
        this.columnMap = Collections.unmodifiableList(columnMap);
        this.columns = columnMap.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }


    @Override
    public long getSize() {
        return model.size();
    }

    @SuppressWarnings("unchecked")
    public static @Nonnull <T> T getAs(@Nonnull Object value, @Nonnull Class<T> cls) {
        if (cls.isAssignableFrom(value.getClass()))
            return (T) value;
        if (cls.equals(Property.class))
            return (T)ResourceFactory.createProperty(value.toString());
        if (cls.equals(Resource.class) || cls.equals(RDFNode.class))
            return (T)ResourceFactory.createResource(value.toString());
        if (cls.equals(Literal.class))
            return (T)ResourceFactory.createPlainLiteral(value.toString());
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addRow(@Nonnull Row row) {
        if (columns.size() < 3)
            throw new UnsupportedOperationException("Cannot add rows when projecting!");
        Statement stmt = model.createStatement(
                getAs(requireNonNull(row.get(columnMap.get(0))), Resource.class),
                getAs(requireNonNull(row.get(columnMap.get(1))), Property.class),
                getAs(requireNonNull(row.get(columnMap.get(2))), RDFNode.class));
        boolean present = model.contains(stmt);
        if (!present)
            model.add(stmt);
        return !present;
    }

    @Override
    public void clear() {
        model.removeAll();
    }

    @Override
    public @Nonnull String getName() {
        return predicateName;
    }

    public @Nonnull Model getModel() {
        return model;
    }

    @Override
    public @Nonnull List<String> getColumns() {
        return columns;
    }

    public @Nonnull Row toRow(@Nonnull Statement s) {
        ArrayList<Object> values = new ArrayList<>(columns.size());
        if (columnMap.get(0) != null) values.add(s.getSubject());
        if (columnMap.get(1) != null) values.add(s.getPredicate());
        if (columnMap.get(2) != null) values.add(s.getObject());
        return new IndexedListRow(columns, values);
    }

    public class RowIterator implements Iterator<Row> {
        private final @Nonnull StmtIterator base;

        public RowIterator(@Nonnull StmtIterator base) {
            this.base = base;
        }

        @Override
        public boolean hasNext() {
            return base.hasNext();
        }

        @Override
        public @Nonnull Row next() {
            if (!hasNext()) throw new NoSuchElementException();
            return toRow(base.nextStatement());
        }
    }

    @Override
    public @Nonnull Iterator<Row> iterator() {
        return new RowIterator(model.listStatements());
    }

    @Override
    public @Nonnull Relation projection(@Nonnull ProjectionSpec projection) {
        return new IteratorProjection(this, projection);
    }

    private @Nonnull Iterator<Row> iteratorForSelection(@Nonnull Row query) {
        Resource s = null;
        Property p = null;
        RDFNode o = null;
        for (String column : query.getColumns()) {
            int idx = columnMap.indexOf(column);
            assert idx >= 0;
            switch (idx) {
                case 0: s = getAs(requireNonNull(query.get(column)), Resource.class); break;
                case 1: p = getAs(requireNonNull(query.get(column)), Property.class); break;
                case 2: o = getAs(requireNonNull(query.get(column)), RDFNode.class); break;
                default: assert false : "idx >= 3: " + idx;
            }
        }
        return new RowIterator(getModel().listStatements(s, p, o));
    }

    @Override
    public @Nonnull Relation selection(@Nonnull Row query) {
        String n = getName() + "@" + query;
        return new IteratorRelation(getColumns(), () -> iteratorForSelection(query), isUnique(), n);
    }

    @Override
    public boolean isUnique() {
        return columns.size() == 3; // if projected, there may be duplicates
    }

    @Override
    public long getSizeEstimate() {
        return getSize();
    }
}
