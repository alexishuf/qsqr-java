package br.ufsc.lapesd.qsq.rdf;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.apache.jena.rdf.model.RDFNode;

import javax.annotation.Nonnull;
import java.lang.reflect.Type;

public class RDFNodeGsonSerializer implements JsonSerializer<RDFNode> {
    @Override
    public JsonElement serialize(@Nonnull RDFNode src, @Nonnull Type typeOfSrc,
                                 @Nonnull JsonSerializationContext context) {
        return new JsonPrimitive(src.toString());
    }
}
