package br.ufsc.lapesd.qsq.rdf.program;

import br.ufsc.lapesd.qsq.rules.ProgramBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RDFProgramBuilder extends ProgramBuilder {
    private @Nonnull String dataPredicate = "T";

    public RDFProgramBuilder() {}

    public RDFProgramBuilder(@Nullable String name) {
        super(name);
    }

    public RDFProgramBuilder(@Nullable String name, @Nonnull String dataPredicate) {
        super(name);
        this.dataPredicate = dataPredicate;
    }

    public @Nonnull String getDataPredicate() {
        return dataPredicate;
    }

    public RDFProgramBuilder withDataPredicate(@Nonnull String dataPredicate) {
        this.dataPredicate = dataPredicate;
        return this;
    }

    public static @Nonnull RDFProgramBuilder start(@Nullable String name) {
        return new RDFProgramBuilder(name);
    }
    public static @Nonnull RDFProgramBuilder start(@Nullable String name,
                                                   @Nonnull String dataPredicate) {
        return new RDFProgramBuilder(name, dataPredicate);
    }
    public static @Nonnull RDFProgramBuilder start() {
        return start(null);
    }

    @Override
    public @Nonnull RDFProgram build() {
        return new RDFListProgram(getDataPredicate(), rules, name);
    }


}
