package br.ufsc.lapesd.qsq.rdf.tbox;

import br.ufsc.lapesd.qsq.rdf.ModelUtils;
import br.ufsc.lapesd.qsq.rdf.RDFTermUtils;
import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Program;
import br.ufsc.lapesd.qsq.rules.Rule;
import br.ufsc.lapesd.qsq.rules.Term;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import org.apache.jena.atlas.lib.Pair;
import org.apache.jena.iri.IRI;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.system.PrefixMap;
import org.apache.jena.vocabulary.RDF;
import org.semanticweb.vlog4j.core.model.api.PositiveLiteral;
import org.semanticweb.vlog4j.core.model.api.QueryResult;
import org.semanticweb.vlog4j.core.reasoner.KnowledgeBase;
import org.semanticweb.vlog4j.core.reasoner.QueryResultIterator;
import org.semanticweb.vlog4j.core.reasoner.implementation.VLogReasoner;
import org.semanticweb.vlog4j.parser.ParsingException;
import org.semanticweb.vlog4j.parser.RuleParser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static br.ufsc.lapesd.qsq.rdf.RDFTermUtils.fromTTL;
import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;

public class VLogRDFTBoxReasoner implements RDFTBoxReasoner {
    private @Nonnull PrefixMap prefixMap;
    private @Nullable String prolog;
    private @Nullable String rules;
    private @Nullable Program program;
    private @Nonnull String dataPredicate;

    public VLogRDFTBoxReasoner(@Nonnull String dataPredicate) {
        this(dataPredicate, RDFTermUtils.defaultPrefixMap);
    }

    public VLogRDFTBoxReasoner(@Nonnull String dataPredicate, @Nonnull PrefixMap prefixMap) {
        this.dataPredicate = dataPredicate;
        this.prefixMap = prefixMap;
    }

    public static class Factory implements RDFTBoxReasoner.Factory {
        @Override
        public @Nonnull RDFTBoxReasoner create(@Nonnull String dataPredicate) {
            return new VLogRDFTBoxReasoner(dataPredicate);
        }
        @Override
        public String toString() {
            return VLogRDFTBoxReasoner.class.getSimpleName()+"$Factory";
        }
    }
    public static Factory FACTORY = new Factory();

    @Override
    public String toString() {
        String name = program == null ? "null" : "unnamed";
        if (program != null && program.getName() != null)
            name = program.getName();
        return format("VLogRDFTBoxReasoner(%s, program=%s)", dataPredicate, name);
    }

    @Override
    public @Nonnull RDFTBoxReasoner setProgram(@Nonnull Program program) {
        convertRules(this.program = program);
        return this;
    }

    public @Nullable Program getProgram() {
        return program;
    }

    public void setDataPredicate(@Nonnull String dataPredicate) {
        this.dataPredicate = dataPredicate;
    }

    public @Nonnull String getDataPredicate() {
        return dataPredicate;
    }

    @Nonnull
    public PrefixMap getPrefixMap() {
        return prefixMap;
    }

    @Override
    public void close() { /* pass */ }

    @Override
    public @Nonnull Model materialize(@Nonnull Model model) {
        try (VLogSpec spec = new VLogSpec(model)) {
            KnowledgeBase kb = RuleParser.parse(spec.vlogRules);
            Model result = ModelFactory.createDefaultModel();
            try (VLogReasoner reasoner = new VLogReasoner(kb)) {
                reasoner.reason();
                String allStr = format("%s(?S, ?P, ?O)", dataPredicate);
                PositiveLiteral all = RuleParser.parsePositiveLiteral(allStr);
                try (QueryResultIterator it = reasoner.answerQuery(all, false)) {
                    it.forEachRemaining(ans -> addSPO(result, prefixMap, ans));
                }
            }
            return result;
        } catch (ParsingException | IOException e) {
            throw new Exception(this, e);
        }
    }

    protected class VLogSpec implements AutoCloseable {
        protected @Nonnull File triplesFile;
        protected @Nonnull String vlogRules;
        protected @Nullable File vlogFile = null;

        public VLogSpec(@Nonnull Model model) throws IOException {
            Preconditions.checkArgument(rules != null, "No defined rules! Call setProgram()!");
            triplesFile = ModelUtils.toTemp(model, true, RDFFormat.NT, true);
            String loadStmt = format("@source %s(3) : load-rdf(\"%s\") .\n",
                    dataPredicate, triplesFile.getAbsolutePath());
            vlogRules = prolog + "\n" + loadStmt + "\n" + rules;
        }

        public @Nonnull File vlogRulesToFile() throws IOException {
            vlogFile = Files.createTempFile("rules", ".vlog").toFile();
            try (FileOutputStream fileOut = new FileOutputStream(vlogFile);
                 OutputStreamWriter out = new OutputStreamWriter(fileOut, Charsets.UTF_8)) {
                out.write(vlogRules);
                out.flush();
            }
            return vlogFile;
        }

        @SuppressWarnings("ResultOfMethodCallIgnored")
        @Override
        public void close() {
            triplesFile.delete();
            if (vlogFile != null)
                vlogFile.delete();
        }
    }

    protected static void addSPO(@Nonnull Model model, @Nonnull PrefixMap prefixMap,
                                 @Nonnull QueryResult result) {
        RDFNode node = fromTTL(result.getTerms().get(0).getName(), prefixMap);
        Resource p = fromTTL(result.getTerms().get(1).getName(), prefixMap).asResource();
        RDFNode  o = fromTTL(result.getTerms().get(2).getName(), prefixMap);
        if (node.isLiteral()) {
            checkArgument(p.equals(RDF.type),
                    "weird statement with literal as subject (predicate is not rdf:type)");
        } else {
            model.add(node.asResource(), ResourceFactory.createProperty(p.getURI()), o);
        }
    }

    public void convertRules(@Nonnull Program program) {
        StringBuilder sb = new StringBuilder(prefixMap.size()*64);
        for (Map.Entry<String, IRI> entry : prefixMap.getMapping().entrySet()) {
            String escaped;
            try {
                escaped = entry.getValue().toASCIIString();
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
            sb.append("@prefix ").append(entry.getKey())
                    .append(": <").append(escaped).append("> .\n");
        }
        prolog = sb.toString();
        sb = new StringBuilder(program.getRuleCount()*64);
        for (Rule rule : program.getRules())
            convertRule(sb, rule);
        rules = sb.toString();
    }

    private void convertRule(@Nonnull StringBuilder sb, @Nonnull Rule rule) {
        Set<Term> blanks = new HashSet<>();
        rule.getHead().getTerms().stream().filter(Term::isVariable).forEach(blanks::add);
        rule.getBody().stream().flatMap(a -> a.getTerms().stream()).forEach(blanks::remove);

        convertAtom(sb, rule.getHead(), blanks);
        if (!rule.getBody().isEmpty()) {
            sb.append(" :- ");
            for (Atom atom : rule.getBody())
                convertAtom(sb, atom, blanks).append(", ");
            sb.setLength(sb.length()-2);
        }
        sb.append(" .\n");
    }

    private @Nonnull StringBuilder convertAtom(@Nonnull StringBuilder sb, @Nonnull Atom atom,
                                               @Nonnull Set<Term> blanks) {
        sb.append(atom.getPredicate()).append("(");
        for (Term term : atom.getTerms()) {
            if (blanks.contains(term))
                sb.append("!").append(term.getName()).append(", ");
            else
                convertTerm(sb, term).append(", ");
        }
        sb.setLength(sb.length()-2);
        return sb.append(")");
    }

    private @Nonnull StringBuilder convertTerm(StringBuilder sb, Term term) {
        if (term.isVariable())
            return sb.append(term.toString());
        RDFNode node = RDFTermUtils.toRDFNode(term);
        if (node.isLiteral())
            sb.append(RDFTermUtils.toTTL(node, prefixMap));
        if (node.isAnon())
            throw new UnsupportedOperationException("Cannot have BNodes as constants");
        String uri = node.asResource().getURI();
        Pair<String, String> abbrev = prefixMap.abbrev(uri);
        if (abbrev != null)
            return sb.append(abbrev.getLeft()).append(":").append(abbrev.getRight());
        else
            return sb.append("<").append(term.getValue().toString()).append(">");
    }
}
