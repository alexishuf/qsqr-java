package br.ufsc.lapesd.qsq.rdf.tbox;

import br.ufsc.lapesd.qsq.rdf.ModelUtils;
import br.ufsc.lapesd.qsq.relations.TBox;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.stream.Stream;

import static br.ufsc.lapesd.qsq.rdf.RDFTermUtils.toResource;

public class ModelTBox implements TBox {
    private @Nonnull Model model;

    /**
     * Creates a TBox interface backed by a Model with all materialized inferences.
     */
    public ModelTBox(@Nonnull Model model) {
        this.model = model;
    }

    @Override
    public boolean isSubClassOf(@Nonnull Object subClass, @Nonnull Object superClass) {
        Resource sub = toResource(subClass), sup = toResource(superClass);
        Preconditions.checkArgument(sub != null, "subClass=" + subClass + " is not a resource");
        Preconditions.checkArgument(sup != null, "superClass=" + superClass + " is not a resource");
        return sub.equals(sup) || model.contains(sub, RDFS.subClassOf, sup);
    }

    @Override
    public @Nonnull Stream<Object> streamSubClasses(@Nonnull Object superClass) {
        Resource root = toResource(superClass);
        Preconditions.checkArgument(root != null, "class="+superClass+" is not a resource");
        return ModelUtils.list(model, null, RDFS.subClassOf, root, Statement::getSubject);
    }

    @Override
    public @Nonnull Stream<Object> streamSuperClasses(@Nonnull Object subClass) {
        Resource root = toResource(subClass);
        Preconditions.checkArgument(root != null, "class="+subClass+" is not a resource");
        return ModelUtils.list(model, root, RDFS.subClassOf, null, Statement::getResource);
    }

    @Override
    public boolean isSubPropertyOf(@Nonnull Object subProperty, @Nonnull Object superProperty) {
        Resource sub = toResource(subProperty), sup = toResource(superProperty);
        Preconditions.checkArgument(sub != null, "subClass="+subProperty+" is not a resource");
        Preconditions.checkArgument(sup != null, "superClass="+superProperty+" is not a resource");
        return sub.equals(sup) || model.contains(sub, RDFS.subPropertyOf, sup);
    }

    @Override
    public @Nonnull Stream<Object> streamSubProperties(@Nonnull Object superProperty) {
        Resource root = toResource(superProperty);
        Preconditions.checkArgument(root != null, "class="+superProperty+" is not a resource");
        return ModelUtils.list(model, null, RDFS.subPropertyOf, root, Statement::getSubject);
    }

    @Override
    public @Nonnull Stream<Object> streamSuperProperties(@Nonnull Object subProperty) {
        Resource root = toResource(subProperty);
        Preconditions.checkArgument(root != null, "class="+subProperty+" is not a resource");
        return ModelUtils.list(model, root, RDFS.subPropertyOf, null, Statement::getResource);
    }
}
