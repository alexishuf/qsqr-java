package br.ufsc.lapesd.qsq.rdf.program;

import br.ufsc.lapesd.qsq.rules.ListProgram;
import br.ufsc.lapesd.qsq.rules.Rule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;

public class RDFListProgram extends ListProgram implements RDFProgram {
    private @Nonnull String dataPredicate;

    public RDFListProgram(@Nonnull String dataPredicate, @Nonnull List<Rule> rules,
                          @Nullable String name) {
        super(rules, name);
        this.dataPredicate = dataPredicate;
    }

    @Override
    public @Nonnull String getDataPredicate() {
        return dataPredicate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RDFListProgram that = (RDFListProgram) o;
        return getDataPredicate().equals(that.getDataPredicate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDataPredicate());
    }
}
