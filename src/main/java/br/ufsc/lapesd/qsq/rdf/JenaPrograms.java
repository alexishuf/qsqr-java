package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.rdf.program.RDFListProgram;
import br.ufsc.lapesd.qsq.rdf.program.RDFProgram;
import br.ufsc.lapesd.qsq.rules.Atom;
import br.ufsc.lapesd.qsq.rules.Term;
import br.ufsc.lapesd.qsq.rules.plain.PlainAtom;
import br.ufsc.lapesd.qsq.rules.plain.PlainRule;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.graph.*;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.reasoner.TriplePattern;
import org.apache.jena.reasoner.rulesys.ClauseEntry;
import org.apache.jena.reasoner.rulesys.Functor;
import org.apache.jena.reasoner.rulesys.Rule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static br.ufsc.lapesd.qsq.rules.Term.createConstant;
import static br.ufsc.lapesd.qsq.rules.Term.createVariable;
import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;

public class JenaPrograms {
    private static class CacheKey {
        public final @Nonnull String t;
        public final boolean ignoreUnsupported;
        public final @Nonnull String filename;

        public CacheKey(@Nonnull String t, boolean ignoreUnsupported, @Nonnull String filename) {
            this.t = t;
            this.ignoreUnsupported = ignoreUnsupported;
            this.filename = filename;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CacheKey cacheKey = (CacheKey) o;
            return ignoreUnsupported == cacheKey.ignoreUnsupported &&
                    t.equals(cacheKey.t) &&
                    filename.equals(cacheKey.filename);
        }

        @Override
        public int hashCode() {
            return Objects.hash(t, ignoreUnsupported, filename);
        }
    }
    private static ConcurrentHashMap<CacheKey, RDFProgram> cached = new ConcurrentHashMap<>();
    private static final @Nonnull Set<String> NOP_FUNCTORS =
            Stream.of("table", "tableAll", "bound").collect(toSet());

    public static class UnsupportedFeature extends Exception {
        public UnsupportedFeature(@Nonnull String message) {
            super(message);
        }
    }

    public static @Nonnull RDFProgram loadJenaRules(@Nonnull String t, boolean ignoreUnsupported,
                                                 @Nonnull BufferedReader reader)
            throws UnsupportedFeature {
        return loadJenaRules(t, ignoreUnsupported, reader, null);
    }
    public static @Nonnull RDFProgram loadJenaRules(@Nonnull String t, boolean ignoreUnsupported,
                                                 @Nonnull BufferedReader reader,
                                                 @Nullable String programName)
            throws UnsupportedFeature {
        Rule.Parser parser = Rule.rulesParserFromReader(reader);
        List<Rule> rules = Rule.parseRules(parser);
        return convertJenaRules(t, ignoreUnsupported, rules, programName);
    }

    public static @Nonnull RDFProgram loadJenaRules(@Nonnull String t, boolean ignoreUnsupported,
                                                 @Nonnull String filename)
            throws UnsupportedFeature {
        try {
            String name = "jena/" + filename + (ignoreUnsupported ? "[U]" : "");
            return loadJenaRules(t, ignoreUnsupported, openJenaRulesFile(filename), name);
        } catch (FileNotFoundException e) {
            // Since the files are resources, there is no point in handling the exception
            // its better to die, since something with packaging is wrong
            throw new RuntimeException(e);
        }
    }

    public static @Nonnull RDFProgram getJenaRules(@Nonnull String t, boolean ignoreUnsupported,
                            @Nonnull String filename)
            throws UnsupportedFeature {
        CacheKey key = new CacheKey(t, ignoreUnsupported, filename);
        RDFProgram program = cached.getOrDefault(key, null);
        if (program == null) {
            program = loadJenaRules(key.t, key.ignoreUnsupported, key.filename);
            cached.put(key, program);
        }
        return program;
    }

    public static @Nonnull RDFProgram convertJenaRules(@Nonnull String t, boolean ignoreUnsupported,
                                                    @Nonnull List<Rule> jenaRules)
            throws UnsupportedFeature {
        return convertJenaRules(t, ignoreUnsupported, jenaRules, null);
    }
    public static @Nonnull RDFProgram convertJenaRules(@Nonnull String t, boolean ignoreUnsupported,
                                                    @Nonnull List<Rule> jenaRules,
                                                    @Nullable String programName)
            throws UnsupportedFeature {
        List<br.ufsc.lapesd.qsq.rules.Rule> list = new ArrayList<>();
        for (Rule jenaRule : jenaRules)
            addPlainRules(list, t, ignoreUnsupported, jenaRule);
        return new RDFListProgram(t, list, programName);
    }

    private static @Nonnull BufferedReader openJenaRulesFile(@Nonnull String filename)
            throws FileNotFoundException {
        // All Jena Rules are in the etc/ resource directory
        if (filename.equals("owl.rules")) {
            filename = "br/ufsc/lapesd/qsq/rdf/owl.rules";
        } else if (!filename.startsWith("etc") && !filename.startsWith("/etc"))
            filename = "etc/" + filename;
        // start with global class loader
        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(filename);
        if (is == null) {
            // absolute path, local CL
            if (!filename.startsWith("/")) filename = "/" + filename;
            is = JenaPrograms.class.getClassLoader().getResourceAsStream(filename);
            if (is == null) {
                // relative path, local CL
                filename = filename.substring(1);
                is = JenaPrograms.class.getClassLoader().getResourceAsStream(filename);
            }
        }
        if (is == null) {
            String msg = "Could not find resources file for jena rules: " + filename;
            throw new  FileNotFoundException(msg);
        }
        return new BufferedReader(new InputStreamReader(is));
    }

    private static void addPlainRules(@Nonnull Collection<br.ufsc.lapesd.qsq.rules.Rule> out,
                                      @Nonnull String t, boolean ignoreUnsupported,
                                      @Nonnull Rule jenaRule) throws UnsupportedFeature {
        ClauseEntry[] head = jenaRule.getHead();
        ClauseEntry[] body = jenaRule.getBody();
        for (ClauseEntry headEntry : head) {
            try {
                PlainAtom headAtom = triplePattern2Atom(t, headEntry);
                if (headAtom == null) continue;
                List<Atom> bodyAtoms = new ArrayList<>();
                for (ClauseEntry entry : body) {
                    PlainAtom atom = triplePattern2Atom(t, entry);
                    if (atom != null)
                        bodyAtoms.add(atom);
                }
                out.add(new PlainRule(jenaRule.getName(), headAtom, bodyAtoms));
            } catch (UnsupportedFeature ex) {
                if (!ignoreUnsupported)
                    throw ex;
                System.err.println(format("%s. Ignoring rule %s", ex.getMessage(), jenaRule));
                break;
            }
        }
    }

    private static @Nullable PlainAtom triplePattern2Atom(@Nonnull String t,
                                                          @Nonnull ClauseEntry clauseEntry)
            throws UnsupportedFeature {
        if (clauseEntry instanceof TriplePattern) {
            TriplePattern tp = (TriplePattern) clauseEntry;
            Term s = jenaNode2Term(tp.getSubject());
            Term p = jenaNode2Term(tp.getPredicate());
            Term o = jenaNode2Term(tp.getObject());
            return new PlainAtom(t, s, p, o);
        } else if (clauseEntry instanceof Functor) {
            String name = ((Functor) clauseEntry).getName();
            if (!NOP_FUNCTORS.contains(name))
                throw new UnsupportedFeature("Functor " + name + " not supported");
            return null;
        } else if (clauseEntry instanceof Rule) {
            throw new UnsupportedFeature("Rules are not supported as rule atoms");
        }
        throw new UnsupportedFeature("Usupported feature:" + clauseEntry.toString());
    }

    private static Term jenaNode2Term(@Nonnull Node node) throws UnsupportedFeature {
        if (node instanceof Node_Variable) {
            String name = node.getName();
            if (name.startsWith("?"))
                name = name.substring(1);
            return createVariable(name);
        } else if (node instanceof Node_Blank) {
            return createVariable(node.getBlankNodeId().getLabelString());
        } else if (node instanceof Node_URI) {
            String uri = node.getURI();
//            if (uri.startsWith("urn:x-hp-jena"))
//                throw new UnsupportedFeature("Jena internal URN "+uri+" is unsupported");
            return createConstant(ResourceFactory.createResource(uri));
        } else if (node instanceof Node_Literal) {
            String lang = node.getLiteralLanguage();
            String valueString = node.getLiteralValue().toString();
            RDFDatatype dtype = node.getLiteralDatatype();
            if (lang != null && !lang.equals("")) {
                return createConstant(ResourceFactory.createLangLiteral(valueString, lang));
            } else if (dtype != null) {
                if (dtype.equals(Functor.FunctorDatatype.theFunctorDatatype))
                    throw new UnsupportedFeature("Functor literals are not supported: " + node);
                return createConstant(ResourceFactory.createTypedLiteral(valueString, dtype));
            } else { // plain literal
                return createConstant(ResourceFactory.createPlainLiteral(valueString));
            }
        }
        throw new IllegalArgumentException("Cannot convert " + node);
    }
}
