package br.ufsc.lapesd.qsq.rdf.program;

import br.ufsc.lapesd.qsq.rdf.JenaPrograms;
import br.ufsc.lapesd.qsq.rules.ProgramBuilder;
import br.ufsc.lapesd.qsq.rules.Term;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.Collections;

import static br.ufsc.lapesd.qsq.rules.Term.createVariable;

public class RDFPrograms {
    public static RDFProgram createRDFS(@Nonnull String t, boolean deriveResource,
                                     boolean subReflexivity) {
        Term x = createVariable("x"), y = createVariable("y"), z = createVariable("z");
        Term a = createVariable("a"), b = createVariable("b");
        String name = String.format("RDFS(%s)", t);
        if (!deriveResource)
            name += "+noresource";
        if (!subReflexivity)
            name += "+noreflex";
        ProgramBuilder builder = RDFProgramBuilder.start(name, t)
                .beginRule("rdfs2")
                    .head(t, y, RDF.type, x)
                    .add(t, a, RDFS.domain, x)
                    .add(t, y, a, z).endRule()
                .beginRule("rdfs3")
                    .head(t, z, RDF.type, x)
                    .add(t, a, RDFS.range, x)
                    .add(t, y, a, z).endRule();
        if (deriveResource) {
            builder.beginRule("rdfs4a")
                        .head(t, x, RDF.type, RDFS.Resource)
                        .add(t, x, a, y).endRule()
                    .beginRule("rdfs4b")
                        .head(t, y, RDF.type, RDFS.Resource)
                        .add(t, x, a, y).endRule()
                    .beginRule("rdfs8")
                        .head(t, x, RDFS.subClassOf, RDFS.Resource)
                        .add(t, x, RDF.type, RDFS.Class).endRule();
        }
        builder.beginRule("rdfs5")
                    .head(t, x, RDFS.subPropertyOf, z)
                    .add(t, x, RDFS.subPropertyOf, y)
                    .add(t, y, RDFS.subPropertyOf, z).endRule();
        if (subReflexivity) {
            builder.beginRule("rdfs6")
                    .head(t, x, RDFS.subPropertyOf, x)
                    .add(t, x, RDF.type, RDF.Property).endRule();
        }
        builder.beginRule("rdfs7")
                    .head(t, x, b, y)
                    .add(t, a, RDFS.subPropertyOf, b)
                    .add(t, x, a, y).endRule()
                .beginRule("rdfs9")
                    .head(t, z, RDF.type, y)
                    .add(t, x, RDFS.subClassOf, y)
                    .add(t, z, RDF.type, x).endRule();
        if (subReflexivity) {
            builder.beginRule("rdfs10")
                    .head(t, x, RDFS.subClassOf, x)
                    .add(t, x, RDF.type, RDFS.Class).endRule();
        }
        builder.beginRule("rdfs11")
                    .head(t, x, RDFS.subClassOf, z)
                    .add(t, x, RDFS.subClassOf, y)
                    .add(t, y, RDFS.subClassOf, z).endRule()
                .beginRule("rdfs12")
                    .head(t, x, RDFS.subPropertyOf, RDFS.member)
                    .add(t, x, RDF.type, RDFS.ContainerMembershipProperty).endRule()
                .beginRule("rdfs13")
                    .head(t, x, RDFS.subClassOf, RDFS.Literal)
                    .add(t, x, RDF.type, RDFS.Datatype).endRule();
        return (RDFProgram) builder.build();
    }


    public static @Nonnull RDFProgram getOwl(@Nonnull String t) {
        try {
            return JenaPrograms.getJenaRules(t, true, "owl.rules");
        } catch (JenaPrograms.UnsupportedFeature ex) {
            throw new RuntimeException(ex);
        }
    }
    public static @Nonnull RDFProgram getOwlMicro(@Nonnull String t) {
        try {
            return JenaPrograms.getJenaRules(t, true, "owl-fb-micro.rules");
        } catch (JenaPrograms.UnsupportedFeature ex) {
            throw new RuntimeException(ex);
        }
    }
    public static @Nonnull RDFProgram getOwlMini(@Nonnull String t) {
        try {
            return JenaPrograms.getJenaRules(t, true, "owl-fb-mini.rules");
        } catch (JenaPrograms.UnsupportedFeature ex) {
            throw new RuntimeException(ex);
        }
    }

    public static @Nonnull RDFProgram getOwl() {
        return getOwl("T");
    }
    public static @Nonnull RDFProgram getOwlMicro() {
        return getOwlMicro("T");
    }
    public static @Nonnull RDFProgram getOwlMini() {
        return getOwlMini("T");
    }

    public static RDFProgram RDFS_T = createRDFS("T", false, true);
    public static RDFProgram RDFS_FULL = createRDFS("T", true, true);
    public static RDFProgram EMPTY = new RDFListProgram("T",
            Collections.emptyList(), "empty");
}
