package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.relations.InstanceData;
import org.apache.jena.rdf.model.Model;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ModelInstanceData implements InstanceData {
    private final @Nonnull Map<String, ModelRelation> map = new HashMap<>();

    public ModelInstanceData(@Nonnull String predicate, @Nonnull Model model) {
        map.put(predicate, new ModelRelation(predicate, model));
    }

    public @Nonnull ModelInstanceData setModel(@Nonnull String predicate, @Nonnull Model model) {
        map.put(predicate, new ModelRelation(predicate, model));
        return this;
    }

    @Override
    public @Nullable ModelRelation get(@Nonnull String name) {
        return map.get(name);
    }

    public Collection<String> getDatalogPredicates() {
        return map.keySet();
    }
}
