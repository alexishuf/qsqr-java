package br.ufsc.lapesd.qsq.rdf;

import br.ufsc.lapesd.qsq.rules.parser.ConstantParser;
import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.system.PrefixMap;
import org.apache.jena.riot.system.PrefixMapStd;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;
import static org.apache.jena.rdf.model.ResourceFactory.*;

public class JenaConstantParser implements ConstantParser {
    //                                          1    2          3                      4        5      6           7        8      9
    private final Pattern RX = compile("^\\s*(?:(a)|<([^>]+)>|\"([^\"]+)\"(?:\\^\\^(?:<([^>]+)>|(\\w+):([^ ,)]*))|@(\\w+))?|(\\w+):([^ ,)]*))");
    private final Pattern PREFIX_RX = compile("^\\s*@prefix\\s+(\\w+):\\s+<([^>]+)>\\s*.?\\s*$");
    private PrefixMap prefixMap = new PrefixMapStd();

    @Override
    public @Nullable Parse parse(@Nonnull String text) {
        Matcher m = RX.matcher(text);
        if (m.find()) {
            RDFNode value = null;
            if (m.group(1) != null) {
                value = RDF.type;
            } else if (m.group(2) != null) {
                value = createResource(m.group(2));
            } else if (m.group(3) != null && m.group(4) == null && m.group(5) == null &&
                       m.group(6) == null && m.group(7) == null) {
                value = createPlainLiteral(m.group(3));
            } else if (m.group(3) != null && m.group(4) != null) {
                value = createTypedLiteral(m.group(3),
                        TypeMapper.getInstance().getTypeByName(m.group(4)));
            } else if (m.group(3) != null && m.group(5) != null && m.group(6) != null) {
                String dtURI = prefixMap.expand(m.group(5), m.group(6));
                value = createTypedLiteral(m.group(3),
                        TypeMapper.getInstance().getTypeByName(dtURI));
            } else if (m.group(3) != null && m.group(7) != null) {
                value = createLangLiteral(m.group(3), m.group(7));
            } else if (m.group(8) != null && m.group(9) != null) {
                value = createResource(prefixMap.expand(m.group(8), m.group(9)));
            }
            assert value != null : "RX groups do not match Java code";
            String replaced = m.replaceFirst("");
            return new Parse(value, text.length() - replaced.length());
        }
        return null;
    }

    @Override
    public boolean tryPrefix(@Nonnull String line) {
        Matcher matcher = PREFIX_RX.matcher(line);
        if (!matcher.matches())
            return false;
        prefixMap.add(matcher.group(1), matcher.group(2));
        return true;
    }
}
