package br.ufsc.lapesd.qsq.interactive.impl;

import br.ufsc.lapesd.qsq.interactive.InteractionController;
import br.ufsc.lapesd.qsq.interactive.Value;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

public class BatchInteractionController implements InteractionController {
    @Override
    public void showState(@Nonnull Supplier<Iterable<Value>> state) {
        // pass
    }
}
