package br.ufsc.lapesd.qsq.interactive;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

public interface InteractionController {
    void showState(@Nonnull Supplier<Iterable<Value>> state);
}
