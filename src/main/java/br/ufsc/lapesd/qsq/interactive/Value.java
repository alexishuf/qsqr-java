package br.ufsc.lapesd.qsq.interactive;

import br.ufsc.lapesd.qsq.relations.Marker;
import br.ufsc.lapesd.qsq.relations.Relation;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Objects;

public class Value {
    private @Nonnull String name;
    private int nesting;
    private Object value;
    private @Nonnull Type type;

    public Value(@Nonnull String name, @Nonnull Type type, @Nullable Object value) {
        this(name, 0, type, value);
    }

    public Value(@Nonnull String name, int nesting, @Nonnull Type type, @Nullable Object value) {
        this.name = name;
        this.nesting = nesting;
        this.value = value;
        this.type = type;
    }

    public static Value separator() {
        return new Value("", 0, Type.SEPARATOR, null);
    }

    public static Value empty(@Nonnull String name) {
        return empty(name, 0);
    }

    public static Value empty(@Nonnull String name, int nesting) {
        return new Value(name, nesting, Type.EMPTY, null);
    }

    public enum Type {
        EMPTY,
        SEPARATOR,
        STRING,
        RELATION,
        MARKER;

        public boolean isTabular() {
            return isRowsContainer();
        }
        public boolean isRowsContainer() {
            return this == RELATION || this == MARKER;
        }

    }

    public @Nonnull Type getType() {
        return type;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    public int getNesting() {
        return nesting;
    }

    public boolean isEmpty() {
        return type == Type.EMPTY;
    }

    public boolean isNull() {
        return value == null;
    }

    public @Nonnull Collection<String> getColumns() {
        assert getType().isTabular();
        if (getType() == Type.RELATION)
            return asRelation().getColumns();
        else if (getType() == Type.MARKER)
            return asMarker().getRelation().getColumns();
        else
            throw new UnsupportedOperationException();
    }
    @SuppressWarnings("unchecked")
    public @Nonnull <T> Iterable<T> asIterable(@Nonnull Class<T> cls) {
        if (getType() == Type.MARKER) {
            Preconditions.checkArgument(cls.equals(Row.class));
            return (Iterable<T>) asMarker();
        } else if (getType() == Type.RELATION) {
            Preconditions.checkArgument(cls.equals(Row.class));
            return (Iterable<T>) asRelation();
        }
        throw new UnsupportedOperationException();
    }

    public @Nonnull String asString() {
        assert value != null;
        return (String)value;
    }

    public @Nonnull Relation asRelation() {
        assert value != null;
        return (Relation)value;
    }

    public @Nonnull Marker asMarker() {
        assert value != null;
        return (Marker)value;
    }

    @Override
    public String toString() {
        return Objects.toString(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Value value1 = (Value) o;
        return Objects.equals(value, value1.value) &&
                getType() == value1.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, getType());
    }
}
