package br.ufsc.lapesd.qsq.interactive.impl;

import br.ufsc.lapesd.qsq.interactive.InteractionController;
import br.ufsc.lapesd.qsq.interactive.Value;
import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.table.Table;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.googlecode.lanterna.gui2.GridLayout.createHorizontallyFilledLayoutData;

public class LanternaDisplay implements InteractionController {
    private final Screen screen;
    private final MultiWindowTextGUI multiWindow;

    public LanternaDisplay() throws IOException {
        DefaultTerminalFactory terminalFactory = new DefaultTerminalFactory();
        screen = terminalFactory.createScreen();
        screen.startScreen();
        multiWindow = new MultiWindowTextGUI(screen);
    }

    @Override
    public void showState(@Nonnull Supplier<Iterable<Value>> supplier) {
        StateWindow stateWindow = new StateWindow(supplier.get());
        multiWindow.addWindowAndWait(stateWindow);
    }

    private class StateWindow extends BasicWindow {
        private final int GRID_COLUMNS = 2;

        public StateWindow(@Nonnull Iterable<Value> state) {
            this("State", state);
        }

        public StateWindow(@Nonnull String title, @Nonnull Iterable<Value> state) {
            super(title);
            GridLayout layout = new GridLayout(GRID_COLUMNS);
            Panel panel = new Panel(layout);
            panel.addComponent(new Button("Continue", StateWindow.this::close)
                    .setLayoutData(createHorizontallyFilledLayoutData(GRID_COLUMNS)));
            int nesting = 0;
            for (Value value : state) {
                if (nesting > value.getNesting())
                    addSeparator(panel);
                nesting = value.getNesting();
                if (value.getType() == Value.Type.SEPARATOR) {
                    addSeparator(panel);
                } else {
                    Label label = new Label(value.getName());
                    if (value.isEmpty())
                        label.setLayoutData(createHorizontallyFilledLayoutData(GRID_COLUMNS));
                    panel.addComponent(label);
                    if (!value.isEmpty())
                        panel.addComponent(createComponent(value));
                }
            }
            setComponent(panel);
        }

        private void addSeparator(@Nonnull Panel panel) {
            panel.addComponent(new Separator(Direction.HORIZONTAL)
                    .setLayoutData(createHorizontallyFilledLayoutData(GRID_COLUMNS)));
        }

        private @Nonnull Component createComponent(@Nonnull Value value) {
            if (value.getType().isTabular()) {
                Table<String> table = new Table<>(value.getColumns().toArray(new String[1]));
                if (value.getType().isRowsContainer()) {
                    for (Row row : value.asIterable(Row.class)) {
                        table.getTableModel().addRow(RowOps.toValuesList(row).stream()
                                .map(Objects::toString).collect(Collectors.toList()));
                    }
                } else {
                    throw new UnsupportedOperationException();
                }
                return table;
            } else {
                return new Label(value.asString());
            }
        }

    }
}
