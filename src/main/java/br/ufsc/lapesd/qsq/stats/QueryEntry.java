package br.ufsc.lapesd.qsq.stats;

import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class QueryEntry implements LogEntry {
    protected long sentSubQueries;
    protected long receivedTuples;
    protected long receivedTBoxTuples;
    protected long receivedABoxTuples;
    protected final @Nonnull String predicate;
    protected final @Nonnull List<Row> selectionClauses;

    public QueryEntry(long sentSubQueries, long receivedTuples,
                      long receivedTBoxTuples, long receivedABoxTuples,
                      @Nonnull String predicate,
                      @Nonnull List<Row> selectionClauses) {
        this.sentSubQueries = sentSubQueries;
        this.receivedTuples = receivedTuples;
        this.receivedTBoxTuples = receivedTBoxTuples;
        this.receivedABoxTuples = receivedABoxTuples;
        this.selectionClauses = selectionClauses;
        this.predicate = predicate;
    }

    public long getSentSubQueries() {
        return sentSubQueries;
    }

    public long getReceivedTuples() {
        return receivedTuples;
    }

    public @Nonnull List<Row> getSelectionClauses() {
        return selectionClauses;
    }

    public @Nonnull String getPredicate() {
        return predicate;
    }

    public @Nonnull List<String> getSelectionColumns() {
        return getSelectionClauses().stream().flatMap(row -> row.getColumns().stream())
                .distinct().sorted().collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryEntry that = (QueryEntry) o;
        return getSentSubQueries() == that.getSentSubQueries() &&
                getReceivedTuples() == that.getReceivedTuples() &&
                getPredicate().equals(that.getPredicate()) &&
                getSelectionClauses().equals(that.getSelectionClauses());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSentSubQueries(), getReceivedTuples(), getPredicate(),
                getSelectionClauses());
    }

    @Override
    public String toString() {
        String clauseString;
        if (getSelectionClauses().size() > 1) {
            clauseString = format("%d or-clauses over %s", getSelectionClauses().size(),
                    String.join(", ", getSelectionColumns()));
        } else {
            clauseString = getSelectionClauses().get(0).toString();
        }
        return format("QueryEntry{%d sub, %d recv, on %s, %s}", getSentSubQueries(),
                getReceivedTuples(), getPredicate(), clauseString);
    }
}
