package br.ufsc.lapesd.qsq.stats;

import br.ufsc.lapesd.qsq.relations.rows.Row;
import br.ufsc.lapesd.qsq.relations.rows.RowOps;

import javax.annotation.Nonnull;

public interface TBoxContext {
    boolean isTBoxURI(Object uri);

    default boolean isTBoxRow(@Nonnull Row row) {
        return RowOps.toValuesList(row).stream().allMatch(this::isTBoxURI);
    }
}
