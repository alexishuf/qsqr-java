package br.ufsc.lapesd.qsq.stats;

import br.ufsc.lapesd.qsq.relations.rows.Row;

import javax.annotation.Nonnull;
import java.util.ArrayList;

public class MutableQueryEntry extends QueryEntry {
    public MutableQueryEntry(@Nonnull String predicate) {
        super(0, 0, 0, 0,
                predicate, new ArrayList<>());
    }

    public void incrementSentSubQueries() {
        ++sentSubQueries;
    }

    public void incrementReceivedTuples() {
        ++receivedTuples;
    }

    public void incrementReceivedTBoxTuples() {
        ++receivedTBoxTuples;
    }

    public void incrementReceivedABoxTuples() {
        ++receivedABoxTuples;
    }

    public void addSelectionClause(@Nonnull Row clause) {
        selectionClauses.add(clause);
    }
}
