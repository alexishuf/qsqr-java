package br.ufsc.lapesd.qsq.stats;

import br.ufsc.lapesd.qsq.rdf.ModelInstanceData;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.annotation.Nonnull;
import java.util.Objects;

import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

public class ModelInstanceDataTBoxContext implements TBoxContext {
    private final  @Nonnull ModelInstanceData data;

    public ModelInstanceDataTBoxContext(@Nonnull ModelInstanceData data) {
        this.data = data;
    }

    @Override
    public boolean isTBoxURI(Object uri) {
        Resource r;
        if (uri instanceof String)
            r = ResourceFactory.createResource(uri.toString());
        else if (uri instanceof Resource)
            r = (Resource)uri;
        else
            return false;

        RDFNode nil = null;
        for (String t : data.getDatalogPredicates()) {
            if (r.toString().startsWith(RDF.getURI())) return true;
            if (r.toString().startsWith(RDFS.getURI())) return true;
            if (r.toString().startsWith(OWL2.getURI())) return true;
            Model model = Objects.requireNonNull(data.get(t)).getModel();
            if (model.contains(null, createProperty(r.getURI()), nil)) return true;
            if (model.contains(r,       RDF.type,    RDFS.Class)) return true;
            if (model.contains(r,       RDF.type,    OWL2.Class)) return true;
            if (model.contains(r,       RDF.type,  RDF.Property)) return true;
            if (model.contains(null, RDFS.subClassOf,      r)) return true;
            if (model.contains(null, RDFS.subPropertyOf,   r)) return true;
            if (model.contains(r,       RDFS.subClassOf,    nil)) return true;
            if (model.contains(r,       RDFS.subPropertyOf, nil)) return true;
            if (model.contains(null, RDFS.domain,          r)) return true;
            if (model.contains(null, RDFS.range,           r)) return true;
            if (model.contains(r,       RDFS.domain,        nil)) return true;
            if (model.contains(r,       RDFS.range,         nil)) return true;
        }
        return false;
    }
}
